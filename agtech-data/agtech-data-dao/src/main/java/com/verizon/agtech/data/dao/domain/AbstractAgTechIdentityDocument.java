package com.verizon.agtech.data.dao.domain;

import com.verizon.agtech.data.common.domain.AbstractAgTechOwnedDocument;

/**
 * Created by drq on 4/26/16.
 */
abstract public class AbstractAgTechIdentityDocument extends AbstractAgTechOwnedDocument {

    private String email;

    private String contactPhone;

    private String address;

    private String firstName;

    private String lastName;

    public AbstractAgTechIdentityDocument() {
    }

    public AbstractAgTechIdentityDocument(String customerId) {
        super(customerId);
    }

    public AbstractAgTechIdentityDocument(String customerId, String email, String contactPhone, String address, String firstName, String lastName) {
        super(customerId);
        this.email = email;
        this.contactPhone = contactPhone;
        this.address = address;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
