package com.verizon.agtech.data.common.data.query;

import java.util.Iterator;

/**
 * Created by drq on 9/14/14.
 */
public class LocationSearchResults {
    private Integer pageTotalItems;
    private Iterator<?> dataItems;

    public LocationSearchResults() {
    }

    public LocationSearchResults(Integer pageTotalItems, Iterator<?> dataItems) {
        this.pageTotalItems = pageTotalItems;
        this.dataItems = dataItems;
    }

    public Integer getPageTotalItems() {
        return pageTotalItems;
    }

    public void setPageTotalItems(Integer pageTotalItems) {
        this.pageTotalItems = pageTotalItems;
    }

    public Iterator<?> getDataItems() {
        return dataItems;
    }

    public void setDataItems(Iterator<?> dataItems) {
        this.dataItems = dataItems;
    }
}
