package com.verizon.agtech.data.dao.service;

import com.verizon.agtech.data.common.data.identity.Device;
import com.verizon.agtech.data.common.data.identity.DeviceIdentity;
import com.verizon.agtech.data.common.data.identity.VerificationStatus;
import com.verizon.agtech.data.common.error.ResourceNotFoundException;
import com.verizon.agtech.data.dao.domain.CustomerDocument;
import com.verizon.agtech.data.dao.domain.FlowMeterReadingDocument;
import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Thanigai on 6/15/16.
 */
//@Service
@Transactional
public class VerificationService {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private WeatherStationReadingService weatherStationReadingService;

    @Autowired
    private FlowMeterReadingService flowMeterReadingService;

    String HEART_BEAT_TYPE = "heartbeat";

    private long flowMeterHeartbeatInterval;

    private long weatherStationDataPostInterval;


    public CustomerDocument populateVerificationStatusOfDevices(String customerId) {
        CustomerDocument verificationdocument = customerService.findCustomerByAnyId(customerId);
        try {
            if (verificationdocument != null) {
                List<Device> devices = verificationdocument.getEntities().getDevices();
                Iterator<Device> iteratorDevice = devices.iterator();
                while(iteratorDevice.hasNext()) {
                    Device device = iteratorDevice.next();
                    if(device.getType().toLowerCase().matches("gate[\\s]*way")) {
                        List<DeviceIdentity> verficationIdentities = device.getConnectedDevices();
                        Iterator<DeviceIdentity> iterVerificationIdentity = verficationIdentities.iterator();
                        while(iterVerificationIdentity.hasNext()) {
                            DeviceIdentity verficationIdentity = iterVerificationIdentity.next();
                            checkAndPopulateVerificationStatus(verficationIdentity.getSerialNumber(),verificationdocument,verficationIdentity);
                        }
                    }
                }

            } else {
                throw new ResourceNotFoundException(customerId, "Customer");
            }

        } catch (Exception e) {

        }

        return verificationdocument;
    }

    private void checkAndPopulateVerificationStatus(String deviceSerialNumber,CustomerDocument verificationDocument,DeviceIdentity verficationIdentity) {

        List<Device> devices = verificationDocument.getEntities().getDevices();
        Iterator<Device> iteratorDevice = devices.iterator();
        while(iteratorDevice.hasNext()) {
            Device device = iteratorDevice.next();
            if(device.getType().toLowerCase().matches("weather[\\s]*station")) {

                Page<WeatherStationReadingDocument> weatherDocuments = weatherStationReadingService.findLatestWeatherStationReadingDocument(deviceSerialNumber);
                if(weatherDocuments != null) {
                    Iterator<WeatherStationReadingDocument> iterateWeatherStationData = weatherDocuments.iterator();
                    while (iterateWeatherStationData.hasNext()) {
                        WeatherStationReadingDocument  weatherStationReadingDocument = iterateWeatherStationData.next();
                        long lastWeatherDateTime = weatherStationReadingDocument.getCreatedAtTimestamp();
                        long currentTime = System.currentTimeMillis();
                        boolean status = (currentTime - lastWeatherDateTime) > weatherStationDataPostInterval;
                        if(status) {
                            verficationIdentity.setVerificationStatus(new VerificationStatus(false,new Date(lastWeatherDateTime)));
                            device.setVerificationStatus(new VerificationStatus(false,new Date(lastWeatherDateTime)));

                        } else {
                            verficationIdentity.setVerificationStatus(new VerificationStatus(true,new Date(lastWeatherDateTime)));
                            device.setVerificationStatus(new VerificationStatus(true,new Date(lastWeatherDateTime)));
                        }

                    }

                }

            }
             else if (device.getType().toLowerCase().matches("flow[\\s]*meter")) {
                    Page<FlowMeterReadingDocument> flowMeterDocuments = flowMeterReadingService.findLatestFlowMeterReadingDocument(deviceSerialNumber,HEART_BEAT_TYPE);
                     if(flowMeterDocuments != null) {
                         Iterator<FlowMeterReadingDocument> iterateFlowMererData = flowMeterDocuments.iterator();
                             while (iterateFlowMererData.hasNext()) {
                                 FlowMeterReadingDocument  flowMeterReadingDocument = iterateFlowMererData.next();
                                 long lastWeatherDateTime = flowMeterReadingDocument.getCreatedAtTimestamp();
                                 long currentTime = System.currentTimeMillis();
                                 boolean status = (currentTime - lastWeatherDateTime) > flowMeterHeartbeatInterval;
                                     if(status) {
                                         verficationIdentity.setVerificationStatus(new VerificationStatus(false,new Date(lastWeatherDateTime)));
                                         device.setVerificationStatus(new VerificationStatus(false,new Date(lastWeatherDateTime)));

                                     } else {
                                         verficationIdentity.setVerificationStatus(new VerificationStatus(true,new Date(lastWeatherDateTime)));
                                         device.setVerificationStatus(new VerificationStatus(true,new Date(lastWeatherDateTime)));
                                     }

                             }

                     }

             }

        }

    }


    public long getWeatherStationDataPostInterval() {
        return weatherStationDataPostInterval;
    }

    public void setWeatherStationDataPostInterval(long weatherStationDataPostInterval) {
        this.weatherStationDataPostInterval = weatherStationDataPostInterval;
    }

    public long getFlowMeterHeartbeatInterval() {
        return flowMeterHeartbeatInterval;
    }

    public void setFlowMeterHeartbeatInterval(long flowMeterHeartbeatInterval) {
        this.flowMeterHeartbeatInterval = flowMeterHeartbeatInterval;
    }


}
