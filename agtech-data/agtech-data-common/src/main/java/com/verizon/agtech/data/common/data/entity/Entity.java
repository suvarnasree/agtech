package com.verizon.agtech.data.common.data.entity;

import javax.validation.constraints.NotNull;

/**
 * Created by donthi 
 */
public class Entity {


    @NotNull
    private String serialNumber;

    private String blockName;
    
    private EntityDetail entityDetail;
    
    private String deviceType;

    public Entity() {
    }

    public Entity( String serialNumber, String blockName, String deviceType) {
    	super();
        this.serialNumber = serialNumber;
        this.blockName = blockName;
    	this.deviceType = deviceType;
    }
    
   public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

   public String getBlockName() {
		return blockName;
	}

	public void setBlockName(String blockName) {
		this.blockName = blockName;
	}

	public EntityDetail getEntityDetail() {
		return entityDetail;
	}

	public void setEntityDetail(EntityDetail entityDetail) {
		this.entityDetail = entityDetail;
	}
   public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}   
}
