package com.verizon.agtech.data.dao.repository;

import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;
import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by drq on 6/8/15.
 */
public interface WeatherStationReadingRepository extends PagingAndSortingRepository<WeatherStationReadingDocument, Long>, WeatherStationReadingRepositoryCustom {
    /**
     * Returns the customer with the given identifier.
     *
     * @param id
     * @return
     */
    WeatherStationReadingDocument findOne(Long id);

    /**
     * Saves the given {@link WeatherStationReadingDocument}.
     *
     * @param weatherStationReading
     * @return
     */
    WeatherStationReadingDocument save(WeatherStationReadingDocument weatherStationReading);

    /**
     * Removes the given {@link WeatherStationReadingDocument}.
     *
     * @param weatherStationReading
     * @return
     */
    void delete(WeatherStationReadingDocument weatherStationReading);

    /**
     * @param id
     * @return
     */
    WeatherStationReadingDocument findById(String id);

    /**
     * @param guid
     * @return
     */
    WeatherStationReadingDocument findByGuid(String guid);
    
    WeatherStationReadingDocument findByWeatherStationID(String weatherStationId);
    

    /**
     * @return
     */
    Page<WeatherStationReadingDocument> findAll(Pageable pageable);

    /**
     *
     * @param startDateTime
     * @param endDateTime
     * @param pageable
     * @return
     */
    @Query("{'timeStamp': {$gte: ?0, $lte: ?1}}")
    Page<WeatherStationReadingDocument> findWeatherStationReadingsByTimeStamp(DateTime startDateTime, DateTime endDateTime, Pageable pageable);

    /**
     *
     * @param id
     * @param startDateTime
     * @param endDateTime
     * @param pageable
     * @return
     */
    @Query("{'$and': [{'weatherStationID': ?0}, {'timeStamp': {$gte: ?1, $lte: ?2}}]}")
    Page<WeatherStationReadingDocument> findWeatherStationReadingsByIDAndTimeRange(String id, DateTime startDateTime, DateTime endDateTime, Pageable pageable);

//,$sort: {createdAt:-1}

    /**
     * This method is to provide the latest Weather station data for the given Weather Station ID
     * @param id
     * @param pageable
     * @return Page<WeatherStationReadingDocument>
     */
    @Query("{'weatherStationID': ?0}")
    Page<WeatherStationReadingDocument> findLatestWeatherStationReadingPage(String id,Pageable pageable);
}
