package com.verizon.weatherstationemulator.model;

import java.util.Collection;

public class WeatherStationInformationRequest {

	private String weatherStationId;
	private Collection<EmulatorEvent> events;

	public void setWeatherStationId(String weatherStationId) {
		this.weatherStationId = weatherStationId;
	}

	public Collection<EmulatorEvent> getEvents() {
		return events;
	}

	public void setEvents(Collection<EmulatorEvent> events) {
		this.events = events;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((events == null) ? 0 : events.hashCode());
		result = prime * result + ((weatherStationId == null) ? 0 : weatherStationId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WeatherStationInformationRequest other = (WeatherStationInformationRequest) obj;
		if (events == null) {
			if (other.events != null)
				return false;
		} else if (!events.equals(other.events))
			return false;
		if (weatherStationId == null) {
			if (other.weatherStationId != null)
				return false;
		} else if (!weatherStationId.equals(other.weatherStationId))
			return false;
		return true;
	}

}
