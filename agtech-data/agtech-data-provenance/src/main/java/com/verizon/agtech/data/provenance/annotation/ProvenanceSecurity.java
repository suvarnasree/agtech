package com.verizon.agtech.data.provenance.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

/**
 * Marks classes that need generate security trust information.
 */
@java.lang.annotation.Retention(RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({ElementType.METHOD})
public @interface ProvenanceSecurity
{
    String voter() default "master";
}
