package com.verizon.agtech.data.ingestor.delegate;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.StopWorkException;
import com.verizon.agtech.data.common.data.weatherstation.WeatherStationData;


import com.verizon.agtech.data.ingestor.data.RabbitMQData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WeatherStationReadingRabbitMQProcessor  extends IngestorDelegate {
    public final static String NAME = "WEATHER_STATION_READING_RABBITMQ_PROCESSOR";

    private final static Logger logger = LoggerFactory.getLogger(WeatherStationReadingRabbitMQProcessor.class);

    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (logger.isDebugEnabled()) {
            logger.debug(messageSource.getMessage("delegate.entering", new Object[]{NAME}, null));
        }
        if (data != null && data instanceof RabbitMQData) {
            try {
                RabbitMQData rabbitMQData = (RabbitMQData) data;
                String message = rabbitMQData.getPayload();

                ObjectMapper mapper = new ObjectMapper();
                WeatherStationData weatherStationData = mapper.readValue(message, WeatherStationData.class);

                if (logger.isDebugEnabled()) {
                    logger.debug(messageSource.getMessage("delegate.leaving", new Object[]{NAME}, null));
                }
                return weatherStationData;
            } catch (Exception e) {
                throw new StopWorkException("Failed to process RabbitMQ message", e);
            }
        } else {
            throw new StopWorkException(messageSource.getMessage("delegate.invalidData", new Object[]{NAME, RabbitMQData.class.getCanonicalName(), data.getClass().getCanonicalName()}, null));
        }

    }
}
