package com.verizon.agtech.data.provenance.aspect;

import com.verizon.agtech.data.common.request.RequestContext;
import com.verizon.agtech.data.common.request.RequestContextSingleton;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.UUID;

abstract public class ProvenanceSender extends AnnotationUtils {

    private static Log logger = LogFactory.getLog(ProvenanceSender.class);
    private AmqpTemplate messageTemplate;
    private String targetQueue;

    public AmqpTemplate getMessageTemplate() {
        return messageTemplate;
    }

    public void setMessageTemplate(AmqpTemplate messageTemplate) {
        this.messageTemplate = messageTemplate;
    }

    public String getTargetQueue() {
        return targetQueue;
    }

    public void setTargetQueue(String targetQueue) {
        this.targetQueue = targetQueue;
    }

    public void sendProvenanceEvent(String operationClass, String operationType, HashMap<String, Object> eventDetails) {

        if (logger.isDebugEnabled()) {
            logger.debug("Request user is now (sender) " + RequestContextSingleton.getRequestContext().getRequestUser());
        }

        RequestContext requestContext = RequestContextSingleton.getRequestContext();

        String requestId = requestContext.getRequestId();
        if (requestId == null) {
            requestId = UUID.randomUUID().toString();
            requestContext.setRequestId(requestId);
        }

        String requestUser = requestContext.getRequestUser();

        if (requestUser == null) {
            requestUser = "anonymous";
        }

        MessageProperties properties = new MessageProperties();
        properties.setMessageId(UUID.randomUUID().toString());

        JSONObject jsonObject = new JSONObject();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        TimeZone timeZone = TimeZone.getTimeZone("America/New_York");
        sdf.setTimeZone(timeZone);

        try {
            jsonObject.put("messageType", "data-event");
            jsonObject.put("dateTime", sdf.format(new Date()));
            jsonObject.put("operationType", operationType);
            jsonObject.put("operationClass", operationClass);
            jsonObject.put("executingUser", requestUser);
            jsonObject.put("requestId", requestId);

            JSONObject body = new JSONObject();
            JSONObject entity = new JSONObject();
            for (String key : eventDetails.keySet()) {
                if (key.equals("id")) {
                    JSONObject keyObj = new JSONObject();
                    keyObj.put("id", eventDetails.get(key));
                    entity.put("key", keyObj);
                } else {
                    entity.put(key, eventDetails.get(key));
                }
            }
            body.put(operationClass, entity);
            jsonObject.put("body", body);
        } catch (JSONException e) {
            logger.error("Failed to populate event message", e);
        }

        Message message = new Message(jsonObject.toString().getBytes(), properties);

        logger.info(String.format("Send Provenance message to RabbitMQ"));
        logger.info(jsonObject.toString());

        messageTemplate.send(targetQueue, message);
    }
}
