package com.verizon.agtech.data.akka;

import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import org.springframework.beans.factory.InitializingBean;

/**
 * Created by drq on 4/11/14.
 */
public abstract class CollectorDelegate implements InitializingBean, CollectorDataHandler {

    private ActorSystem actorSystem;

    private String delegateBeanName;

    private CollectorRouter collectorRouter;

    protected ActorSelection delegate;

    public ActorSystem getActorSystem() {
        return actorSystem;
    }

    public void setActorSystem(ActorSystem actorSystem) {
        this.actorSystem = actorSystem;
    }

    public String getDelegateBeanName() {
        return delegateBeanName;
    }

    public void setDelegateBeanName(String delegateBeanName) {
        this.delegateBeanName = delegateBeanName;
    }

    public CollectorRouter getCollectorRouter() {
        return collectorRouter;
    }

    public void setCollectorRouter(CollectorRouter collectorRouter) {
        this.collectorRouter = collectorRouter;
    }

    protected Object processData(Object data, CollectorActor sender) {
        return data;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (delegateBeanName != null) {
            delegate = actorSystem.actorSelection("/user/" + delegateBeanName);
        }
    }

    public ActorSelection getDelegate(Object data) {
        if (delegate != null && collectorRouter == null) {
            return delegate;
        }

        if (collectorRouter != null) {
            String routeBeanName = collectorRouter.router(data);
            return delegate = actorSystem.actorSelection("/user/" + routeBeanName);
        } else {
            return null;
        }
    }

    @Override
    public Object handle(Object message, CollectorActor sender) {
        Object data = processData(message, sender);
        if (data != null) {
            delegate = this.getDelegate(data);
            if (delegate != null) {
                delegate.tell(data, sender.self());
            }
        }
        return data;
    }
}
