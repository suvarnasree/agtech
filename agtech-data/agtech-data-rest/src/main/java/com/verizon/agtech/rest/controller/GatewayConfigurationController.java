package com.verizon.agtech.rest.controller;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.WebApplicationException;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.verizon.agtech.data.common.data.identity.VerficationIdentity;
import org.apache.http.HttpResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.support.incrementer.DerbyMaxValueIncrementer;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.util.ISO8601Utils;
import com.mysema.util.FileUtils;
import com.verizon.agtech.data.common.data.identity.Device;
import com.verizon.agtech.data.common.data.identity.DeviceIdentity;
import com.verizon.agtech.data.common.data.identity.Entities;
import com.verizon.agtech.data.dao.domain.CustomerDocument;
import com.verizon.agtech.data.dao.domain.EntityDocument;
import com.verizon.agtech.data.dao.service.CustomerService;
import com.verizon.agtech.data.dao.service.EntityService;

import io.swagger.annotations.ApiParam;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLConnection;
import java.nio.charset.Charset;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("v1/file")

public class GatewayConfigurationController {
	Logger logger = LoggerFactory.getLogger(GatewayConfigurationController.class);

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/customer/{customerId}/Gateway/{gatewayId}", method = RequestMethod.GET)
	public void downloadFile(HttpServletResponse response, @PathVariable String customerId,
			@PathVariable String gatewayId) throws IOException, JSONException {

		String weatherStationId = GetWeatherStationId(customerId, gatewayId);

		File file = null;
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		file = new File(classloader.getResource("vzw-agtech.zip").getFile());
		File file1 = new File("vzw-agtech_" + gatewayId + ".zip");
		FileOutputStream fos = new FileOutputStream(file1);
		ZipOutputStream out = new ZipOutputStream(fos);
		ZipInputStream in = new ZipInputStream(new FileInputStream(file));
		ZipEntry entry = in.getNextEntry();
		while (entry != null) {
			byte[] buf = new byte[1024];
			out.putNextEntry(new ZipEntry(entry.getName()));
			int len;
			while((len = in.read(buf)) > 0 && !entry.getName().equalsIgnoreCase("vzw-agtech/VZW-agtech-config.json") ) {
				out.write(buf, 0, len);
			}
			if (entry.getName().equalsIgnoreCase("vzw-agtech/VZW-agtech-config.json")) {
				String theFile = new String(buf, 0, len);
				JSONObject object = new JSONObject(theFile);
				
				if (!weatherStationId.equals("")) {
					object.put("weather-station-present", true);
				}
				object.put("customer-id", customerId);
				object.put("weather-station-id", weatherStationId);
				object.put("gateway-id", gatewayId);
				String accountName = customerService.findCustomerByCustomerId(customerId).getAccountName();
				object.put("account-name", accountName);
				String apiKey = customerService.findCustomerByCustomerId(customerId).getKey();
				object.put("api-key", apiKey);
				buf = object.toString().getBytes();
				out.write(buf);
     		}

        		 out.closeEntry();
        		 entry = in.getNextEntry();
        		 }
		out.close();
		String mimeType = URLConnection.guessContentTypeFromName(file1.getName());
		if (mimeType == null) {
			mimeType = "application/octet-stream";
		}
		response.setContentType(mimeType);
		response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file1.getName() + "\""));
		response.setContentLength((int) file1.length());
		InputStream inputStream = new BufferedInputStream(new FileInputStream(file1));
		FileCopyUtils.copy(inputStream, response.getOutputStream());
		logger.info("Gatewayconfiguration file has been succesfuly sent to UI");
	}

	private String GetWeatherStationId(String customerId, String gatewayId) {

		CustomerDocument document = customerService.findCustomerByCustomerId(customerId);
		List<Device> listOfDevices = document.getEntities().getDevices();
		Iterator<Device> iterDevice = listOfDevices.iterator();
		while (iterDevice.hasNext()) {
			Device device = iterDevice.next();
			if (device.getSerialNumber().equals(gatewayId)) {
				List<DeviceIdentity> listOfDeviceConnected = device.getConnectedDevices();
				if (listOfDeviceConnected != null) {
					Iterator<DeviceIdentity> iterConnectedDevices = listOfDeviceConnected.iterator();
					while (iterConnectedDevices.hasNext()) {
						DeviceIdentity connectedDevice = iterConnectedDevices.next();
						String serialNoForConnectedDevice = connectedDevice.getSerialNumber();
						List<Device> listOfDevices1 = document.getEntities().getDevices();
						Iterator<Device> iterDeviceBySerialNumber = listOfDevices1.iterator();
						while (iterDeviceBySerialNumber.hasNext()) {
							Device device1 = iterDeviceBySerialNumber.next();
							if (device1.getSerialNumber().equals(serialNoForConnectedDevice)) {
								boolean weatherstation = device1.getType().toLowerCase()
										.matches("weather[\\s]*station");
								if (weatherstation) {
									String weatherStationId = device1.getSerialNumber();
									logger.info("WeatherstationId for the gateway:" + gatewayId + " is "
											+ weatherStationId);
									return weatherStationId;
								}
							}

						}
					}
				}
			}
		}
		logger.info(" No weatherstation is associated with this gateway " + gatewayId);
		return "";
	}


}
