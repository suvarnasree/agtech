package com.verizon.agtech.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.verizon.agtech.data.common.data.login.Login;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Thani on 6/23/16.
 *
 */
public class CustomUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private boolean postOnly = true;

    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        if(this.postOnly && !request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        } else if (request.getContentType().equalsIgnoreCase("application/json")){
            String username = null;
            String password = null;

            ObjectMapper objectMapper = new ObjectMapper();
            try {
                Login login = objectMapper.readValue(request.getReader(),Login.class);
                username = login.getUsername();
                password = login.getPassword();
            } catch (IOException e) {
                e.printStackTrace();
            }


            if(username == null) {
                username = "";
            }

            if(password == null) {
                password = "";
            }

            username = username.trim();
            UsernamePasswordAuthenticationToken authRequest = new UsernamePasswordAuthenticationToken(username, password);
            this.setDetails(request, authRequest);
            return this.getAuthenticationManager().authenticate(authRequest);
        } else {
            throw new AuthenticationServiceException("Authentication request ContentType is not JSON " + request.getMethod());
        }
    }
}
