package com.verizon.agtech.data.common.data.identity;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * Created by pusasu5 on 6/15/16.
 */
public class VerificationStatus {

    private boolean status;
    private Date date;


    public VerificationStatus() {

    }

    public VerificationStatus (boolean status,Date date) {
        this.status = status;
        this.date = date;

    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
