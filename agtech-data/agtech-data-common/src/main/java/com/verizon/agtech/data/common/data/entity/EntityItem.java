package com.verizon.agtech.data.common.data.entity;

/**
 * Created by donthi 
 */
public class EntityItem {
	
	    private String blockName;
	    private String deviceType;
	    private String serialNumber;
	    private EntityDetail detail;
	
		public EntityDetail getDetail() {
		return detail;
	    }
	    public void setDetail(EntityDetail detail) {
		this.detail = detail;
	    }
		public String getBlockName() {
			return blockName;
		}
		public void setBlockName(String blockName) {
			this.blockName = blockName;
		}
		public String getDeviceType() {
			return deviceType;
		}
		public void setDeviceType(String deviceType) {
			this.deviceType = deviceType;
		}
		public String getSerialNumber() {
			return serialNumber;
		}
		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}
}
