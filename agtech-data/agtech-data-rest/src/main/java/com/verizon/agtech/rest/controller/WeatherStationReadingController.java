package com.verizon.agtech.rest.controller;

import com.verizon.agtech.data.WeatherStationReadingSender;
import com.verizon.agtech.data.common.data.identity.Device;
import com.verizon.agtech.data.common.data.identity.Entities;
import com.verizon.agtech.data.common.data.weatherstation.WeatherStationData;
import com.verizon.agtech.data.common.data.weatherstation.WeatherStationReadings;
import com.verizon.agtech.data.common.error.ResourceNotFoundException;
import com.verizon.agtech.data.dao.domain.APIKeyDocument;
import com.verizon.agtech.data.dao.domain.CustomerDocument;
import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;
import com.verizon.agtech.data.dao.service.APIKeyService;
import com.verizon.agtech.data.dao.service.CustomerService;
import com.verizon.agtech.data.dao.service.WeatherStationReadingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.*;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;


@Controller
@RequestMapping("v1/customers")
@Api(value = "/v1/customers", tags = "WeatherStation", description = "Weather station readings REST operations")
public class WeatherStationReadingController {
	
	@Autowired
    private HttpServletRequest request;
	
	@Autowired
    private APIKeyService apiKeyService;
	
	@Autowired
    private CustomerService customerService;
	
    @Autowired
    private WeatherStationReadingService weatherStationReadingService;

    @Autowired
    private WeatherStationReadingSender weatherStationReadingSender;

    private final static Logger logger = LoggerFactory.getLogger(WeatherStationReadingService.class);


    @RequestMapping(value = "/{customerId}/weatherStations/events", method = RequestMethod.GET)
    @ApiOperation(value = "Query weather station events", notes = "Events of all weather stations for the given customer and time range.")
    public Model findAllWeatherStationEvents(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "customerId", value = "Customer Id", required = true)
                    String customerId,
            @RequestParam(value = "fromTime", required = false)
            @ApiParam(name = "fromTime", value = "From timestamp in milliseconds", required = false)
                    Long fromTime,
            @RequestParam(value = "toTime", required = false)
            @ApiParam(name = "toTime", value = "To timestamp in milliseconds", required = false)
                    Long toTime,
            @RequestParam(value = "p", required = false, defaultValue = "0")
            @ApiParam(name = "p", value = "Start page number", required = false, defaultValue = "0")
                    Integer page,
            @ApiParam(name = "s", value = "Page size", required = false, defaultValue = "20")
            @RequestParam(value = "s", required = false, defaultValue = "20")
                    Integer size,
            @RequestHeader(value="VINTELUserName") String VINTELUserName,
            @RequestHeader(value="APIKey") String apikey) {
    	logger.info("entering WeatherStationReadingController.findWeatherStationEvents(customerID)");
    	APIKeyDocument document = apiKeyService.findKeyCustomerId(VINTELUserName);
    	if(document == null) {
    		logger.info("APIKeyDocument is null");
    	}
    	if(document!=null && document.getKey() == null) {
    		logger.info("APIKey is null");
    	}
		 if(document != null && document.getKey() != null && apikey != null) {
			 logger.info("VINTELUserName exists in Database");
			 if(document.getKey().trim().equals(apikey.trim())) {
				 logger.info("API-KEY validation successful");
				 DateTime startDateTime = fromTime != null ? new DateTime(fromTime,DateTimeZone.UTC) : new DateTime(0,DateTimeZone.UTC);
				 DateTime endDateTime = toTime != null ? new DateTime(toTime,DateTimeZone.UTC) : new DateTime(DateTimeZone.UTC);
			        logger.info("entering WeatherStationReadingService.findAllWeatherStationEvents()");
			        CustomerDocument customerDocument = customerService.findCustomerByAnyId(customerId);
			        if(customerDocument != null) {
						Entities entities = customerDocument.getEntities();
						List<Device>  devices = entities.getDevices();
						List<WeatherStationReadingDocument> weatherStationReadingDocuments1= new ArrayList<WeatherStationReadingDocument>();
						boolean check = false;
						if(devices.size()>0)
						{
							for(Device device:devices)
							{
								if(device.getType().toLowerCase().matches("weather[\\s]*station")) {
									check = true;
									logger.info("Id" + device.getId());
									Page<WeatherStationReadingDocument> document1 = weatherStationReadingService.findWeatherStationReadingsByIDAndTimeRange(device.getSerialNumber(), startDateTime, endDateTime, page, size);
									logger.info("documentt" + document1);
									if (document1 != null)
										weatherStationReadingDocuments1.addAll(document1.getContent());
								}
							}
							if(check) {
								model.addAttribute("customerId", customerId);
								model.addAttribute("fromTime", fromTime);
								model.addAttribute("toTime", toTime);
								model.addAttribute("events", weatherStationReadingDocuments1.iterator());
								model.addAttribute("page", page);
								model.addAttribute("pageSize", size);
							} else {
								response.setStatus(HttpServletResponse.SC_FORBIDDEN);
								model.addAttribute("cause", "no weather station id is linked to this customer");
							}
						}
						logger.info("exiting WeatherStationReadingService.findAllWeatherStationEvents()");
					} else {
						response.setStatus(HttpServletResponse.SC_FORBIDDEN);
						model.addAttribute("cause", "Customer with id " + customerId + "not found");
					}
			      } else {
				 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				 model.addAttribute("cause", "authentication failed - API KEY is not valid");
			 }
		 } else {
			 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			 model.addAttribute("cause", "VINTEL user is not valid user");
		 }
		 Enumeration headerNames = request.getHeaderNames();
		   	while(headerNames.hasMoreElements()) {
		     		String key =(String) headerNames.nextElement();
		     		String value = request.getHeader(key);
		     		logger.info("key : " + key + " Value : " + value);
		     	}
    	logger.info("exiting WeatherStationReadingController.findWeatherStationEvents(customerID)");
        return model;
    }


    @RequestMapping(value = "/{customerId}/weatherStations/{weatherStationId}/events", method = RequestMethod.GET)
    @ApiOperation(value = "Query weather station events", notes = "Events of a specific weather station for the given customer and time range.")
    public Model findWeatherStationEvents(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "customerId", value = "Customer Id", required = true)
                    String customerId,
            @PathVariable
            @ApiParam(name = "weatherStationId", value = "Weather Station Id", required = true)
                    String weatherStationId,
            @RequestParam(value = "fromTime", required = false)
            @ApiParam(name = "fromTime", value = "From timestamp in milliseconds", required = false)
                    Long fromTime,
            @RequestParam(value = "toTime", required = false)
            @ApiParam(name = "toTime", value = "To timestamp in milliseconds", required = false)
                    Long toTime,
            @RequestParam(value = "p", required = false, defaultValue = "0")
            @ApiParam(name = "p", value = "Start page number", required = false, defaultValue = "0")
                    Integer page,
            @ApiParam(name = "s", value = "Page size", required = false, defaultValue = "20")
            @RequestParam(value = "s", required = false, defaultValue = "20")
                    Integer size ,
                   @RequestHeader(value="VINTELUserName") String VINTELUserName,
                    @RequestHeader(value="APIKey") String apikey){
    	Enumeration headerNames = request.getHeaderNames();
    	  	while(headerNames.hasMoreElements()) {
    	    		String key =(String) headerNames.nextElement();
    	    		String value = request.getHeader(key);
    	    		logger.info("key : " + key + " Value : " + value);
    	    	}
    	logger.info("entering WeatherStationReadingController.findWeatherStationEvents(customerId,weatherStationId)");
    	
    	APIKeyDocument document = apiKeyService.findKeyCustomerId(VINTELUserName);
    	if(document == null) {
    		logger.info("APIKeyDocument is null");
    	}
    	if(document!=null && document.getKey() == null) {
    		logger.info("APIKey is null");
    	}
    	
		if(document != null && document.getKey() != null && apikey != null) {
			logger.info("VINTELUserName exists in Database");
			 if(document.getKey().trim().equals(apikey.trim())) {
				 logger.info("API-KEY validation successful");
				 boolean weatherStationExist = validateWeatherStationRelatedToCustomer(customerId,weatherStationId,response,model);
				 if(weatherStationExist) {
					 DateTime startDateTime = fromTime != null ? new DateTime(fromTime,DateTimeZone.UTC) : new DateTime(0,DateTimeZone.UTC);
					 DateTime endDateTime = toTime != null ? new DateTime(toTime,DateTimeZone.UTC) : new DateTime(DateTimeZone.UTC);
					 logger.info("entering WeatherStationReadingService.findWeatherStationReadingsByIDAndTimeRange(weatherStationId)");
					 Page<WeatherStationReadingDocument> weatherStationReadingDocuments = weatherStationReadingService.findWeatherStationReadingsByIDAndTimeRange(weatherStationId, startDateTime, endDateTime, page, size);
					 logger.info("exiting WeatherStationReadingService.findWeatherStationReadingsByIDAndTimeRange(weatherStationId)");
					 model.addAttribute("customerId", customerId);
					 model.addAttribute("weatherStationId", weatherStationId);
					 model.addAttribute("fromTime", fromTime);
					 model.addAttribute("toTime", toTime);
					 model.addAttribute("events", weatherStationReadingDocuments.iterator());
					 model.addAttribute("page", page);
					 model.addAttribute("pageSize", size);
					 model.addAttribute("pageTotalItems", weatherStationReadingDocuments.getNumberOfElements());
					 model.addAttribute("hasNextPage", weatherStationReadingDocuments.hasNext());
					 model.addAttribute("hasPreviousPage", weatherStationReadingDocuments.hasPrevious());
					 model.addAttribute("totalPages", weatherStationReadingDocuments.getTotalPages());
					 logger.info("existing WeatherStationReadingController.createWeatherStationReadings(customerId,weatherStationId)");
					 return model;
				 } else {
					 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
					 model.addAttribute("cause", "Customer and Weather station are not related/linked");
				 }
			 } else {
				 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				 return model.addAttribute("cause", "authentication failed - API KEY is not valid");
			 }
		 } else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			 return model.addAttribute("cause", "VINTEL user is not valid user");
		 }
		return model;
   }

   

    @RequestMapping(value = "/{customerId}/weatherStations/{weatherStationId}/events", method = RequestMethod.POST)
    @ApiOperation(value = "Inject Weather Station Events", notes = "Inject batch of weather station events.")
    public Model createWeatherStationReadings(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "customerId", value = "Customer Id", required = true)
            String customerId,
            @PathVariable
            @ApiParam(name = "weatherStationId", value = "weather station Id", required = true)
            String weatherStationId,
            @RequestBody
            @ApiParam(name = "events", value = "Weather station events to be injected", required = true)
            WeatherStationReadings weatherStationReadings,
            @RequestHeader(value="api-key") String apikey) throws Exception {
    	logger.info("entering WeatherStationReadingController.createWeatherStationReadings(customerId,weatherStationId)");
    	
    	Enumeration headerNames = request.getHeaderNames();
    	   	while(headerNames.hasMoreElements()) {
    	   		String key =(String) headerNames.nextElement() ;
    	    		String value = request.getHeader(key);
    	    		logger.info("key : " + key + " Value : " + value);
    	    	}
    	
    	if(customerId == null || apikey == null) {
    		logger.info("customerId : " + customerId + " apikey : " + apikey);
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    		return model.addAttribute("cause", "apikey and customer id are required");
    	}
    	
    	if(customerId != null && customerId.length() > 0 && apikey != null && apikey.length() > 0) {
    		logger.info("Received Customer ID : " +customerId + " " + "APIKey " +apikey  );
    		CustomerDocument customerDocument = customerService.findCustomerByCustomerId(customerId);
    		if(customerDocument != null) {
    		logger.info("Customer Document for CustomerId " +customerId + " is returned");
    		logger.info("Collecting the ApikEy from the document is progressing");
    		String apikeyByVintel = customerDocument.getKey();
    		logger.info("Apikey is retrived from cusromer Document : " +apikeyByVintel );
    		if(apikeyByVintel != null && apikeyByVintel.length() > 0) {
    			if(apikeyByVintel.equals(apikey)) {
    				logger.info("entering to send event for weather station");
    				boolean weatherStationExist = validateWeatherStationRelatedToCustomer(customerId,weatherStationId, response,model);
   				 if(weatherStationExist) {
    				for (WeatherStationData weatherStationData : weatherStationReadings.getEvents()) {
						weatherStationData.setWeatherStationID(weatherStationId);
    					weatherStationReadingSender.sendNotification(weatherStationData, WeatherStationData.NAME);
    		        }
    				logger.info("exiting from send event for weather station");
    		        model.addAttribute("customerId", customerId);
    		        model.addAttribute("weatherStationId", weatherStationId);
    		        model.addAttribute("events", weatherStationReadings);
    		        } else {
   					response.setStatus(HttpServletResponse.SC_FORBIDDEN);
					return  model.addAttribute(" cause ", "This customer(Id: " +customerId +") doesn't have WeatherStation (Id:  "+weatherStationId+") ");
				 }
    			} else {
					response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    				return model.addAttribute("cause", "authentication failed - ApiKey is not valid");
    			}
    		}
    		}else {
    			logger.info("customer id does not exit in mongo db" );
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    			model.addAttribute("cause", "Customer id "+customerId+" not found");
    		}
    	}
    	logger.info("existing WeatherStationReadingController.createWeatherStationReadings(customerId,weatherStationId)");
        return model;
    }
    
    private boolean validateWeatherStationRelatedToCustomer(String customerId,String weatherStationId,HttpServletResponse response,Model model) throws ResourceNotFoundException {

 	   CustomerDocument customerDocument = customerService.findCustomerByCustomerId(customerId);
 	   if(customerDocument != null) {
 		   Iterator<Device> iterator = customerDocument.getEntities().getDevices().iterator();
 		   while (iterator.hasNext()) {
 			   Device device = iterator.next();
 			   if (device.getSerialNumber().equalsIgnoreCase(weatherStationId)) {
 				   return true;
 			   }
           }
         } else {
 		   response.setStatus(HttpServletResponse.SC_FORBIDDEN);
 		   model.addAttribute("msg","Customer with Id" + customerId + " is not valid");
      }
 		return false;
 	}

}
