package com.verizon.agtech.data.pusher.job;

import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import static org.quartz.JobBuilder.*;
import static org.quartz.CronScheduleBuilder.*;
import static org.quartz.TriggerBuilder.*;

/**
 * Created by drq on 10/3/15.
 */
public class EON360Scheduler {
    private final static String NAME = "EON360 Job Scheduler";
    private final static Logger logger = LoggerFactory.getLogger(EON360Scheduler.class);

    @Autowired
    private Scheduler scheduler;

    @Value("${job.company}")
    private String company;

    @Value("${job.cronExpression}")
    private String cronExpression;

    @Value("${job.interval}")
    private Integer interval;

    public void stop() {
        if (logger.isDebugEnabled()) {
            logger.debug("Stopping " + NAME);
        }
    }

    public void start() {
        if (logger.isDebugEnabled()) {
            logger.debug("Staring " + NAME);
        }

        try {
            /*
            JobDetail job = newJob(PusherJob.class).usingJobData(OutagePoller.KEY_POLLING_INTERVAL, interval)
                    .usingJobData(OutagePoller.KEY_POLLING_COMPANY, company)
                    .withIdentity(PusherJob.NAME, company)
                    .build();
            */
            //Trigger trigger = newTrigger()
            //        .withIdentity(PusherJob.NAME, company)
            //        .startNow()
            //        .withSchedule(cronSchedule(new CronExpression(cronExpression /*"5 * * * * ?"*/)))
            //        .build();

            // Tell quartz to schedule the job using our trigger
            //scheduler.scheduleJob(job, trigger);

            scheduler.start();
        } catch (Exception ex) {
           logger.error("Failed to schedule job ", ex);
        }
    }
}
