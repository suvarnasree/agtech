package com.verizon.agtech.data.consumer;

import junit.framework.TestCase;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

/**
 * Created by drq on 1/12/15.
 */
@RunWith(JUnit4ClassRunner.class)
@ContextConfiguration({"/messaging-test-context.xml"})
public class MessagingTest extends TestCase {

    private final static Logger logger = LoggerFactory.getLogger(MessagingTest.class);

    @Autowired
    private AmqpTemplate amqpTemplate;

    private String targetQueue = "pons-nms-in";

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void publishMessageTest() {
        MessageProperties properties = new MessageProperties();
        properties.setMessageId(UUID.randomUUID().toString());

        JSONObject jsonObject = new JSONObject();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        TimeZone timeZone = TimeZone.getTimeZone("America/New_York");
        sdf.setTimeZone(timeZone);

        try {
            jsonObject.put("messageType", "collection-event");
            jsonObject.put("dateTime", sdf.format(new Date()));
            jsonObject.put("foo", "bar");
        } catch (JSONException e) {
            logger.error("JSON object prepare error", e);
        }

        Message message = new Message(jsonObject.toString().getBytes(), properties);
        amqpTemplate.send(targetQueue, message);

        Message receivedMessage = amqpTemplate.receive(targetQueue);
        try {
            String messageBody = new String(receivedMessage.getBody(),"UTF-8");
            JSONObject receivedJSONObject = new JSONObject(messageBody);
            logger.info("Message Body:" + messageBody);
            assertEquals("Received message has expect value", receivedJSONObject.get("foo"), "bar");
        } catch (Exception e) {
            logger.error("Failed to encoding the message body", e);
        }
    }
}