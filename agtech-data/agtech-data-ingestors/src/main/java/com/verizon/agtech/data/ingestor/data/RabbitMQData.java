package com.verizon.agtech.data.ingestor.data;

import java.util.Map;

/**
 * Created by drq on 8/23/14.
 */
public class RabbitMQData {
    private Map<String, Object> headers;
    private String payload;

    public RabbitMQData(Map<String, Object> headers, String payload) {
        this.headers = headers;
        this.payload = payload;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}
