package com.verizon.agtech.data.common.data.identity;

import java.util.List;

/**
 * Created by rangapo on 5/29/16.
 */
public class Entities {
    private List<Device>  devices;
    private List<Block> blocks;

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public List<Block> getBlocks() {
        return blocks;
    }

    public void setBlocks(List<Block> blocks) {
        this.blocks = blocks;
    }
}
