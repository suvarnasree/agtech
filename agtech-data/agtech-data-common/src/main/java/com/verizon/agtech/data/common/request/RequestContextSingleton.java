package com.verizon.agtech.data.common.request;

public class RequestContextSingleton {

    private static ThreadLocal requestThreadLocal = new ThreadLocal<RequestContext>();

    public static void reset() {
        requestThreadLocal.remove();
    }

    public static RequestContext getRequestContext() {
        if (null == requestThreadLocal.get()) {
            requestThreadLocal.set(new RequestContext());
        }
        return (RequestContext) requestThreadLocal.get();
    }

    public static void storeContext(RequestContext context) {
        requestThreadLocal.set(context);
    }
}
