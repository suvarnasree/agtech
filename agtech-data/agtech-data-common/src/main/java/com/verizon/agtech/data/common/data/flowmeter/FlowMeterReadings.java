package com.verizon.agtech.data.common.data.flowmeter;

import javax.validation.constraints.NotNull;

/**
 * Created by drq on 4/3/16.
 */
public class FlowMeterReadings {
    @NotNull
    private FlowMeterData[] events;

    public FlowMeterReadings() {
    }

    public FlowMeterReadings(String flowMeterId, FlowMeterData[] readings) {
        this.events = readings;
    }

    public FlowMeterData[] getEvents() {
        return events;
    }

    public void setEvents(FlowMeterData[] events) {
        this.events = events;
    }
}
