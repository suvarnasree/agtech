package com.verizon.agtech.data.common.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No such Resource")
public class ResourceNotFoundException extends  RuntimeException {
    private String id;
    private String type;

    public ResourceNotFoundException(String id, String type,Throwable cause) {
        super(type + " with id " + id + " not found!",cause);
        this.id = id;
        this.type = type;
    }
    public ResourceNotFoundException(String id, String type) {
        super(type + " with id " + id + " not found!");
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
  
    public ResourceNotFoundException(Throwable cause) {
    	
        super(cause.getMessage().replaceAll("*", "testt"));
       
    }
    public void setType(String type) {
        this.type = type;
    }
}
