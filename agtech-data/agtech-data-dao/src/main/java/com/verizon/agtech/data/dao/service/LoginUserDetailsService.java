package com.verizon.agtech.data.dao.service;

import com.verizon.agtech.data.common.error.ResourceNotFoundException;
import com.verizon.agtech.data.dao.domain.UserDocument;
import com.verizon.agtech.data.dao.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * Created by Thani on 6/20/16.
 */
public class LoginUserDetailsService implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(LoginUserDetailsService.class);
    Collection<GrantedAuthority> authorities = Collections.emptySet();

    @Autowired
    private UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        UserDocument userDocument =userRepository.findByUserName(userName);
        User user = null;

        if(userDocument!=null) {
            GrantedAuthority authority = new SimpleGrantedAuthority(userDocument.getRole());
            authorities = Collections.singleton(authority);
            user = new User(userDocument.getUserName(), userDocument.getOneTimePassword(), true, true, true, true, authorities);
            logger.info("User with name " + userName + " is being authenticated by loading the user details from database to cross validate");
        } else {
            throw new ResourceNotFoundException(userName, "User");
        }
        return user;
    }
}
