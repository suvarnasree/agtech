package com.verizon.agtech.data.pusher.delegate;

import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.CollectorDelegate;
import com.verizon.agtech.data.akka.StopWorkException;
import com.verizon.agtech.data.pusher.util.PusherUtils;
import org.quartz.CronExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import java.util.Date;

/**
 * Created by drq on 4/12/14.
 */
public class MailNotifier extends CollectorDelegate {
    public final static String NAME = "Mail Notifier";
    private final static Logger logger = LoggerFactory.getLogger(MailNotifier.class);

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private CronExpression cronExpression;

    private MessageChannel targetChannel;

    public void setTargetChannel(MessageChannel targetChannel) {
        this.targetChannel = targetChannel;
    }

    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (cronExpression.isSatisfiedBy(new Date())) {
            if (logger.isDebugEnabled()) {
                logger.debug(messageSource.getMessage("delegate.entering", new Object[]{NAME}, null));
            }

            /*
            if (data instanceof Outages) {
                Outages outages = (Outages) data;
                byte[] bytes = outages.getFormattedOutages().getBytes();

                String fileName = PusherUtils.getNotificationFileName(outages.getJobDetails());
                String subject = PusherUtils.getNotificationSubject(outages);

                Message<byte[]> message = MessageBuilder.withPayload(bytes)
                        .setHeader("fileName", fileName)
                        .setHeader("subject", subject).build();

                if (targetChannel != null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug(String.format("Sending message to Channel %s", targetChannel.toString()));
                    }
                    try {
                        targetChannel.send(message);
                    } catch (Exception ex) {
                        logger.error("Failed to send mail", ex);
                    }
                }
                if (logger.isDebugEnabled()) {
                    logger.debug(messageSource.getMessage("delegate.leaving", new Object[]{NAME}, null));
                }

                return data;
            } else {
                throw new StopWorkException(messageSource.getMessage("delegate.invalidData", new Object[]{NAME, Outages.class.getCanonicalName(), data.getClass().getCanonicalName()}, null));
            }
            */
            return data;
        } else {
            return data;
        }
    }
}
