package com.verizon.agtech.data.consumer.delegate;

import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.CollectorDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by drq on 4/17/14.
 */
public class BusDataProcessor extends CollectorDelegate {

    private final static Logger logger = LoggerFactory.getLogger(BusDataProcessor.class);

    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering Bus Data Processor....");
        }

        if (logger.isDebugEnabled()) {
            logger.debug("Leaving Bus Data Processor....");
        }
        return data;
    }
}
