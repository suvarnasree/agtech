package com.verizon.agtech.data.dao.domain;

import javax.validation.constraints.NotNull;

/**
 * Created by donthi 
 */

public class CatalogItem {
    @NotNull
    private String modelNumber;

    @NotNull
    private String manufacturer;
 
    @NotNull
    private String type;
    
  public CatalogItem(String modelNumber, String manufacturer, String type) {
		super();
		this.modelNumber = modelNumber;
		this.manufacturer = manufacturer;
		this.type = type;
	}

	public String getModelNumber() {
		return modelNumber;
	}

	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}

	public String getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}


}
