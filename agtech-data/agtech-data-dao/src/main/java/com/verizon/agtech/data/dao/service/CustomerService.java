package com.verizon.agtech.data.dao.service;

import com.verizon.agtech.data.common.data.identity.Customer;
import com.verizon.agtech.data.dao.domain.CustomerDocument;
import com.verizon.agtech.data.dao.repository.CustomerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by drq on 2/3/14.
 */
@Repository
@Transactional
public class CustomerService {

    private final static Logger logger = LoggerFactory.getLogger(CustomerService.class);

    @Autowired
    private CustomerRepository customerRepository;

    public CustomerDocument findCustomerById(String id) {
        return customerRepository.findById(id);
    }
  
    public CustomerDocument findCustomerByGuid(String guid) {
        return customerRepository.findByGuid(guid);
    }

    public CustomerDocument findCustomerByCustomerId(String customerId) {
    	logger.info("Processing the document fetching from CustomerService");
        return customerRepository.findById(customerId);
       
    }

    public CustomerDocument findCustomerByAnyId(String id) {
        CustomerDocument    customerDocument = customerRepository.findByGuid(id);
        if (customerDocument == null) {
            customerDocument = customerRepository.findById(id);
        }
        return customerDocument;
    }

    public CustomerDocument findCustomerByEcpdId(String EcpdId) {
        CustomerDocument document = customerRepository.findByEcpdId(EcpdId);
        return document;
    }

    public CustomerDocument findAccountByAnyName(String id) {
        CustomerDocument customerDocument = customerRepository.findByAccountName(id);
        if (customerDocument == null) {
            customerDocument = customerRepository.findByGuid(id);
        }
        if (customerDocument == null) {
            customerDocument = customerRepository.findById(id);
        }
        return customerDocument;
    }

    public Page<CustomerDocument> findCustomerDocuments(int page, int size) {
        return customerRepository.findAll(new PageRequest(page, size));
    }

    public CustomerDocument saveCustomerDocument(CustomerDocument customerDocument) {
        return customerRepository.save(customerDocument);
    }

    public Boolean removeCustomerDocument(String customerId) {
        CustomerDocument customerDocumentDocument = this.findCustomerById(customerId);
        customerRepository.delete(customerDocumentDocument);
        return this.findCustomerById(customerId) == null;
    }

    public Boolean removeCustomerDocument(CustomerDocument customerDocument) {
        String customerId = customerDocument.getId();
        customerRepository.delete(customerDocument);
        return this.findCustomerById(customerId) == null;
    }

    public Page<CustomerDocument> findCustomers(int page, int size) {
        return customerRepository.findAll(new PageRequest(page, size));
    }

    public CustomerDocument updateCustomerDocument(CustomerDocument customerDocument, Customer customer) {
        return customerRepository.save(customerDocument);
    }
}
