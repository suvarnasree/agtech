package com.verizon.agtech.data.akka;

import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.japi.Creator;

/**
 * Created by drq on 4/11/14.
 */
public class CollectorActor extends UntypedActor {

    private CollectorDataHandler dataHandler;

    public static Props props(final CollectorDataHandler dataHandler) {
        return Props.create(new Creator<CollectorActor>() {
            private static final long serialVersionUID = 1L;

            @Override
            public CollectorActor create() throws Exception {
                return new CollectorActor(dataHandler);
            }
        });
    }

    public CollectorActor(CollectorDataHandler dataHandler) {
        this.dataHandler = dataHandler;
    }

    @Override
    public void onReceive(Object message) throws Exception {
        this.dataHandler.handle(message, this);
    }
}
