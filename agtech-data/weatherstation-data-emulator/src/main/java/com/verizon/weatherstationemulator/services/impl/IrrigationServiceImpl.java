package com.verizon.weatherstationemulator.services.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.verizon.weatherstation.emulator.services.IrrigationService;

public class IrrigationServiceImpl implements IrrigationService {
	

	  private static final Map<String, Integer> IRRIGATION_CACHE = new HashMap();
	  
	  public int start(String weatherStationId)
	  {
	    IRRIGATION_CACHE.put(weatherStationId, Integer.valueOf(0));
	    
	    return 0;
	  }
	  
	  public int get(String weatherStationId)
	  {
	    return IRRIGATION_CACHE.containsKey(weatherStationId) ? ((Integer)IRRIGATION_CACHE.get(weatherStationId)).intValue() : 0;
	  }
	  
	  public int update(String weatherStationId)
	  {
	    int currentValue = get(weatherStationId);
	    
	    int updatedValue = currentValue + new Random().nextInt(200);
	    
	    IRRIGATION_CACHE.put(weatherStationId, Integer.valueOf(updatedValue));
	    
	    return updatedValue;
	  }
	  
	  public int end(String weatherStationId)
	  {
	    int currentValue = get(weatherStationId);
	    
	    IRRIGATION_CACHE.put(weatherStationId, Integer.valueOf(0));
	    
	    return currentValue;
	  }

}
