package com.verizon.agtech.data.common.domain;

import org.springframework.data.mongodb.core.index.Indexed;

import java.util.UUID;

/**
 * Created by drq on 8/23/14.
 */
public class AbstractAgTechDocument extends AbstractDocument{
    @Indexed
    private String guid;

    public AbstractAgTechDocument() {
        this.guid = UUID.randomUUID().toString();
    }

    public String getGuid() {
        return guid;
    }
}
