package com.verizon.agtech.data.akka;

/**
 * Created by drq on 4/11/14.
 */
public interface CollectorDataHandler {
    public Object handle(Object data, CollectorActor sender);
}
