package com.verizon.agtech.rest.controller;

import com.verizon.agtech.data.common.data.catalog.Catalog;
import com.verizon.agtech.data.common.data.catalog.DeviceModel;
import com.verizon.agtech.data.common.error.ResourceExistsException;
import com.verizon.agtech.data.common.error.ResourceNotFoundException;
import com.verizon.agtech.data.dao.domain.CatalogDocument;
import com.verizon.agtech.data.dao.domain.CatalogItem;
import com.verizon.agtech.data.dao.domain.CustomerDocument;
import com.verizon.agtech.data.dao.service.CatalogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by donthi
 */
@Controller
@RequestMapping("v1/catalogs")
@Api(value = "/v1/catalogs", tags = "Catalog", description = "Catalog REST operations")
public class CatalogController {
    @Autowired
    private CatalogService catalogService;

    private final static Logger logger = LoggerFactory.getLogger(CatalogController.class);

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "List catalogs", notes = "Returns the current list of catalogs")
    public Model findAllCatalogDocuments(
            @ApiIgnore
            final Model model,
            @PathVariable
            @RequestParam(value = "p", required = false, defaultValue = "0")
            @ApiParam(name = "p", value = "Start page number", required = false, defaultValue = "0")
                    Integer page,
            @ApiParam(name = "s", value = "Page size", required = false, defaultValue = "200")
            @RequestParam(value = "s", required = false, defaultValue = "200")
                    Integer size) {
    	logger.info("entering CatalogController.findAllCatalogDocuments()");
    	logger.info("entering catalogService.findAllCatalogDocuments()");
        Page<CatalogDocument> catalogs = catalogService.findCatalogs(page, size);
        logger.info("exiting catalogService.findAllCatalogDocuments()");

        model.addAttribute("catalogs", catalogs.iterator());
        model.addAttribute("page", page);
        model.addAttribute("pageSize", size);
        model.addAttribute("pageTotalItems", catalogs.getNumberOfElements());
        model.addAttribute("hasNextPage", catalogs.hasNext());
        model.addAttribute("hasPreviousPage", catalogs.hasPrevious());
        model.addAttribute("totalPages", catalogs.getTotalPages());
        logger.info("exiting CatalogController.findAllCatalogDocuments()");
        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create catalog", notes = "Creates a catalog entry.")
    public Model createCatalogDocument(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @RequestBody
            @ApiParam(name = "catalog", value = "Catalog", required = true)
                    Catalog catalog) {
    	logger.info("entering CatalogController.createCatalogDocument()");
    	logger.info("entering catalogService.findByCatalogName()");
        CatalogDocument catalogDocument = catalogService.findByCatalogName(catalog.getName());
        logger.info("exiting catalogService.findByCatalogName()");
        if(catalogDocument == null) {
        	logger.info("catalog document is null due to catalog name does not exist");
        	generateId(catalog);
            catalogDocument = new CatalogDocument();
            catalogDocument.setName(catalog.getName());
            catalogDocument.setDeviceModels(catalog.getDeviceModels());
            logger.info("entering catalogService.saveCatalogDocument(catalogDocument)");
            catalogDocument = catalogService.saveCatalogDocument(catalogDocument);
            logger.info("exiting catalogService.saveCatalogDocument(catalogDocument)");
            model.addAttribute("catalog", catalogDocument);
        } else {
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return  model.addAttribute("cause", "catalogName   "+catalog.getName()+" already exists");
          }
        logger.info("exiting CatalogController.saveCatalogDocument(catalogDocument)");
        return model;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "Update catalog", notes = "Updates a catalog entry.")
    public Model updateCatalogDocument(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "id", value = "Catalog Id", required = true) String id,
            @RequestBody
            @ApiParam(name = "catalog", value = "Catalog", required = true)
                    Catalog catalog) {
    	logger.info("entering CatalogController.updateCatalogDocument(catalogID)");
    	logger.info("entering catalogService.findCatalogByAnyId(id)");
       
        CatalogDocument catalogDocument = catalogService.findCatalogById(id);
        if (catalogDocument != null && catalogDocument.getName().equals(catalog.getName())){
        	updateCatalog(catalog, catalogDocument, model);
        }else if(catalogDocument != null && catalogDocument.getName() != catalog.getName()) {
        	CatalogDocument catalogDocument1 = catalogService.findByCatalogName(catalog.getName());
        	if (catalogDocument1 != null) {
        		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        		return model.addAttribute("cause", "Catalog  Name "+catalog.getName()+"  already exists");
            }else {
            	updateCatalog(catalog, catalogDocument, model);
            }
        }
        else {
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return  model.addAttribute("cause", "Catalog not found");
        }
        	return model;
    }
    
    private void updateCatalog(Catalog catalog,CatalogDocument catalogDocument, final Model model) {
    	generateId(catalog);
        catalogDocument.setName(catalog.getName());
        catalogDocument.setDeviceModels(catalog.getDeviceModels());
        logger.info("entering catalogService.saveCatalogDocument(catalogDocument)");
        catalogDocument = catalogService.saveCatalogDocument(catalogDocument);
        logger.info("exiting catalogService.saveCatalogDocument(catalogDocument)");
        model.addAttribute("catalog", catalogDocument);
        logger.info("exiting CatalogController.updateCatalogDocument(catalogID)");
        
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Query catalog details", notes = "Details of a specific catalog for the given catalog id.")
    public Model getCatalogDetailsById(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "id", value = "catalog Id", required = true) String id) {
    	logger.info("entering CatalogController.getCatalogDetailsById(catalogID)");
    	logger.info("entering catalogService.findCatalogByAnyId(catalogID)");
        CatalogDocument catalogDocument = catalogService.findCatalogByAnyId(id);
        logger.info("exiting catalogService.findCatalogByAnyId(catalogID)");
        if(catalogDocument != null){
            model.addAttribute("catalogDocuments", catalogDocument);
        }
        else {
        	logger.info("catalog document is null due to catalog id is not found in mongo db");
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            return model.addAttribute("cause", "catalog   "+id+" not found ");
        }
        logger.info("exiting CatalogController.getCatalogDetailsById(catalogDocument)");
        return model;
    }
    private void generateId(Catalog catalog) {
        if (catalog == null) return;
        List<DeviceModel> deviceModels = catalog.getDeviceModels();
        if (deviceModels != null && !deviceModels.isEmpty()) {
            for (DeviceModel deviceModel : deviceModels) {
                if (deviceModel.getId() == null || deviceModel.getId().isEmpty()) {
                    deviceModel.setId(new ObjectId().toString());
                }
            }
        }
    }

}
