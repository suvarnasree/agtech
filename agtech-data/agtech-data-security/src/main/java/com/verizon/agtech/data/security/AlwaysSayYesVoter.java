package com.verizon.agtech.data.security;

/**
 * Created by IntelliJ IDEA.
 * User: drq
 * Date: 10/18/13
 * Time: 9:20 PM
 * To change this template use File | Settings | File Templates.
 */

import com.verizon.agtech.data.common.request.RequestContextSingleton;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.access.AccessDecisionVoter;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.FilterInvocation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class AlwaysSayYesVoter implements AccessDecisionVoter<FilterInvocation> {

    private static Log logger = LogFactory.getLog(AlwaysSayYesVoter.class);

    private List<String> allowedAttributes = Arrays.asList("ROOT_AUTHORIZED", "CUSTOMER_AUTHORIZED");

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        if (logger.isDebugEnabled()) {
            logger.debug("Check access " + configAttribute.getAttribute() + " " + allowedAttributes.contains(configAttribute.getAttribute()));
        }
        return allowedAttributes.contains(configAttribute.getAttribute());
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return FilterInvocation.class.isAssignableFrom(aClass);
    }

    @Override
    @ProvenanceSecurity(voter = "always-say-yes")
    public int vote(Authentication authentication, FilterInvocation filterInvocation, Collection<ConfigAttribute> configAttributes) {
        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Granting access to [ %s ] for [ %s ]", filterInvocation.getFullRequestUrl(), authentication.toString()));
        }
        List<String> userAuthorities = new ArrayList<String>();

        for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
            if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
                userAuthorities.add("ROOT_AUTHORIZED");
            }
            if (grantedAuthority.getAuthority().equalsIgnoreCase("SALES") || grantedAuthority.getAuthority().equalsIgnoreCase("DEPLOYER") || grantedAuthority.getAuthority().equalsIgnoreCase("ADMIN")) {
                userAuthorities.add("CUSTOMER_AUTHORIZED");
            }
        }

        for (ConfigAttribute configAttribute : configAttributes) {
            String requiredAuthority = configAttribute.toString();
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("config attribute: %s", requiredAuthority));
            }
            if (!userAuthorities.contains(requiredAuthority)) {
                return AccessDecisionVoter.ACCESS_DENIED;
            }
        }
        Object principal = authentication.getPrincipal();
        if (principal != null && principal instanceof User) {
            principal = ((User) principal).getUsername();
            if (logger.isDebugEnabled()) {
                logger.info("Principal " + principal);
            }
        }
        String user = principal.toString();
        RequestContextSingleton.getRequestContext().setRequestUser(user);
        return AccessDecisionVoter.ACCESS_GRANTED;
    }
}
