package com.verizon.weatherstationemulator.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

//import com.verizon.agtech.data.common.data.Measurement;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmulatorEvent {

	private String weatherStationID;
	private Date timeStamp;
	private Measurement windSpeed;
	private Measurement relativeHumidity;
	private Measurement totalPrecipitation;
	private Measurement solarRadiation;

	public EmulatorEvent(String weatherStationID, Date timeStamp, Measurement windSpeed, Measurement relativeHumidity,
			Measurement totalPrecipitation, Measurement solarRadiation) {
		super();
		this.weatherStationID = weatherStationID;
		this.timeStamp = timeStamp;
		this.windSpeed = windSpeed;
		this.relativeHumidity = relativeHumidity;
		this.totalPrecipitation = totalPrecipitation;
		this.solarRadiation = solarRadiation;
	}

	public String getWeatherStationID() {
		return weatherStationID;
	}

	public void setWeatherStationID(String weatherStationID) {
		this.weatherStationID = weatherStationID;
	}

	public Date getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}

	public Measurement getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(Measurement windSpeed) {
		this.windSpeed = windSpeed;
	}

	public Measurement getRelativeHumidity() {
		return relativeHumidity;
	}

	public void setRelativeHumidity(Measurement relativeHumidity) {
		this.relativeHumidity = relativeHumidity;
	}

	public Measurement getTotalPrecipitation() {
		return totalPrecipitation;
	}

	public void setTotalPrecipitation(Measurement totalPrecipitation) {
		this.totalPrecipitation = totalPrecipitation;
	}

	public Measurement getSolarRadiation() {
		return solarRadiation;
	}

	public void setSolarRadiation(Measurement solarRadiation) {
		this.solarRadiation = solarRadiation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((relativeHumidity == null) ? 0 : relativeHumidity.hashCode());
		result = prime * result + ((solarRadiation == null) ? 0 : solarRadiation.hashCode());
		result = prime * result + ((timeStamp == null) ? 0 : timeStamp.hashCode());
		result = prime * result + ((totalPrecipitation == null) ? 0 : totalPrecipitation.hashCode());
		result = prime * result + ((weatherStationID == null) ? 0 : weatherStationID.hashCode());
		result = prime * result + ((windSpeed == null) ? 0 : windSpeed.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmulatorEvent other = (EmulatorEvent) obj;
		if (relativeHumidity == null) {
			if (other.relativeHumidity != null)
				return false;
		} else if (!relativeHumidity.equals(other.relativeHumidity))
			return false;
		if (solarRadiation == null) {
			if (other.solarRadiation != null)
				return false;
		} else if (!solarRadiation.equals(other.solarRadiation))
			return false;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		if (totalPrecipitation == null) {
			if (other.totalPrecipitation != null)
				return false;
		} else if (!totalPrecipitation.equals(other.totalPrecipitation))
			return false;
		if (weatherStationID == null) {
			if (other.weatherStationID != null)
				return false;
		} else if (!weatherStationID.equals(other.weatherStationID))
			return false;
		if (windSpeed == null) {
			if (other.windSpeed != null)
				return false;
		} else if (!windSpeed.equals(other.windSpeed))
			return false;
		return true;
	}

	public static class EmulatorEventBuilder {

		private String weatherStationID;
		private Date timeStamp;
		private Measurement windSpeed;
		private Measurement relativeHumidity;
		private Measurement totalPrecipitation;
		private Measurement solarRadiation;

		public EmulatorEventBuilder timeStamp(Date timeStamp) {
			this.timeStamp = timeStamp;
			return this;
		}

		public EmulatorEventBuilder weatherStationID(String weatherStationID) {
			this.weatherStationID = weatherStationID;
			return this;
		}

		public EmulatorEventBuilder windSpeed(Measurement windSpeed) {
			this.windSpeed = windSpeed;
			return this;
		}

		public EmulatorEventBuilder relativeHumidity(Measurement relativeHumidity) {
			this.relativeHumidity = relativeHumidity;
			return this;
		}

		public EmulatorEventBuilder totalPrecipitation(Measurement totalPrecipitation) {
			this.totalPrecipitation = totalPrecipitation;
			return this;
		}

		public EmulatorEventBuilder solarRadiation(Measurement solarRadiation) {
			this.solarRadiation = solarRadiation;
			return this;
		}

		/*
		 * public EmulatorEventBuilder type(String type) { this.type = type;
		 * return this; }
		 */

		public EmulatorEvent build() {
			return new EmulatorEvent(this.weatherStationID, this.timeStamp, this.windSpeed, this.relativeHumidity,
					this.totalPrecipitation, this.solarRadiation);
		}
	}
}