package com.verizon.agtech.data.provenance.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation for controlling which operations are reported to the Provenance engine.
 */
@java.lang.annotation.Retention(RetentionPolicy.RUNTIME)
@java.lang.annotation.Target({ElementType.METHOD})
public @interface ProvenanceOperation
{
    String operationType();

    String operationClass();
}
