package com.verizon.agtech.rest;

/**
 * Created by drq on 3/25/15.
 */

import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.orm.ObjectRetrievalFailureException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

@ControllerAdvice
public class DefaultExceptionHandler {

    private final static Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @ExceptionHandler({HttpMessageNotReadableException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleBadRequestException(HttpMessageNotReadableException ex) {
        logger.error("BAD_REQUEST", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Bad JSON data");
        map.put("cause", getExceptionMessage(ex));
        return map;
    }

    @ExceptionHandler({MissingServletRequestParameterException.class,
            UnsatisfiedServletRequestParameterException.class,
            HttpRequestMethodNotSupportedException.class,
            ServletRequestBindingException.class
    })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleRequestException(Exception ex) {
        logger.error("BAD_REQUEST", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Request Error");
        map.put("cause", getExceptionMessage(ex));
        return map;
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleValidationException(ConstraintViolationException ex) throws IOException {
        logger.error("BAD_REQUEST", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Validation Failure");
        map.put("violations", convertConstraintViolation(ex.getConstraintViolations()));
        return map;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String, Object> handleValidationException(MethodArgumentNotValidException ex) throws IOException {
        logger.error("BAD_REQUEST", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Validation Failure");
        map.put("violations", convertConstraintViolation(ex));
        return map;
    }

    @ExceptionHandler(ObjectRetrievalFailureException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public Map<String, Object> handleValidationException(ObjectRetrievalFailureException ex) throws IOException {
        logger.error("NOT_FOUND", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Entity Not Found");
        map.put("cause", getExceptionMessage(ex));
        return map;
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT)
    @ResponseBody
    public Map<String, Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex) throws IOException {
        logger.error("CONFLICT", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Data Integrity Error");
        map.put("cause", getExceptionMessage(ex));
        return map;
    }

    @ExceptionHandler(DataAccessException.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Map<String, Object> handleDataAccessException(DataAccessException ex) throws IOException {
        logger.error("DataAccessException", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Data Error");
        map.put("cause", getExceptionMessage(ex));
        return map;
    }

    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    @ResponseStatus(value = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ResponseBody
    public Map<String, Object> handleUnsupportedMediaTypeException(HttpMediaTypeNotSupportedException ex) throws IOException {
        logger.error("UNSUPPORTED_MEDIA_TYPE", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Unsupported Media Type");
        map.put("cause", ex.getLocalizedMessage());
        map.put("supported", ex.getSupportedMediaTypes());
        return map;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Map<String, Object> handleUncaughtException(Exception ex) throws IOException {
        logger.error("UncaughtException", ex);
        Map<String, Object> map = Maps.newHashMap();
        map.put("error", "Unknown Error");
        map.put("cause", getExceptionMessage(ex));
        return map;
    }

    private Map<String, Map<String, Object>> convertConstraintViolation(Set<ConstraintViolation<?>> constraintViolations) {
        Map<String, Map<String, Object>> result = Maps.newHashMap();
        for (ConstraintViolation constraintViolation : constraintViolations) {
            Map<String, Object> violationMap = Maps.newHashMap();
            violationMap.put("value", constraintViolation.getInvalidValue());
            violationMap.put("type", constraintViolation.getRootBeanClass());
            violationMap.put("message", constraintViolation.getMessage());
            result.put(constraintViolation.getPropertyPath().toString(), violationMap);
        }
        return result;
    }

    private Map<String, Map<String, Object>> convertConstraintViolation(MethodArgumentNotValidException ex) {
        Map<String, Map<String, Object>> result = Maps.newHashMap();
        for (ObjectError error : ex.getBindingResult().getAllErrors()) {
            Map<String, Object> violationMap = Maps.newHashMap();
            violationMap.put("target", ex.getBindingResult().getTarget());
            violationMap.put("type", ex.getBindingResult().getTarget().getClass());
            violationMap.put("message", error.getDefaultMessage());
            result.put(error.getObjectName(), violationMap);
        }
        return result;
    }

    private String getExceptionMessage(Exception ex) {
        try {
            String cause;
            if (ex.getCause() != null) {
                cause = ex.getCause().getMessage();
            } else {
                cause = ex.getMessage();
            }
            return cause != null ? cause : "";
        } catch (Exception e) {
            return "";
        }
    }
}
