package com.verizon.agtech.data.ingestor.data;

/**
 * Created by drq on 8/19/14.
 */
public interface ProtoData {
    public byte[] getBytes();
}
