package com.verizon.agtech.data.consumer;

import junit.framework.TestCase;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by drq on 1/12/15.
 */
@RunWith(JUnit4ClassRunner.class)
@ContextConfiguration({"/xmpp-receive-test-context.xml"})
public class MessageConsumerTest extends TestCase {

    private final static Logger logger = LoggerFactory.getLogger(MessageConsumerTest.class);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void consumerMessageTest() {
        logger.info("Receiving messages...");
        try {
            Thread.sleep(600000);
            logger.info("End of test.");
        } catch (InterruptedException e) {
            logger.error("Thread error", e);
        }
    }
}