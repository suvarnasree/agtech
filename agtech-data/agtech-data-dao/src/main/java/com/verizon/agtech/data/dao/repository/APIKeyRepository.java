package com.verizon.agtech.data.dao.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.verizon.agtech.data.dao.domain.APIKeyDocument;

public interface APIKeyRepository extends PagingAndSortingRepository<APIKeyDocument, Long>, APIKeyRepositoryCustom{

	APIKeyDocument save(APIKeyDocument catalog);
	APIKeyDocument findByCustomerId(String CustomerId);
}
