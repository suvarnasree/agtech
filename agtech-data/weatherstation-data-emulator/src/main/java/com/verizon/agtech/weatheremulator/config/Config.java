package com.verizon.agtech.weatheremulator.config;

import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import com.verizon.weatherstation.emulator.services.IrrigationService;
import com.verizon.weatherstation.emulator.services.SensorProvider;
import com.verizon.weatherstationemulator.model.WeatherStationInformationRequest;
import com.verizon.weatherstationemulator.services.impl.IrrigationProviderImpl;
import com.verizon.weatherstationemulator.services.impl.IrrigationServiceImpl;

@Configuration
public class Config extends TimerTask {
	private static final Logger LOGGER = LoggerFactory.getLogger(Config.class);
	@Value("${gateway.url}")
	private String gatewayUrl;
	
	
	@Value("${weather.reading.time}")
	private String weatherReadingTime;
	@Value("${weatherstation.id}")
	private String flowMeterIds;
	@Value("${login}")
	private String login;
	@Value("${password}")
	private String password;
	
	/*@Bean
	public SensorProvider irrigation()
	  {
	    return new IrrigationProviderImpl();
	  }
	
	@Bean
	  public IrrigationService irrigationService()
	  {
	    return new IrrigationServiceImpl();
	  }*/
	  
	
	public void Config() {
		Config runner = new Config();
		String time = weatherReadingTime;
		if (time != null && time.length() > 0) {
			Integer timeInMinutes = Integer.parseInt(time);
			Timer timer = new Timer();
			timer.scheduleAtFixedRate(runner, 0, timeInMinutes * 60 * 1000);
		} else {
			runner.run();
		}
	}
	
	public void run() {
		//System.out.println("_____$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$Contol reaching here");
		RestTemplate restTemplate = new RestTemplate();
		WeatherStationInformationRequest weatherInfo = restTemplate.getForObject(gatewayUrl,
				WeatherStationInformationRequest.class);
		//write the logic to set data into database
		
	}

}
