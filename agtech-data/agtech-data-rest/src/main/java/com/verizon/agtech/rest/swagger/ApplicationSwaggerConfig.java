package com.verizon.agtech.rest.swagger;

/**
 * Created by drq on 3/23/16.
 */

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableWebMvc
@EnableSwagger2
public class ApplicationSwaggerConfig extends WebMvcConfigurerAdapter {
    @Bean
    public Docket restfulApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("restful-api")
                .select()
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {

        ApiInfo apiInfo = new ApiInfo(
                "AgTech API",
                "AgTech REST API for Flow Meter and Weather Station",
                "1.0",
                "AgTech API terms of service",
                "agtech@verizon.com",
                "Commercial License",
                "http://www.verizon.com"
        );
        return apiInfo;
    }
}