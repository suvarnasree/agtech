package com.verizon.agtech.data.ingestor.parser;

import com.verizon.agtech.data.common.parser.DataParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created by drq on 8/18/14.
 */
public class TimestampParser implements DataParser {

    private final static Logger logger = LoggerFactory.getLogger(TimestampParser.class);

    public final static String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private SimpleDateFormat simpleDateFormat;

    public TimestampParser() {
        this.simpleDateFormat = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT);
        TimeZone timeZone = TimeZone.getTimeZone("America/New_York");
        this.simpleDateFormat.setTimeZone(timeZone);
    }

    public TimestampParser(SimpleDateFormat simpleDateFormat) {
        this.simpleDateFormat = simpleDateFormat;
        TimeZone timeZone = TimeZone.getTimeZone("America/New_York");
        this.simpleDateFormat.setTimeZone(timeZone);
    }

    @Override
    public Object parseData(Object data) {
        try {
            if (data != null && !data.toString().trim().equals("")) {
                return simpleDateFormat.parse(data.toString());
            } else {
                return null;
            }
        } catch (ParseException e) {
            logger.error("Failed to parse date string " + data, e);
            return null;
        }
    }
}
