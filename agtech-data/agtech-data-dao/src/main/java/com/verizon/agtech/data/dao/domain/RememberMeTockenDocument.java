package com.verizon.agtech.data.dao.domain;

import com.verizon.agtech.data.common.domain.AbstractAgTechDocument;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

import java.util.Date;

/**
 * Created by Thani on 6/20/16.
 */
public class RememberMeTockenDocument extends AbstractAgTechDocument {

    private String userName;
    private String series;
    private String tokenValue;
    private Date date;


    public RememberMeTockenDocument() {

    }


    public RememberMeTockenDocument(String username,String series,String tokenValue,Date date) {
        this.userName = username;
        this.series = series;
        this.tokenValue = tokenValue;
        this.date = date;
    }


    public String getUsername() {
        return userName;
    }

    public void setUsername(String username) {
        this.userName = username;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
