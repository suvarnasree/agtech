package com.verizon.agtech.data.dao.domain;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.verizon.agtech.data.common.data.Measurement;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by drq on 3/31/16.
 */
@Document(collection = "flow-meter-readings")
@CompoundIndexes({
        @CompoundIndex(name = "timeStamp_flowMeterId_type_1", def = "{ 'timeStamp': 1, 'flowMeterId' : 1, 'type' : 1}")
})
public class FlowMeterReadingDocument extends AbstractAgTechReadingDocument {
    @NotNull
    private String flowMeterId;

    @NotNull
    private Measurement waterAmount;

    private Measurement energyAvailable;

    @NotNull
    private String type;

    public FlowMeterReadingDocument() {
    }

    public FlowMeterReadingDocument(Date timeStamp, String flowMeterId, Measurement waterAmount, Measurement energyAvailable, String type) {
        super(timeStamp);
        this.flowMeterId = flowMeterId;
        this.energyAvailable = energyAvailable;
        this.waterAmount = waterAmount;
        this.type = type;
    }

    public String getFlowMeterId() {
        return flowMeterId;
    }

    public void setFlowMeterId(String flowMeterId) {
        this.flowMeterId = flowMeterId;
    }

    public Measurement getWaterAmount() {
        return waterAmount;
    }

    public void setWaterAmount(Measurement waterAmount) {
        this.waterAmount = waterAmount;
    }

    public Measurement getEnergyAvailable() {
        return energyAvailable;
    }

    public void setEnergyAvailable(Measurement energyAvailable) {
        this.energyAvailable = energyAvailable;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
