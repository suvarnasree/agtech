package com.verizon.agtech.data.ingestor.delegate;

import com.verizon.CDFProtos;
import com.verizon.agtech.AgTechCDFProtos;
import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.StopWorkException;
import com.verizon.agtech.data.common.data.AgTechDateTime;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReading;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReadingType;
import com.verizon.agtech.data.ingestor.data.AgTechProtoData;
import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by drq on 4/12/16.
 */
public class FlowMeterReadingProtoBuilder extends IngestorDelegate {
    public final static String NAME = "FLOW_METER_READING_PROTO_BUILDER";

    private final static Logger logger = LoggerFactory.getLogger(FlowMeterReadingProtoBuilder.class);

    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (logger.isDebugEnabled()) {
            logger.debug(messageSource.getMessage("delegate.entering", new Object[]{NAME}, null));
        }

        if (data != null && data instanceof FlowMeterReading) {
            FlowMeterReading flowMeterReading = (FlowMeterReading) data;

            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Flow Meter Reading: %s", flowMeterReading.toString()));
            }

            // Set up proto collection builder
            CDFProtos.Collection.Builder collectionBuilder = CDFProtos.Collection.newBuilder();

            // Set up collection basic info
            String collectionGuid = UUID.randomUUID().toString();
            collectionBuilder.setGuid(collectionGuid);
            collectionBuilder.setType("flat");
            collectionBuilder.setMode(CDFProtos.Collection.CollectionMode.STATIC);
            collectionBuilder.setStatus(CDFProtos.Collection.Status.GOOD);
            collectionBuilder.setFriendlyName("");
            collectionBuilder.setTriageMode(CDFProtos.Collection.TriageMode.NONE);
            collectionBuilder.setTriageValue("");
            collectionBuilder.setParentGuid("");

            // Set up data blob builder
            CDFProtos.DataBlob.Builder dataBlobBuilder = CDFProtos.DataBlob.newBuilder();
            dataBlobBuilder.setGuid(UUID.randomUUID().toString());
            dataBlobBuilder.setFriendlyName("agtech");
            dataBlobBuilder.setDataType(CDFProtos.DataBlob.Type.MEASUREMENT);

            AgTechCDFProtos.FlowMeterReading.Builder flowMeterReadingBuilder = AgTechCDFProtos.FlowMeterReading.newBuilder();

            FlowMeterReadingType flowMeterReadingType = flowMeterReading.getType();
            switch (flowMeterReadingType) {
                case IRRIGATION_START:
                    flowMeterReadingBuilder.setType(AgTechCDFProtos.FlowMeterReading.Type.IRRIGATION_START);
                    break;
                case IRRIGATION_UPDATE:
                    flowMeterReadingBuilder.setType(AgTechCDFProtos.FlowMeterReading.Type.IRRIGATION_UPDATE);
                    break;
                case IRRIGATION_END:
                    flowMeterReadingBuilder.setType(AgTechCDFProtos.FlowMeterReading.Type.IRRIGATION_END);
                    break;
                case HEARTBEAT:
                    flowMeterReadingBuilder.setType(AgTechCDFProtos.FlowMeterReading.Type.HEARTBEAT);
                    break;
                default:
                    //TODO: handle invalid type?
                    break;
            }

            AgTechCDFProtos.FlowMeterMeasurement.Builder waterAmountBuilder = AgTechCDFProtos.FlowMeterMeasurement.newBuilder();
            waterAmountBuilder.setUnit(flowMeterReading.getWaterAmount().getUnit());
            waterAmountBuilder.setValue(flowMeterReading.getWaterAmountValue());
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Water Amount: %s %f", flowMeterReading.getWaterAmount().getUnit(), flowMeterReading.getWaterAmountValue()));
            }
            flowMeterReadingBuilder.setWaterAmount(waterAmountBuilder.build());

            if (flowMeterReadingType == FlowMeterReadingType.HEARTBEAT) {
                AgTechCDFProtos.FlowMeterMeasurement.Builder energyAvailableBuilder = AgTechCDFProtos.FlowMeterMeasurement.newBuilder();
                energyAvailableBuilder.setUnit(flowMeterReading.getEnergyAvailable().getUnit());
                energyAvailableBuilder.setValue(flowMeterReading.getEnergyAvailableValue());
                flowMeterReadingBuilder.setEnergyAvailable(energyAvailableBuilder.build());
                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("Energy Available: %s %f", flowMeterReading.getEnergyAvailable().getUnit(), flowMeterReading.getEnergyAvailableValue()));
                }
            }

            CDFProtos.Measurement.Builder measurementBuilder = CDFProtos.Measurement.newBuilder();

            CDFProtos.Time.Builder timeBuilder = CDFProtos.Time.newBuilder();
            timeBuilder.setBegin(AgTechDateTime.getDefaultDateFormat().format(flowMeterReading.getTimeStamp()));
            timeBuilder.setEnd("");

            measurementBuilder.setTimespan(timeBuilder.build());

            measurementBuilder.setExtension(AgTechCDFProtos.flowMeterReading, flowMeterReadingBuilder.build());
            measurementBuilder.setSensorGuid(flowMeterReading.getFlowMeterId());

            CDFProtos.Units.Builder unitsBuilder = CDFProtos.Units.newBuilder();
            unitsBuilder.setType(CDFProtos.Units.Type.OTHERS);
            measurementBuilder.setUnits(unitsBuilder.build());

            measurementBuilder.setEntityUid("");

            dataBlobBuilder.setMeasure(measurementBuilder.build());

            collectionBuilder.addData(dataBlobBuilder.build());

            CDFProtos.Collection collection = collectionBuilder.build();

            if (logger.isDebugEnabled()) {

            }

            ByteArrayOutputStream bOutput = new ByteArrayOutputStream();
            try {
                collection.writeTo(bOutput);
                if (logger.isDebugEnabled()) {
                    logger.debug("Encoded Binary Data: " + Base64.encodeBase64((bOutput.toByteArray())));
                    logger.debug(messageSource.getMessage("delegate.leaving", new Object[]{NAME}, null));
                }
                return new AgTechProtoData(bOutput.toByteArray());
            } catch (IOException e) {
                throw new StopWorkException("Failed to get binary data", e);
            } finally {
                try {
                    bOutput.close();
                } catch (IOException e) {
                    logger.error("Failed to close the output stream", e);
                }
            }
        } else {
            throw new StopWorkException(messageSource.getMessage("delegate.invalidData", new Object[]{NAME, FlowMeterReading.class.getCanonicalName(), data.getClass().getCanonicalName()}, null));
        }
    }
}
