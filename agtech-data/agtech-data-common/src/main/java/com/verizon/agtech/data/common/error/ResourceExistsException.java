package com.verizon.agtech.data.common.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Resource Exists")
public class ResourceExistsException extends  RuntimeException {
    private String id;
    private String type;

    public ResourceExistsException(String id, String type) {
        super(type + " with id " + id + " exists!");
        this.id = id;
        this.type = type;
    }
    
    public ResourceExistsException(String id, String type, Throwable cause) {
        super(type + " with id " + id + " exists!", cause);
        this.id = id;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
