package com.verizon.agtech.data;

/**
 * 
 * @author DONTHI
 *
 */
public class SensorIdDetails {
    
	private String flowmeterSensorId;
	private String weatherSensorId;
	
	public String getFlowmeterSensorId() {
		return flowmeterSensorId;
	}
	public void setFlowmeterSensorId(String flowmeterSensorId) {
		this.flowmeterSensorId = flowmeterSensorId;
	}
	public String getWeatherSensorId() {
		return weatherSensorId;
	}
	public void setWeatherSensorId(String weatherSensorId) {
		this.weatherSensorId = weatherSensorId;
	}
	
}
