package com.verizon.agtech.data.dao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.verizon.agtech.data.dao.domain.APIKeyDocument;
import com.verizon.agtech.data.dao.repository.APIKeyRepository;

@Repository
@Transactional
public class APIKeyService {

	 @Autowired
	APIKeyRepository apiKeyRepository;
	 
	 public APIKeyDocument saveCatalogDocument(APIKeyDocument catalogDocument) {
	        return apiKeyRepository.save(catalogDocument);
	    }
	 public APIKeyDocument getApiCustomer(String username) {
	        return apiKeyRepository.findByCustomerId(username);
	    }
	 public APIKeyDocument findKeyCustomerId(String customerId) {
	        return apiKeyRepository.findByCustomerId(customerId);
	    }
}
