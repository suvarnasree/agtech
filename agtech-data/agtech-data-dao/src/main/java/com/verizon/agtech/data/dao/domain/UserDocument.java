package com.verizon.agtech.data.dao.domain;

import com.verizon.agtech.data.common.domain.AbstractAgTechDocument;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotNull;

/**
 * Created by drq on 4/26/16.
 */
@Document(collection = "users")
@CompoundIndexes({
        @CompoundIndex(name = "UserId_UserName_1", def = "{ 'userId': 1, 'userName' : 1}")
})
public class UserDocument extends AbstractAgTechDocument {

    @NotNull
    private String userName;

    @NotNull
    private String role;

    @NotNull
    private String oneTimePassword;

    private UserDetails userDetails;


    public UserDocument( String userName, String role, String oneTimePassword) {
        this.userName = userName;
        this.role = role;
        this.oneTimePassword = oneTimePassword;
    }


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getOneTimePassword() {
        return oneTimePassword;
    }

    public void setOneTimePassword(String oneTimePassword) {
        this.oneTimePassword = oneTimePassword;
    }
}
