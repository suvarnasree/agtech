package com.verizon.agtech.data.dao.service;

import com.verizon.agtech.data.dao.domain.UserDocument;
import com.verizon.agtech.data.dao.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by drq on 2/3/14.
 */
@Repository
@Transactional
public class UserService {

    private final static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    public UserDocument findUserById(String id) {
        return userRepository.findById(id);
    }

    public UserDocument findUserByName(String userName) {
        return userRepository.findByUserName(userName);
    }

    public Page<UserDocument> findUserDocuments(int page, int size) {
        return userRepository.findAll(new PageRequest(page, size));
    }

    public UserDocument saveUserDocument(UserDocument userDocument) {
        return userRepository.save(userDocument);
    }

    public UserDocument updateUserDocument(UserDocument userDocument) {
        return userRepository.save(userDocument);
    }
 
    public Boolean removeUserDocument(String userName) {
        UserDocument userDocumentDocument = this.findUserByName(userName);
        userRepository.delete(userDocumentDocument);
        return this.findUserByName(userName) == null;
    }

    public Boolean removeUserDocument(UserDocument userDocument) {
        String userName = userDocument.getId();
        userRepository.delete(userDocument);
        return this.findUserById(userName) == null;
    }

    public Page<UserDocument> findUsers(int page, int size) {
        return userRepository.findAll(new PageRequest(page, size));
    }
}
