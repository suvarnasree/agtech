package com.verizon.agtech.data.dao.domain;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by drq on 3/31/16.
 */
@Document(collection = "weather-station-readings")
@CompoundIndexes({
        @CompoundIndex(name = "timeStamp_weatherStationID_1", def = "{ 'timeStamp': 1, 'weatherStationID' : 1}")
})
public class WeatherStationReadingDocument extends AbstractAgTechReadingDocument {

    @NotNull
    private String weatherStationID;

    private Measurement meanTemperature;

    private Measurement windSpeed;

    private Measurement relativeHumidity;

    private Measurement totalPrecipitation;

    private Measurement solarRadiation;

    public WeatherStationReadingDocument() {
    }

    public WeatherStationReadingDocument(Date timeStamp, String weatherStationID,
                                         Measurement meanTemperature, Measurement windSpeed,
                                         Measurement relativeHumidity, Measurement totalPrecipitation,
                                         Measurement solarRadiation) {
        super(timeStamp);
        this.weatherStationID = weatherStationID;
        this.meanTemperature = meanTemperature;
        this.windSpeed = windSpeed;
        this.relativeHumidity = relativeHumidity;
        this.totalPrecipitation = totalPrecipitation;
        this.solarRadiation = solarRadiation;
    }

    public String getWeatherStationID() {
        return weatherStationID;
    }

    public void setWeatherStationID(String weatherStationID) {
        this.weatherStationID = weatherStationID;
    }

    public Measurement getMeanTemperature() {
        return meanTemperature;
    }

    public void setMeanTemperature(Measurement meanTemperature) {
        this.meanTemperature = meanTemperature;
    }

    public Measurement getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Measurement windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Measurement getRelativeHumidity() {
        return relativeHumidity;
    }

    public void setRelativeHumidity(Measurement relativeHumidity) {
        this.relativeHumidity = relativeHumidity;
    }

    public Measurement getTotalPrecipitation() {
        return totalPrecipitation;
    }

    public void setTotalPrecipitation(Measurement totalPrecipitation) {
        this.totalPrecipitation = totalPrecipitation;
    }

    public Measurement getSolarRadiation() {
        return solarRadiation;
    }

    public void setSolarRadiation(Measurement solarRadiation) {
        this.solarRadiation = solarRadiation;
    }
}
