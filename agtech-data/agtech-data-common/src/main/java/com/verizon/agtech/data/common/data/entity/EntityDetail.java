package com.verizon.agtech.data.common.data.entity;

import java.util.Calendar;
import java.util.Date;

import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by donthi 
 */

@Document(collection = "entityDetail")
@CompoundIndexes({
})
public class EntityDetail {
 
    private String dripCount;
	private String row;
	private String id;
	
	
		
	public EntityDetail() {
	}
	
	public EntityDetail(String dripCount, String row) {
		super();
		this.dripCount = dripCount;
		this.row = row;
	}
	
	public String getDripCount() {
		return dripCount;
	}
	public void setDripCount(String dripCount) {
		this.dripCount = dripCount;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
