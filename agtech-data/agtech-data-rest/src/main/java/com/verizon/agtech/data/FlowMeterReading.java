package com.verizon.agtech.data;

import java.util.Date;

import javax.validation.constraints.NotNull;

import com.verizon.agtech.data.dao.domain.Measurement;

/**
 * Created by drq on 4/3/16.
 */
public class FlowMeterReading {
    @NotNull
    private Date timeStamp;

    @NotNull
    private Measurement waterAmount;

    @NotNull
    private String type;

    public FlowMeterReading() {
    }

    public FlowMeterReading(Date timeStamp, Measurement waterAmount, String type) {
        this.timeStamp = timeStamp;
        this.waterAmount = waterAmount;
        this.type = type;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Measurement getWaterAmount() {
        return waterAmount;
    }

    public void setWaterAmount(Measurement waterAmount) {
        this.waterAmount = waterAmount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}