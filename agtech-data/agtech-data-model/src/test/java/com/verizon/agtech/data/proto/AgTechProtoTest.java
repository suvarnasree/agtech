package com.verizon.agtech.data.proto;

import com.google.protobuf.ExtensionRegistry;
import com.verizon.CDFProtos;

import com.verizon.agtech.AgTechCDFProtos;
import com.verizon.agtech.data.common.data.AgTechDateTime;
import org.apache.commons.net.util.Base64;
import junit.framework.TestCase;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by drq on 9/5/14.
 */
public class AgTechProtoTest extends TestCase {

    public void setUp() throws Exception {
    }

    public void tearDown() throws Exception {
    }

    @Test
    public void testFlowMeterReading() throws Exception {
        String flowMeterId = "0138A912";
        Long timeStamp = 1456785800000l;
        String unit = "ml";
        Double value = 500.0;
        String entityUid = "";
        DateFormat df = AgTechDateTime.getDefaultDateFormat();

        CDFProtos.Measurement.Builder measurementBuilder = CDFProtos.Measurement.newBuilder();

        AgTechCDFProtos.FlowMeterReading.Builder flowMeterReadingBuilder = AgTechCDFProtos.FlowMeterReading.newBuilder();

        flowMeterReadingBuilder.setType(AgTechCDFProtos.FlowMeterReading.Type.IRRIGATION_START);

        AgTechCDFProtos.FlowMeterMeasurement.Builder waterAmountBuilder = AgTechCDFProtos.FlowMeterMeasurement.newBuilder();
        waterAmountBuilder.setUnit(unit);
        waterAmountBuilder.setValue(value);
        flowMeterReadingBuilder.setWaterAmount(waterAmountBuilder.build());

        measurementBuilder.setExtension(AgTechCDFProtos.flowMeterReading, flowMeterReadingBuilder.build());

        CDFProtos.Time.Builder timeBuilder = CDFProtos.Time.newBuilder();
        timeBuilder.setBegin(df.format(new Date(timeStamp)));
        timeBuilder.setEnd("");

        measurementBuilder.setTimespan(timeBuilder.build());

        measurementBuilder.setSensorGuid(flowMeterId);

        measurementBuilder.setEntityUid(entityUid);

        CDFProtos.Units.Builder unitsBuilder = CDFProtos.Units.newBuilder();
        unitsBuilder.setType(CDFProtos.Units.Type.OTHERS);
        measurementBuilder.setUnits(unitsBuilder.build());

        CDFProtos.Measurement measurement = measurementBuilder.build();

        assertEquals(measurement.getSensorGuid(), flowMeterId);

        CDFProtos.Units units = measurement.getUnits();
        assertNotNull("Flow meter reading should have units", units);

        AgTechCDFProtos.FlowMeterReading flowMeterReading = measurement.getExtension(AgTechCDFProtos.flowMeterReading);
        assertNotNull("Flow meter reading should have values", flowMeterReading);

        assertNotNull("Flow meter reading should have water amount measurement", flowMeterReading.getWaterAmount());

        assertEquals("Flow meter water amount measurement should have expected value", flowMeterReading.getWaterAmount().getValue(), value);
        assertEquals("Flow meter water amount measurement should have expected unit", flowMeterReading.getWaterAmount().getUnit(), unit);
        assertEquals("Flow meter water amount measurement should have expected type", flowMeterReading.getType(), AgTechCDFProtos.FlowMeterReading.Type.IRRIGATION_START);

        CDFProtos.Time time = measurement.getTimespan();
        assertNotNull("Flow meter reading should have timestamp", time);
        assertEquals("Flow meter reading should have expected timestamp", Long.valueOf(df.parse(time.getBegin()).getTime()), timeStamp);

    }

    @Test
    public void testFlowMeterReading2() throws Exception {
        String flowMeterId = "0138A912";
        Long timeStamp = 1456785800000l;
        String unit = "ml";
        Double value = 500.0;
        String entityUid = "";

        String unit2 = "%";
        Double value2 = 15.5;

        // Set up proto collection builder
        CDFProtos.Collection.Builder collectionBuilder = CDFProtos.Collection.newBuilder();

        // Set up collection basic info
        String collectionGuid = UUID.randomUUID().toString();
        collectionBuilder.setGuid(collectionGuid);
        collectionBuilder.setType("flat");
        collectionBuilder.setMode(CDFProtos.Collection.CollectionMode.STATIC);
        collectionBuilder.setStatus(CDFProtos.Collection.Status.GOOD);
        collectionBuilder.setFriendlyName("");
        collectionBuilder.setTriageMode(CDFProtos.Collection.TriageMode.NONE);
        collectionBuilder.setTriageValue("");
        collectionBuilder.setParentGuid("");

        // Set up data blob builder
        CDFProtos.DataBlob.Builder dataBlobBuilder = CDFProtos.DataBlob.newBuilder();
        dataBlobBuilder.setGuid(UUID.randomUUID().toString());
        dataBlobBuilder.setFriendlyName("agtech");
        dataBlobBuilder.setDataType(CDFProtos.DataBlob.Type.MEASUREMENT);

        DateFormat df = AgTechDateTime.getDefaultDateFormat();

        CDFProtos.Measurement.Builder measurementBuilder = CDFProtos.Measurement.newBuilder();

        AgTechCDFProtos.FlowMeterReading.Builder flowMeterReadingBuilder = AgTechCDFProtos.FlowMeterReading.newBuilder();

        flowMeterReadingBuilder.setType(AgTechCDFProtos.FlowMeterReading.Type.HEARTBEAT);

        AgTechCDFProtos.FlowMeterMeasurement.Builder waterAmountBuilder = AgTechCDFProtos.FlowMeterMeasurement.newBuilder();
        waterAmountBuilder.setUnit(unit);
        waterAmountBuilder.setValue(value);
        flowMeterReadingBuilder.setWaterAmount(waterAmountBuilder.build());

        AgTechCDFProtos.FlowMeterMeasurement.Builder energyAvailableBuilder = AgTechCDFProtos.FlowMeterMeasurement.newBuilder();
        energyAvailableBuilder.setUnit(unit2);
        energyAvailableBuilder.setValue(value2);
        flowMeterReadingBuilder.setEnergyAvailable(energyAvailableBuilder.build());

        measurementBuilder.setExtension(AgTechCDFProtos.flowMeterReading, flowMeterReadingBuilder.build());

        CDFProtos.Time.Builder timeBuilder = CDFProtos.Time.newBuilder();
        timeBuilder.setBegin(df.format(new Date(timeStamp)));
        timeBuilder.setEnd("");

        measurementBuilder.setTimespan(timeBuilder.build());

        measurementBuilder.setSensorGuid(flowMeterId);

        measurementBuilder.setEntityUid(entityUid);

        CDFProtos.Units.Builder unitsBuilder = CDFProtos.Units.newBuilder();
        unitsBuilder.setType(CDFProtos.Units.Type.OTHERS);
        measurementBuilder.setUnits(unitsBuilder.build());

        CDFProtos.Measurement measurement = measurementBuilder.build();

        dataBlobBuilder.setMeasure(measurementBuilder.build());

        collectionBuilder.addData(dataBlobBuilder.build());

        CDFProtos.Collection collection = collectionBuilder.build();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        collection.writeTo(bos);

        byte[] bytes = Base64.encodeBase64(bos.toByteArray());

        ExtensionRegistry extensionRegistry = ExtensionRegistry.newInstance();
        CDFProtos.registerAllExtensions(extensionRegistry);
        AgTechCDFProtos.registerAllExtensions(extensionRegistry);

        CDFProtos.Collection collection1 = CDFProtos.Collection.parseFrom(Base64.decodeBase64(bytes), extensionRegistry);

        for (CDFProtos.DataBlob data : collection1.getDataList()) {
            CDFProtos.Measurement measurement1 = data.getMeasure();

            assertEquals(measurement1.getSensorGuid(), flowMeterId);

            CDFProtos.Units units = measurement1.getUnits();
            assertNotNull("Flow meter reading should have units", units);

            AgTechCDFProtos.FlowMeterReading flowMeterReading = measurement1.getExtension(AgTechCDFProtos.flowMeterReading);
            assertNotNull("Flow meter reading should have values", flowMeterReading);

            assertNotNull("Flow meter reading should have water amount measurement", flowMeterReading.getWaterAmount());

            assertEquals("Flow meter water amount measurement should have expected value", flowMeterReading.getWaterAmount().getValue(), value);
            assertEquals("Flow meter water amount measurement should have expected unit", flowMeterReading.getWaterAmount().getUnit(), unit);

            assertNotNull("Flow meter reading should have energy available measurement", flowMeterReading.getEnergyAvailable());

            assertEquals("Flow meter energy available measurement should have expected value", flowMeterReading.getEnergyAvailable().getValue(), value2);
            assertEquals("Flow meter energy available measurement should have expected unit", flowMeterReading.getEnergyAvailable().getUnit(), unit2);

            assertEquals("Flow meter water amount measurement should have expected type", flowMeterReading.getType(), AgTechCDFProtos.FlowMeterReading.Type.HEARTBEAT);

            CDFProtos.Time time = measurement.getTimespan();
            assertNotNull("Flow meter reading should have timestamp", time);
            assertEquals("Flow meter reading should have expected timestamp", Long.valueOf(df.parse(time.getBegin()).getTime()), timeStamp);
        }
    }
}
