package com.verizon.weatherstationemulator.services.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.verizon.weatherstation.emulator.services.IrrigationService;
import com.verizon.weatherstation.emulator.services.SensorProvider;
import com.verizon.weatherstationemulator.model.Measurement;
import com.verizon.weatherstationemulator.model.WeatherStationInformationRequest;

public class IrrigationProviderImpl implements SensorProvider {

	private Map<String, Integer> count = new HashMap<String, Integer>();
	@Autowired
	private IrrigationService irrigationService;
	@Value("${irrigation.update.count}")
	private int maxUpdate;
	

	/*
	public WeatherStationInformationRequest getEvent(String weatherStationId) {
		if (!this.count.containsKey(weatherStationId)) {
			this.count.put(weatherStationId, Integer.valueOf(0));
		}
		WeatherStationInformationRequest result;
		if (((Integer) this.count.get(weatherStationId)).intValue() == 0) {
			// WeatherStationInformationRequest result = build(weatherStationId,
			// "irrigationStart",
			// this.irrigationService.start(weatherStationId));
			result = build(weatherStationId, "irrigationStart", this.irrigationService.start(weatherStationId));

			this.count.put(weatherStationId, Integer.valueOf(1));
		} else if (((Integer) this.count.get(weatherStationId)).intValue() <= this.maxUpdate) {
			// WeatherStationInformationRequest result = build(weatherStationId,
			// "irrigationUpdate",
			// this.irrigationService.update(weatherStationId));
			result = build(weatherStationId, "irrigationUpdate", this.irrigationService.update(weatherStationId));

			this.count.put(weatherStationId,
					Integer.valueOf(((Integer) this.count.get(weatherStationId)).intValue() + 1));
		} else {
			result = build(weatherStationId, "irrigationEnd", this.irrigationService.end(weatherStationId));

			this.count.put(weatherStationId, Integer.valueOf(0));
		}
		return result;
	}
	*/

	/*
	 * private WeatherStationInformationRequest build(String weatherStationId,
	 * String type, int waterAmount) { WeatherStationInformationRequest
	 * weatherstationinformationrequest = new
	 * WeatherStationInformationRequest();
	 * 
	 * weatherstationinformationrequest.setWeatherStationId(weatherStationId);
	 * 
	 * EmulatorEvent emulatorEvent = new
	 * EmulatorEvent.EmulatorEventBuilder().weatherStationID(weatherStationID)
	 * .timeStamp(Long.valueOf(System.currentTimeMillis())).type(type)
	 * .waterAmount(new Measurement("ml",
	 * Integer.valueOf(waterAmount))).build();
	 * 
	 * weatherstationinformationrequest.setEvents(Arrays.asList(new
	 * EmulatorEvent[] { emulatorEvent }));
	 * 
	 * return weatherstationinformationrequest; }
	 */
	private WeatherStationInformationRequest build(String weatherStationID, Date timeStamp,
			 Measurement windSpeed,  Measurement relativeHumidity,  Measurement totalPrecipitation,
			 Measurement solarRadiation) {
		WeatherStationInformationRequest weatherStationInformationRequest = new WeatherStationInformationRequest();
		
		return weatherStationInformationRequest;

	}


	@Override
	public WeatherStationInformationRequest getEvent(String paramString) {
		// TODO Auto-generated method stub
		return null;
	}
}
