package com.verizon.agtech.data.pusher.job;

import akka.actor.ActorSelection;
import com.verizon.agtech.data.akka.ActorSystemFactory;
import com.verizon.agtech.data.pusher.data.JobDetails;
import org.joda.time.DateTime;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import java.util.Date;

/**
 * Created by drq on 10/4/15.
 */
public class PusherJob implements Job {
    public final static String NAME = "SFTP Pusher Job";

    private final static Logger logger = LoggerFactory.getLogger(PusherJob.class);

    @Autowired
    private ActorSystemFactory actorSystemFactory;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException{
        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Pushing Job triggered at %s.", new Date()));
        }

        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);
        try {
            ActorSelection startingActor = actorSystemFactory.getObject().actorSelection("/user/OutagePoller");

            JobKey key = jobExecutionContext.getJobDetail().getKey();
            JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();

            JobDetails jobDetails = new JobDetails();

            DateTime endDateTime = new DateTime();
            /*
            jobDetails.setStart(endDateTime.minusMinutes(dataMap.getInt(OutagePoller.KEY_POLLING_INTERVAL)).toDate());
            jobDetails.setEnd(endDateTime.toDate());
            jobDetails.setCompany(dataMap.getString(OutagePoller.KEY_POLLING_COMPANY));
            jobDetails.setMaxResults(1000l);
            */
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Job Key %s.", key.toString()));
            }
            startingActor.tell(jobDetails, null);
        } catch (Exception ex) {
            logger.error("Failed to start the job", ex);
        }
    }
}
