package com.verizon.agtech.data.dao.repository;

import com.verizon.agtech.data.dao.domain.RememberMeTockenDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;


/**
 * Created by Thani on 6/20/16.
 */
public interface RememberMeTokenRepository extends PagingAndSortingRepository<RememberMeTockenDocument,Long>,RememberMeTokenRespositoryCustom {

    /**
     *
     * @param id
     * @return
     */
    RememberMeTockenDocument findOne(Long id);

    /**
     * Saves the given {@link RememberMeTockenDocument}.
     *
     * @param rememberMeTockenDocument
     * @return
     */
    RememberMeTockenDocument save(RememberMeTockenDocument rememberMeTockenDocument);

    /**
     * Removes the given {@link RememberMeTockenDocument}.
     *
     * @param rememberMeTockenDocument
     * @return
     */
    void delete(RememberMeTockenDocument rememberMeTockenDocument);

    /**
     * @param id
     * @return
     */
    RememberMeTockenDocument findById(String id);

    /**
     *
     * @param userName
     * @return
     */
    Page<RememberMeTockenDocument> findByUserName(String userName,Pageable pageable);

    /**
     *
     * @param series
     * @return
     */
    RememberMeTockenDocument findBySeries(String series);

    /**
     * @return
     */
    Page<RememberMeTockenDocument> findAll(Pageable pageable);

}
