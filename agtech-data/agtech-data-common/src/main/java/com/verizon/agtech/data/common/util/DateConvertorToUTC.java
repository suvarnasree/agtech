package com.verizon.agtech.data.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * This class receives and converts date to UTC timezone
 * Created by Thani on 6/17/16.
 */
public class DateConvertorToUTC {

    private static final Logger logger = LoggerFactory.getLogger(DateConvertorToUTC.class);

    public static Date convertDateToUtc(Date datetime) {

        SimpleDateFormat sdfUTC = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        TimeZone tzInUtc = TimeZone.getTimeZone("UTC");
        sdfUTC.setTimeZone(tzInUtc);

        String sDateInUtc = sdfUTC.format(datetime); // Convert to String first
        Date dateInUtc = null;
        try {
            SimpleDateFormat sdfUTC1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            dateInUtc = sdfUTC1.parse(sDateInUtc);
        } catch (ParseException e) {
            logger.error("Date Conversion to UTC is failing" + e.getMessage());
        }
        return dateInUtc;
    }
}
