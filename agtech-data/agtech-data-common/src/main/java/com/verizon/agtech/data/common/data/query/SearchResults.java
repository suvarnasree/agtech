package com.verizon.agtech.data.common.data.query;

import java.util.Iterator;

/**
 * Created by drq on 9/14/14.
 */
public class SearchResults {
    private Integer pageTotalItems;
    private Boolean hasNextPage;
    private Boolean hasPreviousPage;
    private Integer totalPages;
    private Iterator<?> dataItems;

    public SearchResults() {
    }

    public SearchResults(Integer pageTotalItems, Boolean hasNextPage, Boolean hasPreviousPage, Integer totalPages, Iterator<?> dataItems) {
        this.pageTotalItems = pageTotalItems;
        this.hasNextPage = hasNextPage;
        this.hasPreviousPage = hasPreviousPage;
        this.totalPages = totalPages;
        this.dataItems = dataItems;
    }

    public Integer getPageTotalItems() {
        return pageTotalItems;
    }

    public void setPageTotalItems(Integer pageTotalItems) {
        this.pageTotalItems = pageTotalItems;
    }

    public Boolean getHasNextPage() {
        return hasNextPage;
    }

    public void setHasNextPage(Boolean hasNextPage) {
        this.hasNextPage = hasNextPage;
    }

    public Boolean getHasPreviousPage() {
        return hasPreviousPage;
    }

    public void setHasPreviousPage(Boolean hasPreviousPage) {
        this.hasPreviousPage = hasPreviousPage;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    public Iterator<?> getDataItems() {
        return dataItems;
    }

    public void setDataItems(Iterator<?> dataItems) {
        this.dataItems = dataItems;
    }
}
