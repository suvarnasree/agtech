package com.verizon.agtech.data.dao.domain;


public class Measurement {
    private String unit;
    private Object value;

    public Measurement() {
    }

    public Measurement(String unit, Object value) {
        this.unit = unit;
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Measurement{" +
                "unit='" + unit + '\'' +
                ", value=" + value +
                '}';
    }
}