package com.verizon.agtech.data.akka;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.RouterConfig;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Map;

/**
 * Created by drq on 4/11/14.
 */
public class ActorSystemFactory implements FactoryBean<ActorSystem>, InitializingBean, BeanFactoryAware {

    private String systemName;

    private Map<String, CollectorActorConfigs> actorMap;

    private BeanFactory beanFactory;

    private ActorSystem instance;

    public String getSystemName() {
        return systemName;
    }

    public void setSystemName(String systemName) {
        this.systemName = systemName;
    }

    public Map<String, CollectorActorConfigs> getActorMap() {
        return actorMap;
    }

    public void setActorMap(Map<String, CollectorActorConfigs> actorMap) {
        this.actorMap = actorMap;
    }

    public BeanFactory getBeanFactory() {
        return beanFactory;
    }

    @Override
    public ActorSystem getObject() throws Exception {
        if (instance == null) {
            instance = ActorSystem.create(systemName);
        }
        return instance;
    }

    @Override
    public Class<?> getObjectType() {
        return null;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    public void shutdown() throws Exception {
        ActorSystem actorSystem = getObject();
        actorSystem.shutdown();
        actorSystem.awaitTermination();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (String key : actorMap.keySet()) {
            String beanName = actorMap.get(key).getName();
            CollectorDataHandler handler = beanFactory.getBean(beanName, CollectorDataHandler.class);
            RouterConfig routerConfig = actorMap.get(key).getRouterConfig();
            if (routerConfig != null) {
                ActorRef actor = instance.actorOf(Props.create(CollectorActor.class, handler).withRouter(routerConfig), key);
            } else {
                ActorRef actor = instance.actorOf(Props.create(CollectorActor.class, handler), key);
            }
        }
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        this.beanFactory = beanFactory;
    }
}
