package com.verizon.agtech.data.common.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.joda.time.DateTime;
import org.springframework.data.annotation.*;

public class AbstractDocument {

    @Id
    private String id;

    @Version
    private Long version;

    @JsonIgnore
    @CreatedDate
    private DateTime createdAt;

    @JsonIgnore
    @LastModifiedDate
    private DateTime lastModified;

    @CreatedBy
    private String createdBy;

    @LastModifiedBy
    private String lastModifiedBy;

    /**
     * Returns the identifier of the document.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    public Long getVersion() {
        return version;
    }

    public DateTime getCreatedAt() {
        return createdAt;
    }

    public DateTime getLastModified() {
        return lastModified;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    /*
        * (non-Javadoc)
        * @see java.lang.Object#equals(java.lang.Object)
        */
    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }

        if (this.id == null || obj == null || !(this.getClass().equals(obj.getClass()))) {
            return false;
        }

        AbstractDocument that = (AbstractDocument) obj;

        return this.id.equals(that.getId());
    }

    /*
    * (non-Javadoc)
    * @see java.lang.Object#hashCode()
    */
    @Override
    public int hashCode() {
        return id == null ? 0 : id.hashCode();
    }

    public long getCreatedAtTimestamp() {
        return createdAt.getMillis();
    }

    public long getLastModifiedAtTimestamp() {
        return lastModified.getMillis();
    }
}
