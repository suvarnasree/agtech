package com.verizon.weatherstation.emulator.services;

import com.verizon.weatherstationemulator.model.WeatherStationInformationRequest;

public abstract interface SensorProvider {
	
	public abstract WeatherStationInformationRequest getEvent(String paramString);
	}


