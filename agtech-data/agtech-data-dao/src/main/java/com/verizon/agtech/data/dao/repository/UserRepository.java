package com.verizon.agtech.data.dao.repository;

import com.verizon.agtech.data.dao.domain.UserDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by drq on 6/8/15.
 */
public interface UserRepository extends PagingAndSortingRepository<UserDocument, Long>, UserRepositoryCustom {
    /**
     * Returns the user with the given identifier.
     *
     * @param id
     * @return
     */
    UserDocument findOne(Long id);

    /**
     * Saves the given {@link UserDocument}.
     *
     * @param user
     * @return
     */
    UserDocument save(UserDocument user);

    /**
     * Removes the given {@link UserDocument}.
     *
     * @param user
     * @return
     */
    void delete(UserDocument user);

    /**
     * @param id
     * @return
     */
    UserDocument findById(String id);

    /**
     * This method to retrive the data using UserName
     * @param userName
     * @return
     */
    UserDocument findByUserName(String userName);

    /**
     * @return
     */
    Page<UserDocument> findAll(Pageable pageable);


}
