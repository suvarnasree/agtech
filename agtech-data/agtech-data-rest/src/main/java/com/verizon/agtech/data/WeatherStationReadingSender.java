package com.verizon.agtech.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.verizon.agtech.data.common.data.weatherstation.WeatherStationData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import java.util.Date;
import java.util.UUID;

public class WeatherStationReadingSender {
    private final static Logger logger = LoggerFactory.getLogger(WeatherStationReadingSender.class);

    private AmqpTemplate messageTemplate;
    private String targetQueue;

    public WeatherStationReadingSender() {
    }

    public void setMessageTemplate(AmqpTemplate messageTemplate) {
        this.messageTemplate = messageTemplate;
    }

    public void setTargetQueue(String targetQueue) {
        this.targetQueue = targetQueue;
    }

    public void sendNotification(WeatherStationData weatherStationData, String type) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering Weather Station Reading Sender....");
        }

        try {
            MessageProperties properties = new MessageProperties();
            properties.setMessageId(UUID.randomUUID().toString());
            properties.setHeader("messageType", type);
            properties.setTimestamp(new Date());

            ObjectMapper mapper = new ObjectMapper();
            Message message = new Message(mapper.writeValueAsString(weatherStationData).getBytes(), properties);

            messageTemplate.send(targetQueue, message);

            if (logger.isDebugEnabled()) {
                logger.debug("Leaving Weather Station Reading Sender....");
            }
        } catch (Exception e) {
            logger.error("Weather Station Reading Sender error", e);
            throw e;
        }
    }

}
