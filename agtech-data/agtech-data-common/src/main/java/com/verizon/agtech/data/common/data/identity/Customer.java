package com.verizon.agtech.data.common.data.identity;

/**
 * Created by drq on 4/26/16.
 */
public class Customer {

    private String accountName;

	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
    private String ecpdId;
	private Address address;
    private Entities entities;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
    
	public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEcpdId() {
        return ecpdId;
    }

    public void setEcpdId(String ecpdId) {
        this.ecpdId = ecpdId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Entities getEntities() {
        return entities;
    }

    public void setEntities(Entities entities) {
        this.entities = entities;
    }


}
