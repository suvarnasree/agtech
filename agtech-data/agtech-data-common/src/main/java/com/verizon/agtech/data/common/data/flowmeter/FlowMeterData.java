package com.verizon.agtech.data.common.data.flowmeter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.verizon.agtech.data.common.data.AgTechDateTime;
import com.verizon.agtech.data.common.data.Measurement;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by drq on 4/3/16.
 */
public class FlowMeterData {
    @NotNull
    private Date timeStamp;

    @NotNull
    private Measurement waterAmount;

    private Measurement energyAvailable;

    @NotNull
    private FlowMeterReadingType type;

    public FlowMeterData() {
    }

    public FlowMeterData(Date timeStamp, Measurement waterAmount, Measurement energyAvailable, FlowMeterReadingType type) {
        this.timeStamp = timeStamp;
        this.waterAmount = waterAmount;
        this.energyAvailable = energyAvailable;
        this.type = type;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Measurement getWaterAmount() {
        return waterAmount;
    }

    public void setWaterAmount(Measurement waterAmount) {
        this.waterAmount = waterAmount;
    }

    public Measurement getEnergyAvailable() {
        return energyAvailable;
    }

    public void setEnergyAvailable(Measurement energyAvailable) {
        this.energyAvailable = energyAvailable;
    }

    @JsonIgnore
    public Double getWaterAmountValue() {
        try {
            Object value = waterAmount.getValue();
            if (value != null) {
                return Double.parseDouble(value.toString());
            } else {
                return null;
            }
        } catch (Exception ex) {
            return null;
        }
    }

    @JsonIgnore
    public Double getEnergyAvailableValue() {
        try {
            Object value = energyAvailable.getValue();
            if (value != null) {
                return Double.parseDouble(value.toString());
            } else {
                return null;
            }
        } catch (Exception ex) {
            return null;
        }
    }

    public FlowMeterReadingType getType() {
        return type;
    }

    public void setType(FlowMeterReadingType type) {
        this.type = type;
    }

    public String toString () {
        return String.format("Time Stamp: %s Measurement: %s Type: %s",
                AgTechDateTime.getDefaultDateFormat().format(timeStamp),
                waterAmount.toString(),
                type.toString()
        );
    }
}