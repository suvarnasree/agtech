package com.verizon.agtech.data.dao.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.verizon.agtech.data.common.data.identity.Address;
import com.verizon.agtech.data.common.data.identity.Entities;
import com.verizon.agtech.data.common.domain.AbstractAgTechDocument;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by drq on 4/26/16.
 */
@Document(collection = "customers")
@CompoundIndexes({
        @CompoundIndex(name = "customerId_1", def = "{ 'customerId': 1}")
})
public class CustomerDocument extends AbstractAgTechDocument {
	private String accountName;

	private String firstName;
	private String lastName;
	private String email;
	private String phoneNumber;
	private String ecpdId;
	private Address address;
	private Entities entities;
	@JsonIgnore
	private String key;

	public CustomerDocument(String accountName, String firstName,  String lastName, String email,
							String phoneNumber, String ecpdId, Address address, Entities entities, String key) {
		this.accountName = accountName;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.ecpdId = ecpdId;
		this.address = address;
		this.entities = entities;
		this.key = key;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getEcpdId() {
		return ecpdId;
	}

	public void setEcpdId(String ecpdId) {
		this.ecpdId = ecpdId;
	}

	public Entities getEntities() {
		return entities;
	}

	public void setEntities(Entities entities) {
		this.entities = entities;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	
}
