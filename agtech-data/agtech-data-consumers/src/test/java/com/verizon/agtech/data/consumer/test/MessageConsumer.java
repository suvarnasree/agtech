package com.verizon.agtech.data.consumer.test;

import org.jivesoftware.smack.packet.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by drq on 8/6/15.
 */
@Component
public class MessageConsumer {

    private final static Logger logger = LoggerFactory.getLogger(MessageConsumer.class);

    @ServiceActivator
    public void consume(Object input) throws Throwable {
        String payload;
        if (input instanceof Message) {
            payload = ((Message) input).getBody();
        } else if (input instanceof String) {
            payload = (String) input;
        } else {
            throw new IllegalArgumentException(
                    "expected either a Smack Message or a String, but received: " + input);
        }
        if (logger.isDebugEnabled()) {
            Date date = new Date();
            logger.debug("Received message: Size " + payload.length() + " @ " + date + " - " + date.getTime());
        }
    }
}