package com.verizon.agtech.data.akka;

import akka.actor.ActorSelection;
import akka.actor.ActorSystem;
import org.springframework.beans.factory.InitializingBean;

/**
 * Created by drq on 4/17/14.
 */
public abstract class CollectorChain implements InitializingBean {
    private ActorSystem actorSystem;
    private String startingActorName;
    protected ActorSelection startingActor;

    public ActorSystem getActorSystem() {
        return actorSystem;
    }

    public void setActorSystem(ActorSystem actorSystem) {
        this.actorSystem = actorSystem;
    }

    public String getStartingActorName() {
        return startingActorName;
    }

    public void setStartingActorName(String startingActorName) {
        this.startingActorName = startingActorName;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        startingActor = actorSystem.actorSelection("/user/" + startingActorName);
    }
}
