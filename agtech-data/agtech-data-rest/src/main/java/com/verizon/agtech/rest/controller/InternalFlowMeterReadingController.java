package com.verizon.agtech.rest.controller;

import com.verizon.agtech.data.common.data.flowmeter.FlowMeterData;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReadings;
import com.verizon.agtech.data.common.error.ResourceNotFoundException;
import com.verizon.agtech.data.dao.domain.FlowMeterReadingDocument;
import com.verizon.agtech.data.dao.service.FlowMeterReadingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by drq on 3/31/16.
 */
@Controller
@RequestMapping("internal-api/flowmeterreadings")
@Api(value = "/internal-api/flowmeterreadings", tags = "FlowMeter Internal", description = "Internal API for flow meter readings REST operations")
public class InternalFlowMeterReadingController {
    @Autowired
    private FlowMeterReadingService flowMeterReadingService;

    private final static Logger logger = LoggerFactory.getLogger(InternalFlowMeterReadingController.class);

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "List flow meter readings", notes = "Returns paged flow meter readings.")
    public Model findFlowMeterReadingDocuments(
            @ApiIgnore
            final Model model,
            @RequestParam(value = "p", required = false, defaultValue = "0")
            @ApiParam(name = "p", value = "Start page number", required = false, defaultValue = "0")
                    Integer page,
            @ApiParam(name = "s", value = "Page size", required = false, defaultValue = "20")
            @RequestParam(value = "s", required = false, defaultValue = "20")
                    Integer size) {

        Page<FlowMeterReadingDocument> flowMeterReadings = flowMeterReadingService.findFlowMeterReadingDocuments(page, size);

        model.addAttribute("flowMeterReadings", flowMeterReadings.iterator());
        model.addAttribute("page", page);
        model.addAttribute("pageSize", size);
        model.addAttribute("pageTotalItems", flowMeterReadings.getNumberOfElements());
        model.addAttribute("hasNextPage", flowMeterReadings.hasNext());
        model.addAttribute("hasPreviousPage", flowMeterReadings.hasPrevious());
        model.addAttribute("totalPages", flowMeterReadings.getTotalPages());

        return model;
    }

    @RequestMapping(value = "/{flowMeterReadingId}", method = RequestMethod.GET)
    @ApiOperation(value = "Find single Flow Meter Reading", notes = "Returns flow meter reading that matches provided id which can be either DB id or GUID.")
    public Model findFlowMeterReadingDocument(
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "flowMeterReadingId", value = "Flow Meter Reading Id", required = true)
                    String flowMeterReadingId) {
        model.addAttribute("id", flowMeterReadingId);
        FlowMeterReadingDocument flowMeterReadingDocument = flowMeterReadingService.findFlowMeterReadingByAnyId(flowMeterReadingId);
        if (flowMeterReadingDocument == null) {
            throw new ResourceNotFoundException(flowMeterReadingId, "flowMeterReadings");
        } else {
            model.addAttribute("flowMeterReading", flowMeterReadingDocument);
        }
        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create New Flow Meter Reading", notes = "Create a new  flowMeterReading.")
    public Model createFlowMeterReadingDocument(
            @ApiIgnore
            final Model model,
            @RequestBody
            @ApiParam(name = "flowMeterReadingDocument", value = "Flow Meter Reading Document", required = true)
                    FlowMeterReadingDocument flowMeterReadingDocument) {
        flowMeterReadingService.saveFlowMeterReadingDocument(flowMeterReadingDocument);
        model.addAttribute("flowMeterReading", flowMeterReadingDocument);
        logger.info("Flow Meter Reading Document Saved " + flowMeterReadingDocument.getId());
        return model;
    }

    @RequestMapping(value = "/{flowMeterReadingId}", method = RequestMethod.PUT)
    @ApiOperation(value = "Update Flow Meter Reading", notes = "Update a given flow meter reading.")
    public Model updateFlowMeterReadingDocument(
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "flowMeterReadingId", value = "flow meter reading Id", required = true)
                    String flowMeterReadingId,
            @RequestBody
            @ApiParam(name = "flowMeterReadingDocument", value = "flow meter reading to be updated to", required = true)
                    FlowMeterReadingDocument flowMeterReadingDocument) {
        model.addAttribute("id", flowMeterReadingId);
        FlowMeterReadingDocument updatedFlowMeterReadingDocument = flowMeterReadingService.findFlowMeterReadingByAnyId(flowMeterReadingId);
        if (updatedFlowMeterReadingDocument == null) {
            throw new ResourceNotFoundException(flowMeterReadingId, "flowMeterReading");
        } else {
            flowMeterReadingService.saveFlowMeterReadingDocument(updatedFlowMeterReadingDocument);
            model.addAttribute("flowMeterReading", updatedFlowMeterReadingDocument);
        }
        return model;
    }

    @RequestMapping(value = "/{flowMeterReadingId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Update Flow Meter Reading", notes = "Update a given flow meter reading.")
    public Model deleteFlowMeterReadingDocument(
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "flowMeterReadingId", value = "flow meter reading Id", required = true)
                    String flowMeterReadingId) {
        model.addAttribute("id", flowMeterReadingId);
        FlowMeterReadingDocument flowMeterReadingDocument = flowMeterReadingService.findFlowMeterReadingByAnyId(flowMeterReadingId);
        if (flowMeterReadingDocument == null) {
            throw new ResourceNotFoundException(flowMeterReadingId, "flowMeterReading");
        } else {
            flowMeterReadingService.removeFlowMeterReadingDocument(flowMeterReadingDocument);
        }
        return model;
    }

    @RequestMapping(value = "/{customerId}/{flowMeterId}", method = RequestMethod.POST)
    @ApiOperation(value = "Inject Flow Meter Readings", notes = "Inject batch of flow meter readings to database directly.")
    public Model createFlowMeterReadingDocument(
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "customerId", value = "Customer Id", required = true)
                    String customerId,
            @PathVariable
            @ApiParam(name = "flowMeterId", value = "Flow Meter Id", required = true)
                    String flowMeterId,
            @RequestBody
            @ApiParam(name = "flowMeterReadings", value = "Flow meter readings to be injected", required = true)
                    FlowMeterReadings flowMeterReadings) {
        List<FlowMeterReadingDocument> flowMeterReadingDocuments = new ArrayList<FlowMeterReadingDocument>();
        for (FlowMeterData flowMeterReading : flowMeterReadings.getEvents()) {
            flowMeterReadingDocuments.add(flowMeterReadingService.saveFlowMeterReadingDocument(
                    new FlowMeterReadingDocument(flowMeterReading.getTimeStamp(),
                            flowMeterId,
                            flowMeterReading.getWaterAmount(),
                            flowMeterReading.getEnergyAvailable(),
                            flowMeterReading.getType().toString())
            ));
        }
        model.addAttribute("customerId", customerId);
        model.addAttribute("flowMeterId", flowMeterId);
        model.addAttribute("readings", flowMeterReadingDocuments.iterator());
        return model;
    }
}
