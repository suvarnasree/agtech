package com.verizon.agtech.data.ingestor.rabbitmq;

import com.verizon.agtech.data.akka.CollectorChain;
import com.verizon.agtech.data.ingestor.data.RabbitMQData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Headers;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by drq on 8/18/14.
 */
@Component
public class FlowMeterReadingRabbitMQConsumer extends CollectorChain {

    public final static String NAME = "FLOW_METER_READING_RABBITMQ_CONSUMER";

    private final static Logger logger = LoggerFactory.getLogger(FlowMeterReadingRabbitMQConsumer.class);

    @ServiceActivator
    public void processMessage(Object input, @Headers Map<String, Object> headerMap) throws Throwable {
        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Processing message inbound message for %s.", NAME));
            for (String key : headerMap.keySet()) {
                logger.debug(String.format("Message Header Key: %s , Value: %s", key, headerMap.get(key)));
            }
        }

        if (input != null) {
            String payLoad = new String((byte[]) input, "UTF-8");
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Message Payload: %s", payLoad));
            }
            startingActor.tell(new RabbitMQData(headerMap, payLoad), null);
        }
    }
}
