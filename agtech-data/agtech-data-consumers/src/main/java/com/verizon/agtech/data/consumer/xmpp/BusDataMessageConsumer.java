package com.verizon.agtech.data.consumer.xmpp;


import com.verizon.agtech.data.akka.CollectorChain;
import com.verizon.agtech.data.common.data.BusData;
import org.jivesoftware.smack.packet.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: drq
 * Date: 1/23/14
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
@Component
public class BusDataMessageConsumer extends CollectorChain {

    private final static Logger logger = LoggerFactory.getLogger(BusDataMessageConsumer.class);

    @ServiceActivator
    public void consume(Object input) throws Throwable {
        Map<String, Object> headers = new HashMap<String, Object>();
        String payload;
        if (input instanceof Message) {
            Message receivedMessage = (Message) input;
            for (String name : receivedMessage.getPropertyNames()) {
                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("Name: %s;Value: %s", name, receivedMessage.getProperty(name)));
                }
                headers.put(name, receivedMessage.getProperty(name));
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Message: " + receivedMessage.toXML());
            }
            payload = ((Message) input).getBody();
        } else if (input instanceof String) {
            payload = (String) input;
        } else {
            throw new IllegalArgumentException(
                    "expected either a Smack Message or a String, but received: " + input);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Received message: " + payload);
        }
        if (payload != null) {
            startingActor.tell(new BusData(headers, payload), null);
        }
    }
}
