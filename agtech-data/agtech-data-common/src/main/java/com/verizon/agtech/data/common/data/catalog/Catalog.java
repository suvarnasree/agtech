package com.verizon.agtech.data.common.data.catalog;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by drq on 4/26/16. --
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Catalog {
    @NotNull
    private String name;

    private List<DeviceModel> deviceModels = new ArrayList<>();

    public Catalog() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DeviceModel> getDeviceModels() {
        return deviceModels;
    }

    public void setDeviceModels(List<DeviceModel> deviceModels) {
        this.deviceModels = deviceModels;
    }

	
}
