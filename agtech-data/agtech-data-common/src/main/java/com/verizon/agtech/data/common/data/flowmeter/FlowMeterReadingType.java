package com.verizon.agtech.data.common.data.flowmeter;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by drq on 9/3/14.
 */
public enum FlowMeterReadingType {
    IRRIGATION_START("irrigationStart"),
    IRRIGATION_UPDATE("irrigationUpdate"),
    IRRIGATION_END("irrigationEnd"),
    HEARTBEAT("heartbeat"),
    UNKNOWN("unknown");

    private String type;

    /**
     *
     * @param type
     */
    FlowMeterReadingType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String toString() {
        return this.type;
    }

    /**
     *
     * @param text
     * @return
     */
    @JsonCreator
    public static FlowMeterReadingType fromString(String text) {
        if (text != null) {
            for (FlowMeterReadingType b : FlowMeterReadingType.values()) {
                if (text.equalsIgnoreCase(b.type)) {
                    return b;
                }
            }
        }
        return UNKNOWN;
    }

    @JsonValue
    public String getType() {
        return type;
    }
}
