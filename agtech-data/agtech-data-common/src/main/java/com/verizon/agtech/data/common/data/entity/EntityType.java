package com.verizon.agtech.data.common.data.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Created by drq on 9/3/14.
 */
public enum EntityType {
    SENSOR("SENSOR"),
    BION("BION"),
    GATEWAY("GATEWAY"),
    FEEDER("FEEDER"),
    MACHINE("MACHINE"),
    APPLICATION("APPLICATION"),
    UNKNOWN("unknown");

    private String type;

    /**
     *
     * @param type
     */
    EntityType(String type) {
        this.type = type;
    }

    /**
     *
     * @return
     */
    public String toString() {
        return this.type;
    }

    /**
     *
     * @param text
     * @return
     */
    @JsonCreator
    public static EntityType fromString(String text) {
        if (text != null) {
            for (EntityType b : EntityType.values()) {
                if (text.equalsIgnoreCase(b.type)) {
                    return b;
                }
            }
        }
        return UNKNOWN;
    }

    @JsonValue
    public String getType() {
        return type;
    }
}
