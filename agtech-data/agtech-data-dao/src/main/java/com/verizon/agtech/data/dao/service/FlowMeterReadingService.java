package com.verizon.agtech.data.dao.service;

import com.verizon.agtech.data.dao.domain.FlowMeterReadingDocument;
import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;
import com.verizon.agtech.data.dao.repository.FlowMeterReadingRepository;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by drq on 2/3/14.
 */
@Repository
@Transactional
public class FlowMeterReadingService {

    private final static Logger logger = LoggerFactory.getLogger(FlowMeterReadingService.class);

    @Autowired
    private FlowMeterReadingRepository flowMeterReadingRepository;

    public FlowMeterReadingDocument findFlowMeterReadingById(String id) {
        return flowMeterReadingRepository.findById(id);
    }

    public FlowMeterReadingDocument findFlowMeterReadingByGuid(String guid) {
        return flowMeterReadingRepository.findByGuid(guid);
    }
    
    public FlowMeterReadingDocument findFlowmeterReadingByFlowMeterId(String FlowMeterId) {
    	return flowMeterReadingRepository.findByFlowMeterId(FlowMeterId);
	}

    public FlowMeterReadingDocument findFlowMeterReadingByAnyId(String outageId) {
        FlowMeterReadingDocument flowMeterReadingDocument = flowMeterReadingRepository.findById(outageId);
        if (flowMeterReadingDocument == null) {
            flowMeterReadingDocument = flowMeterReadingRepository.findByGuid(outageId);
        }
        return flowMeterReadingDocument;
    }

    public Page<FlowMeterReadingDocument> findFlowMeterReadingDocuments(int page, int size) {
        return flowMeterReadingRepository.findAll(new PageRequest(page, size));
    }

    public FlowMeterReadingDocument saveFlowMeterReadingDocument(FlowMeterReadingDocument flowMeterReadingDocument) {
    logger.info("entering FlowMeterReadingService.saveFlowMeterReadingDocument(FlowMeterID)");
        
        logger.info(" next call is for Mongo DB Spring API ");
        return flowMeterReadingRepository.save(flowMeterReadingDocument);
    }

    public Boolean removeFlowMeterReadingDocument(String flowMeterReadingId) {
        FlowMeterReadingDocument flowMeterReadingDocumentDocument = this.findFlowMeterReadingById(flowMeterReadingId);
        flowMeterReadingRepository.delete(flowMeterReadingDocumentDocument);
        return this.findFlowMeterReadingById(flowMeterReadingId) == null;
    }

    public Boolean removeFlowMeterReadingDocument(FlowMeterReadingDocument flowMeterReadingDocument) {
        String flowMeterReadingId = flowMeterReadingDocument.getId();
        flowMeterReadingRepository.delete(flowMeterReadingDocument);
        return this.findFlowMeterReadingById(flowMeterReadingId) == null;
    }

    public Page<FlowMeterReadingDocument> findFlowMeterReadingsByTimeStamp(DateTime startDateTime, DateTime endDateTime, int page, int size) {
    	
    	logger.info("entering method FlowMeterReadingService.findFlowMeterReadingsByIDAndTimeRange() without customerID");
    	logger.info("Next call is for MongoDB Spring API");
    	return flowMeterReadingRepository.findFlowMeterReadingsByTimeStamp(startDateTime, endDateTime, new PageRequest(page, size));
    }

    public Page<FlowMeterReadingDocument> findFlowMeterReadingsByIDAndTimeRange(String id, DateTime startDateTime, DateTime endDateTime, int page, int size) {
        logger.info("entering method FlowMeterReadingService.findFlowMeterReadingsByIDAndTimeRange() with customerID");
    	logger.info("customerId :"+id);
    	logger.info("Next call is for MongoDB Spring API");
        
        return flowMeterReadingRepository.findFlowMeterReadingsByIDAndTimeRange(id, startDateTime, endDateTime, new PageRequest(page, size));
    }

    public Page<FlowMeterReadingDocument> findLatestFlowMeterReadingDocument(String flowMeterId,String heartBeatType) {
        logger.info("Fetching the latest WeatherStationData for the Id:" + flowMeterId);
        return flowMeterReadingRepository.findLatestFlowMeterReadingPage(flowMeterId,heartBeatType,new PageRequest(0,1, Sort.Direction.DESC,new String[]{"createdAt"}));

    }

}
