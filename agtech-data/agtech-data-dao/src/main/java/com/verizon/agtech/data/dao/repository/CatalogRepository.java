package com.verizon.agtech.data.dao.repository;

import com.verizon.agtech.data.dao.domain.CatalogDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface CatalogRepository extends PagingAndSortingRepository<CatalogDocument, Long>, CatalogRepositoryCustom {
    /**
     * Returns the customer with the given identifier.
     *
     * @param id
     * @return
     */
    CatalogDocument findOne(Long id);

    /**
     * Saves the given {@link CatalogDocument}.
     *
     * @param catalog
     * @return
     */
    
    CatalogDocument save(CatalogDocument catalog);

    /**
     * Removes the given {@link CatalogDocument}.
     *
     * @param catalog
     * @return
     */
    void delete(CatalogDocument catalog);

    /**
     * @param id
     * @return
     */
    CatalogDocument findById(String id);

    /**
     * @param guid
     * @return
     */
    CatalogDocument findByGuid(String guid);

    /**
     * @param guid
     * @return
     */
    Page<CatalogDocument> findByName(String guid,Pageable pageable);
    /**
     * @return
     */
    Page<CatalogDocument> findAll(Pageable pageable);

	CatalogDocument findByName(String catalogName);
}
