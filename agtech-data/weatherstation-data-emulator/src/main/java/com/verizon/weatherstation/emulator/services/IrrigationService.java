package com.verizon.weatherstation.emulator.services;

public abstract interface IrrigationService {
	
	public abstract int start(String paramString);
	  
	  public abstract int get(String paramString);
	  
	  public abstract int update(String paramString);
	  
	  public abstract int end(String paramString);

}
