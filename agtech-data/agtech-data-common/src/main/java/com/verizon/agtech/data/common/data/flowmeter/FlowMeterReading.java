package com.verizon.agtech.data.common.data.flowmeter;

import com.verizon.agtech.data.common.data.Measurement;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by drq on 4/3/16.
 */
public class FlowMeterReading extends FlowMeterData {
    public static final String NAME = "FlowMeterReading";

    @NotNull
    private String flowMeterId;

    public FlowMeterReading() {
    }

    public FlowMeterReading(Date timeStamp, Measurement waterAmount, Measurement energyAvailable, FlowMeterReadingType type, String flowMeterId) {
        super(timeStamp, waterAmount, energyAvailable, type);
        this.flowMeterId = flowMeterId;
    }

    public String getFlowMeterId() {
        return flowMeterId;
    }

    public void setFlowMeterId(String flowMeterId) {
        this.flowMeterId = flowMeterId;
    }

    public String toString () {
        return String.format("Flow Meter ID: %s %s",
                flowMeterId,
                super.toString()
        );
    }
}