package com.verizon.agtech.rest.controller;


import com.verizon.agtech.data.WeatherStationReadings;
import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;
import com.verizon.agtech.data.dao.service.WeatherStationReadingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("internal-api/weatherStationReadings")
@Api(value = "/internal-api/weatherStationReadings", tags = "WeatherStation Internal", description = "Internal API for weather station readings REST operations")
public class InternalWeatherStationReadingController {

    @Autowired
    private WeatherStationReadingService weatherStationReadingService;
    private final static Logger logger = LoggerFactory.getLogger(InternalWeatherStationReadingController.class);

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create a new weather station reading", notes = "Create a new  weather station reading.")
    public Model createWeatherStationReadingDocument(@ApiIgnore final Model model,
                                                     @RequestBody
                                                     @ApiParam(name = "weatherStationReadingDocument", value = "Weather Station Reading Document", required = true)
                                                             WeatherStationReadingDocument weatherStationReadingDocument) {
        weatherStationReadingService.saveWeatherStationReadingDocument(weatherStationReadingDocument);
        model.addAttribute("weatherStationReading", weatherStationReadingDocument);
        logger.info("Weather station reading document created " + weatherStationReadingDocument.getId());
        return model;
    }


    @RequestMapping(value = "/{weatherStationDocumentId}", method = RequestMethod.DELETE)
    @ApiOperation(value = "Delete the weather station reading", notes = "Delete the weather station reading.")
    public Model deleteWeatherStationReadingDocument(@ApiIgnore final Model model, @PathVariable String weatherStationDocumentId) {
        Boolean deleted = weatherStationReadingService.removeWeatherStationReadingDocument(weatherStationDocumentId);
        model.addAttribute("weatherStationDocumentId", weatherStationDocumentId);
        model.addAttribute("deleted", deleted);
        logger.info("Weather station reading document id: " + weatherStationDocumentId + " deleted :" + deleted);
        return model;
    }

}
