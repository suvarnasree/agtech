package com.verizon.agtech.data.pusher.delegate;

import com.verizon.agtech.data.pusher.util.PusherUtils;
import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.CollectorDelegate;
import com.verizon.agtech.data.akka.StopWorkException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

/**
 * Created by drq on 4/12/14.
 */
public class SFTPPusher extends CollectorDelegate {
    public final static String NAME = "SFTP Pusher";
    private final static Logger logger = LoggerFactory.getLogger(SFTPPusher.class);

    @Autowired
    private MessageSource messageSource;

    private MessageChannel targetChannel;

    public void setTargetChannel(MessageChannel targetChannel) {
        this.targetChannel = targetChannel;
    }

    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (logger.isDebugEnabled()) {
            logger.debug(messageSource.getMessage("delegate.entering", new Object[]{NAME}, null));
        }
        
        return data;

        /*
        if (data instanceof Outages) {
            Outages outages = (Outages) data;
            byte[] bytes = outages.getFormattedOutages().getBytes();

            String fileName = PusherUtils.getNotificationFileName(outages.getJobDetails());

            Message<byte[]> message = MessageBuilder.withPayload(bytes).setHeader("fileName", fileName).build();

            if (targetChannel != null) {
                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("Sending message to Channel %s", targetChannel.toString()));
                }
                targetChannel.send(message);
            }

            if (logger.isDebugEnabled()) {
                logger.debug(messageSource.getMessage("delegate.leaving", new Object[]{NAME}, null));
            }
            return data;
        } else {
            throw new StopWorkException(messageSource.getMessage("delegate.invalidData", new Object[]{NAME, Outages.class.getCanonicalName(), data.getClass().getCanonicalName()}, null));
        }
        */
    }
}
