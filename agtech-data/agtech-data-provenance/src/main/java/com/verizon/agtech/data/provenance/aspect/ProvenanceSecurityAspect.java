package com.verizon.agtech.data.provenance.aspect;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created by IntelliJ IDEA.
 * User: drq
 * Date: 10/19/13
 * Time: 9:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProvenanceSecurityAspect extends ProvenanceSender implements MethodInterceptor {

    private static Log logger = LogFactory.getLog(ProvenanceSecurityAspect.class);

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {

        Object returnObject = methodInvocation.proceed();

        //TODO: Send security provenance event
        /*
        boolean isVoter = methodInvocation instanceof AccessDecisionVoter;
        boolean isManager = methodInvocation instanceof AccessDecisionManager;

        if (isVoter) {
            RequestContext context = RequestContextSingleton.getRequestContext();

            context.addAccessDecision("always-say-yes", (Integer) returnObject);

            logger.info("Request user is now" + RequestContextSingleton.getRequestContext().getRequestUser());
        }

        logger.info("Request user is now" + RequestContextSingleton.getRequestContext().getRequestUser());
        */
        return returnObject;
    }
}
