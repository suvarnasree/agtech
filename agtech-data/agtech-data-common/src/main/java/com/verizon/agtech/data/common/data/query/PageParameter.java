package com.verizon.agtech.data.common.data.query;

/**
 * Created by drq on 9/13/14.
 */
public class PageParameter {
    private Integer page;

    private Integer size;

    public PageParameter() {
        this.page = 0;
        this.size = 20;
    }

    public PageParameter(Integer page, Integer size) {
        this.page = page;
        this.size = size;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
