package com.verizon.agtech.data.akka;

/**
 * Created by drq on 4/12/14.
 */
public class StopWorkException extends RuntimeException {
    public StopWorkException() {
    }

    public StopWorkException(String s) {
        super(s);
    }

    public StopWorkException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public StopWorkException(Throwable throwable) {
        super(throwable);
    }
}
