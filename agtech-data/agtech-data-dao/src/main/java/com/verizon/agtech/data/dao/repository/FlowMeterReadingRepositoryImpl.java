package com.verizon.agtech.data.dao.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by drq on 6/8/15.
 */
public class FlowMeterReadingRepositoryImpl implements FlowMeterReadingRepositoryCustom {
    private final static Logger logger = LoggerFactory.getLogger(FlowMeterReadingRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;
}
