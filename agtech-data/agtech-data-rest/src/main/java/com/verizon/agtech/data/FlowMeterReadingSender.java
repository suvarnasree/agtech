package com.verizon.agtech.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReading;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;

import java.util.Date;
import java.util.UUID;

/**
 * Created by drq on 4/24/15.
 */
public class FlowMeterReadingSender {
    private final static Logger logger = LoggerFactory.getLogger(FlowMeterReadingSender.class);

    private AmqpTemplate messageTemplate;
    private String targetQueue;

    public void setMessageTemplate(AmqpTemplate messageTemplate) {
        this.messageTemplate = messageTemplate;
    }

    public void setTargetQueue(String targetQueue) {
        this.targetQueue = targetQueue;
    }

    public FlowMeterReadingSender() {
    }

    public void sendNotification(FlowMeterReading flowMeterReading, String type) throws Exception {
        if (logger.isDebugEnabled()) {
            logger.debug("Entering Flow Meter Reading Sender....");
        }

        try {
            MessageProperties properties = new MessageProperties();
            properties.setMessageId(UUID.randomUUID().toString());
            properties.setHeader("messageType", type);
            properties.setTimestamp(new Date());

            ObjectMapper mapper = new ObjectMapper();
            Message message = new Message(mapper.writeValueAsString(flowMeterReading).getBytes(), properties);

            messageTemplate.send(targetQueue, message);

            if (logger.isDebugEnabled()) {
                logger.debug("Leaving Flow Meter Reading Sender....");
            }
        } catch (Exception e) {
            logger.error("Flow Meter Reading Sender error", e);
            throw e;
        }
    }
}
