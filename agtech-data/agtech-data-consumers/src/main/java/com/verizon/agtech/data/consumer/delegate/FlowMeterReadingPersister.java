package com.verizon.agtech.data.consumer.delegate;

import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.verizon.CDFProtos;
import com.verizon.agtech.AgTechCDFProtos;
import com.verizon.agtech.AgTechCDFProtos.FlowMeterReading.Type;
import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.CollectorDelegate;
import com.verizon.agtech.data.akka.StopWorkException;
import com.verizon.agtech.data.common.data.AgTechDateTime;
import com.verizon.agtech.data.common.data.BusData;
import com.verizon.agtech.data.common.data.Measurement;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReading;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReadingType;
import com.verizon.agtech.data.dao.domain.FlowMeterReadingDocument;
import com.verizon.agtech.data.dao.service.FlowMeterReadingService;
import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import java.text.ParseException;
import java.util.Date;

import static com.verizon.agtech.data.common.data.flowmeter.FlowMeterReadingType.HEARTBEAT;

/**
 * Created by drq on 4/17/14.
 */
public class FlowMeterReadingPersister extends CollectorDelegate {

    @Autowired
    protected MessageSource messageSource;

    public final static String NAME = "FLOW_METER_READING_PERSISTER";

    private final static Logger logger = LoggerFactory.getLogger(FlowMeterReadingPersister.class);

    @Autowired
    private FlowMeterReadingService flowMeterReadingService;

    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (logger.isDebugEnabled()) {
            logger.debug(messageSource.getMessage("delegate.entering", new Object[]{NAME}, null));
        }
        // Persist bus data
        if (data instanceof BusData) {
            BusData busData = (BusData) data;

            String payload = busData.getPayload();
            byte[] bytes = payload.getBytes();
            if (logger.isDebugEnabled()) {
                logger.debug("Data binary:" + payload);
            }
            try {
                ExtensionRegistry extensionRegistry = ExtensionRegistry.newInstance();
                CDFProtos.registerAllExtensions(extensionRegistry);
                AgTechCDFProtos.registerAllExtensions(extensionRegistry);

                CDFProtos.Collection collection = CDFProtos.Collection.parseFrom(Base64.decodeBase64(bytes), extensionRegistry);

                for (CDFProtos.DataBlob dataBlob : collection.getDataList()) {
                    CDFProtos.Measurement measurement = dataBlob.getMeasure();
                    FlowMeterReadingDocument flowMeterReadingDocument = new FlowMeterReadingDocument();
                    flowMeterReadingDocument.setFlowMeterId(measurement.getSensorGuid());
                    if (logger.isDebugEnabled()) {
                        logger.debug(String.format("FlowMeter ID: %s", measurement.getSensorGuid()));
                    }
                    if (measurement.getTimespan() != null) {
                        try {
                            flowMeterReadingDocument.setTimeStamp(AgTechDateTime.getDefaultDateFormat().parse(measurement.getTimespan().getBegin()));
                        } catch (ParseException e) {

                        }
                    }
                    if (measurement != null) {
                        AgTechCDFProtos.FlowMeterReading flowMeterReading = measurement.getExtension(AgTechCDFProtos.flowMeterReading);
                        if (flowMeterReading != null) {
                            AgTechCDFProtos.FlowMeterMeasurement waterAmountMeasurement = flowMeterReading.getWaterAmount();
                            String waterAmountUnit = waterAmountMeasurement.getUnit();
                            Double waterAmountValue = waterAmountMeasurement.getValue();
                            flowMeterReadingDocument.setWaterAmount(new Measurement(waterAmountUnit, waterAmountValue));
                            if (logger.isDebugEnabled()) {
                                logger.debug(String.format("Water Amount: %s %f", waterAmountUnit, waterAmountValue));
                            }

                            switch (flowMeterReading.getType()) {
                                case IRRIGATION_START:
                                    flowMeterReadingDocument.setType(FlowMeterReadingType.IRRIGATION_START.toString());
                                    break;
                                case IRRIGATION_END:
                                    flowMeterReadingDocument.setType(FlowMeterReadingType.IRRIGATION_END.toString());
                                    break;
                                case IRRIGATION_UPDATE:
                                    flowMeterReadingDocument.setType(FlowMeterReadingType.IRRIGATION_UPDATE.toString());
                                    break;
                                case HEARTBEAT:
                                    flowMeterReadingDocument.setType(HEARTBEAT.toString());

                                    AgTechCDFProtos.FlowMeterMeasurement energyAvailableMeasurement = flowMeterReading.getEnergyAvailable();
                                    String energyAvailableUnit = energyAvailableMeasurement.getUnit();
                                    Double energyAvailableValue = energyAvailableMeasurement.getValue();
                                    flowMeterReadingDocument.setEnergyAvailable(new Measurement(energyAvailableUnit, energyAvailableValue));
                                    if (logger.isDebugEnabled()) {
                                        logger.debug(String.format("Energy Available: %s %f", energyAvailableUnit, energyAvailableValue));
                                    }
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    flowMeterReadingDocument = flowMeterReadingService.saveFlowMeterReadingDocument(flowMeterReadingDocument);

                    if (logger.isDebugEnabled()) {
                        logger.debug(String.format("Persist flowMeterReading %s", flowMeterReadingDocument.getId()));
                        logger.debug(messageSource.getMessage("delegate.leaving", new Object[]{NAME}, null));
                    }
                }
                return data;
            } catch (InvalidProtocolBufferException e) {
                throw new StopWorkException("Failed to decode protobuf binary", e);
            }
        } else {
            throw new StopWorkException(messageSource.getMessage("delegate.invalidData", new Object[]{NAME, FlowMeterReading.class.getCanonicalName(), data.getClass().getCanonicalName()}, null));
        }
    }
}
