package com.verizon.agtech.data.akka;

/**
 * Created by drq on 5/19/14.
 */
public interface CollectorRouter {
    public String router(Object data);
}
