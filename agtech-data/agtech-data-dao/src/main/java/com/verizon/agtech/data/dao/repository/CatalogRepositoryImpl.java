package com.verizon.agtech.data.dao.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by drq on 6/8/15.
 */
public class CatalogRepositoryImpl implements CatalogRepositoryCustom {
    private final static Logger logger = LoggerFactory.getLogger(CatalogRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;
}
