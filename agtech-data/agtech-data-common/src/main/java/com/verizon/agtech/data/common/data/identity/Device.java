package com.verizon.agtech.data.common.data.identity;

import java.util.List;

/**
 * Created by rangapo on 5/29/16.
 */
public class Device extends DeviceIdentity {
    private String id;
    private String type;
    private String catalogId;
    private List<Attribute> attributes;
    private List<Block> connectedBlocks;
    private List<DeviceIdentity> connectedDevices;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public List<Block> getConnectedBlocks() {
        return connectedBlocks;
    }

    public void setConnectedBlocks(List<Block> connectedBlocks) {
        this.connectedBlocks = connectedBlocks;
    }

    public List<DeviceIdentity> getConnectedDevices() {
        return connectedDevices;
    }

    public void setConnectedDevices(List<DeviceIdentity> connectedDevices) {
        this.connectedDevices = connectedDevices;
    }
}
