package com.verizon.agtech.data;

import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;

import javax.validation.constraints.NotNull;
import java.util.List;

public class WeatherStationReadings {
    @NotNull
    private String weatherStationId;

    @NotNull
    private WeatherStationReading[] readings;

    public WeatherStationReadings() {
    }

    public WeatherStationReadings(String weatherStationId, WeatherStationReading[] readings) {
        this.weatherStationId = weatherStationId;
        this.readings = readings;
    }

    public WeatherStationReadings(String weatherStationId, List<WeatherStationReadingDocument> weatherStationReadingDocumentList) {
        this.weatherStationId = weatherStationId;
        readings = new WeatherStationReading[weatherStationReadingDocumentList.size()];
        for (int index = 0; index < weatherStationReadingDocumentList.size(); index++) {
            readings[index] = new WeatherStationReading(weatherStationReadingDocumentList.get(index));
        }
    }

    public String getWeatherStationId() {
        return weatherStationId;
    }

    public void setWeatherStationId(String weatherStationId) {
        this.weatherStationId = weatherStationId;
    }

    public WeatherStationReading[] getReadings() {
        return readings;
    }

    public void setReadings(WeatherStationReading[] readings) {
        this.readings = readings;
    }
}
