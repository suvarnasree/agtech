package com.verizon.agtech.data.dao.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import java.util.Date;

/**
 * Created by Thani on 6/20/16.
 */
public class RememberMeTokenRepositoryImpl implements RememberMeTokenRespositoryCustom{

    private static final Logger logger = LoggerFactory.getLogger(RememberMeTokenRepositoryImpl.class);
    @Autowired
    private MongoTemplate mongoTemplate;

}
