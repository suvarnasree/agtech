package com.verizon.agtech.data.provenance.annotation;

/**
 * Created with IntelliJ IDEA.
 * User: bhersh
 * Date: 9/12/13
 * Time: 12:18 PM
 * To change this template use File | Settings | File Templates.
 */
public @interface InternalEvent
{
    String operationType();

    String operationClass();
}
