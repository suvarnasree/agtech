package com.verizon.agtech.data.common.data.weatherstation;

import javax.validation.constraints.NotNull;

public class WeatherStationReadings {

    @NotNull
    private WeatherStationData[] events;

    public WeatherStationReadings() {
    }

    public WeatherStationReadings(WeatherStationData[] events) {
        this.events = events;
    }

    public WeatherStationData[] getEvents() {
        return events;
    }

    public void setEvents(WeatherStationData[] events) {
        this.events = events;
    }


}
