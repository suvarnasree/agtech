package com.verizon.agtech.data.dao.domain;

import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.verizon.agtech.data.common.domain.AbstractAgTechDocument;

@Document(collection = "apiKeyDocument")
@CompoundIndexes({
})
public class APIKeyDocument extends AbstractAgTechDocument{

	
	String key;
	String customerId;
	String encryptedKey;
	
	
	public APIKeyDocument()
	{
		super();
	}
	public APIKeyDocument(String key,String CustomerId)
	{
		super();
		this.key=key;
		this.customerId=CustomerId;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String CustomerId) {
		this.customerId = CustomerId;
	}
	public String getEncryptedKey() {
		return encryptedKey;
	}
	public void setEncryptedKey(String encryptedKey) {
		this.encryptedKey = encryptedKey;
	}
	
}
