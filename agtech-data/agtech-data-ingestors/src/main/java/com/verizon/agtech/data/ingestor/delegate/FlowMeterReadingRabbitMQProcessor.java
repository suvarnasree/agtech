package com.verizon.agtech.data.ingestor.delegate;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.CollectorDelegate;
import com.verizon.agtech.data.akka.StopWorkException;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReading;
import com.verizon.agtech.data.ingestor.data.RabbitMQData;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

/**
 * Created by drq on 8/20/14.
 */
public class FlowMeterReadingRabbitMQProcessor extends IngestorDelegate {
    public final static String NAME = "FLOW_METER_READING_RABBITMQ_PROCESSOR";

    private final static Logger logger = LoggerFactory.getLogger(FlowMeterReadingRabbitMQProcessor.class);

    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (logger.isDebugEnabled()) {
            logger.debug(messageSource.getMessage("delegate.entering", new Object[]{NAME}, null));
        }

        if (data != null && data instanceof RabbitMQData) {
            try {
                RabbitMQData rabbitMQData = (RabbitMQData) data;
                String message = rabbitMQData.getPayload();

                ObjectMapper mapper = new ObjectMapper();
                FlowMeterReading flowMeterReading = mapper.readValue(message, FlowMeterReading.class);

                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("Message payload: %s", message));
                    logger.debug(String.format("FlowMeterReading: %s", flowMeterReading.toString()));
                    logger.debug(messageSource.getMessage("delegate.leaving", new Object[]{NAME}, null));
                }
                return flowMeterReading;
            } catch (Exception e) {
                throw new StopWorkException("Failed to process RabbitMQ message", e);
            }
        } else {
            throw new StopWorkException(messageSource.getMessage("delegate.invalidData", new Object[]{NAME, RabbitMQData.class.getCanonicalName(), data.getClass().getCanonicalName()}, null));
        }
    }
}
