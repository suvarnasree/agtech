package com.verizon.agtech.data.common.data.catalog;

import javax.validation.constraints.NotNull;


public class DeviceModel {
    private String id;
    @NotNull
    private String devicename;
    @NotNull
    private String type;
    @NotNull
    private String model;
    @NotNull
    private String manufacturer;

    private String description;

    public DeviceModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getdeviceName() {
        return devicename;
    }

    public void setdeviceName(String devicename) {
        this.devicename = devicename;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
