package com.verizon.agtech.data.dao.domain;

import com.verizon.agtech.data.common.domain.AbstractAgTechDocument;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by drq on 3/31/16.
 */
public class AbstractAgTechReadingDocument extends AbstractAgTechDocument {
    @NotNull
    private Date timeStamp;

    public AbstractAgTechReadingDocument() {
    }

    public AbstractAgTechReadingDocument(Date timeStamp) {
        super();
        this.timeStamp = timeStamp;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
