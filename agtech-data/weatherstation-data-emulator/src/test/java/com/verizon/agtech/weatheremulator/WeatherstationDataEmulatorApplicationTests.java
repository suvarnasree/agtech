package com.verizon.agtech.weatheremulator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.verizon.agtech.weatheremulator.config.WeatherstationDataEmulatorApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = WeatherstationDataEmulatorApplication.class)
@WebAppConfiguration
public class WeatherstationDataEmulatorApplicationTests {

	@Test
	public void contextLoads() {
	}

}
