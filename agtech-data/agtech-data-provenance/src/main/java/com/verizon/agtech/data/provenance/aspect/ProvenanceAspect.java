package com.verizon.agtech.data.provenance.aspect;


import com.verizon.agtech.data.provenance.annotation.ProvenanceOperation;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: drq
 * Date: 10/19/13
 * Time: 9:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class ProvenanceAspect extends ProvenanceSender implements MethodInterceptor {

    private static Log logger = LogFactory.getLog(ProvenanceAspect.class);

    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {

        if (logger.isDebugEnabled()) {
            logger.debug("Invoke provenance event");
        }

        Method method = methodInvocation.getMethod();

        ProvenanceOperation annotation = method.getAnnotation(ProvenanceOperation.class);
        String operationClass = annotation.operationClass();
        String operationType = annotation.operationType();

        if (logger.isDebugEnabled()) {
            logger.debug("Operation Class :" + operationClass);
            logger.debug("Operation Type  :" + operationType);
        }

        Object returnObject = methodInvocation.proceed();
        HashMap<String, Object> returnMap = new HashMap<String, Object>();

        if (returnObject instanceof HashMap) {
            returnMap = (HashMap<String, Object>) returnObject;
        }

        sendProvenanceEvent(operationClass, operationType, returnMap);
        return returnObject;
    }
}
