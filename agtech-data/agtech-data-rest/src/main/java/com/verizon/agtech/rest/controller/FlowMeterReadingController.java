package com.verizon.agtech.rest.controller;

import com.verizon.agtech.data.FlowMeterReadingSender;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterData;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReading;
import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReadings;
import com.verizon.agtech.data.common.data.identity.Device;
import com.verizon.agtech.data.common.data.identity.Entities;
import com.verizon.agtech.data.common.error.ResourceNotFoundException;
import com.verizon.agtech.data.dao.domain.APIKeyDocument;
import com.verizon.agtech.data.dao.domain.CustomerDocument;
import com.verizon.agtech.data.dao.domain.FlowMeterReadingDocument;
import com.verizon.agtech.data.dao.service.APIKeyService;
import com.verizon.agtech.data.dao.service.CustomerService;
import com.verizon.agtech.data.dao.service.FlowMeterReadingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created by drq on 3/31/16.
 */
@Controller
@RequestMapping("v1/customers")
@Api(value = "/v1/customers", tags = "FlowMeter", description = "Flow meter events REST operations")
public class FlowMeterReadingController {
	
	@Autowired
    private HttpServletRequest request;
	
	@Autowired
    private APIKeyService apiKeyService;
	
	@Autowired
    private CustomerService customerService;
	
    @Autowired
    private FlowMeterReadingService flowMeterReadingService;

    @Autowired
    private FlowMeterReadingSender flowMeterReadingSender;

    private final static Logger logger = LoggerFactory.getLogger(FlowMeterReadingController.class);

    @RequestMapping(value = "/{customerId}/flowMeters/events", method = RequestMethod.GET)
    @ApiOperation(value = "Query flow meter events", notes = "Readings of all the sensors of a specific block for the given time range.")
    public Model findAllFlowMeterReadingDocuments(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "customerId", value = "Customer Id", required = true)
                    String customerId,
			@RequestParam(value = "fromTime", required = false)
			@ApiParam(name = "fromTime", value = "From timestamp", required = false)
					Long fromTime,
            @RequestParam(value = "toTime", required = false)
            @ApiParam(name = "toTime", value = "To timestamp", required = false)
                    Long toTime,
            @RequestParam(value = "p", required = false, defaultValue = "0")
            @ApiParam(name = "p", value = "Start page number", required = false, defaultValue = "0")
                    Integer page,
            @ApiParam(name = "s", value = "Page size", required = false, defaultValue = "20")
            @RequestParam(value = "s", required = false, defaultValue = "20")
                    Integer size,
                    
                    @RequestHeader(value="VINTELUserName") String VINTELUserName,
                    @RequestHeader(value="APIKey") String apikey) {
    	logger.info("entering FlowMeterReadingController.findAllFlowMeterReadingDocuments(customerID)");
    	APIKeyDocument document = apiKeyService.findKeyCustomerId(VINTELUserName);
    	if(document == null) {
    		logger.info("VINTELUserName does not exist in Mongo DB");
    	}

		 if(document != null && document.getKey() != null && apikey != null) {
			 logger.info("VINTELUserName exists in Database");
			 if(document.getKey().trim().equals(apikey.trim())) {
				 logger.info("API-KEY validation successful");
				 DateTime startDateTime = fromTime != null ? new DateTime(fromTime, DateTimeZone.UTC) : new DateTime(0,DateTimeZone.UTC);
				 DateTime endDateTime = toTime != null ? new DateTime(toTime,DateTimeZone.UTC) : new DateTime(DateTimeZone.UTC);
				 CustomerDocument customerDocument = customerService.findCustomerByCustomerId(customerId);
				 if(customerDocument != null){

					 Entities entities = customerDocument.getEntities();
					 List<Device> devices = entities.getDevices();
					 List<FlowMeterReadingDocument> flowMeterReadings = new ArrayList<FlowMeterReadingDocument>();
					 boolean check = false;
					 if (devices.size() > 0) {

						 for (Device device : devices) {
							 if (device.getType().toLowerCase().matches("flow[\\s]*meter")) {
								 check = true;
								 logger.info("Id" + device.getId());
								 Page<FlowMeterReadingDocument> flowdocument = flowMeterReadingService.findFlowMeterReadingsByIDAndTimeRange(device.getSerialNumber(), startDateTime, endDateTime, page, size);
								 logger.info("document" + flowdocument);
								 if (flowdocument != null)
									 flowMeterReadings.addAll(flowdocument.getContent());
							 }
						 }
						 if (check) {
							 model.addAttribute("customerId", customerId);
							 model.addAttribute("fromTime", fromTime);
							 model.addAttribute("toTime", toTime);
							 model.addAttribute("events", flowMeterReadings.iterator());
							 model.addAttribute("page", page);
							 model.addAttribute("pageSize", size);
						 } else {
							 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
							 model.addAttribute("cause", "No flowmeter id is linked to this customer");
						 }
                      }
				 } else {
					 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
					 model.addAttribute("cause", "Customer Id" + customerId + "is not Valid");
				 }
              } else {
				 logger.info("API-KEY is not valid");
				 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				 return model.addAttribute("cause", "authentication failed - API KEY is not valid");
					}
		 } else {
			 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			 return model.addAttribute("cause", "VINTEL user is not valid user");
		 }

		 	Enumeration headerNames = request.getHeaderNames();
				while(headerNames.hasMoreElements()) {
		 			String key =(String) headerNames.nextElement();
					String value = request.getHeader(key);
					logger.info("key : " + key + " Value : " + value);
				}
		logger.info("exiting FlowMeterReadingController.findAllFlowMeterReadingDocuments(customerID)");
        return model;
		 
    }
    

    @RequestMapping(value = "/{customerId}/flowMeters/{flowMeterId}/events", method = RequestMethod.GET)
    @ApiOperation(value = "Query flow meter events", notes = "Readings of a specific sensor for the given time range.")
    public Model findFlowMeterReadingDocuments(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "customerId", value = "Customer Id", required = true)
                    String customerId,
            @PathVariable
            @ApiParam(name = "flowMeterId", value = "Flow Meter Id", required = true)
                    String flowMeterId,
            @RequestParam(value = "fromTime", required = false)
            @ApiParam(name = "fromTime", value = "From timestamp", required = false)
                    Long fromTime,
            @RequestParam(value = "toTime", required = false)
            @ApiParam(name = "toTime", value = "To timestamp", required = false)
                    Long toTime,
            @RequestParam(value = "p", required = false, defaultValue = "0")
            @ApiParam(name = "p", value = "Start page number", required = false, defaultValue = "0")
                    Integer page,
            @ApiParam(name = "s", value = "Page size", required = false, defaultValue = "20")
            @RequestParam(value = "s", required = false, defaultValue = "20")
                    Integer size,
                    @RequestHeader(value="VINTELUserName") String VINTELUserName,
                    @RequestHeader(value="APIKey") String apikey) {
    	logger.info("entering FlowMeterReadingController.findFlowMeterReadingDocuments(customerID)");
    	APIKeyDocument document = apiKeyService.findKeyCustomerId(VINTELUserName);
		
		 if(document != null && document.getKey() != null && apikey != null) {
			 logger.info("VINTELUserName exists in Database");
			 if(document.getKey().trim().equals(apikey.trim())) {
				 logger.info("API-KEY validation successful");
				 boolean flowmeterExist = validateFlowMeterRelatedToCustomer(customerId,flowMeterId,response,model);
				 if(flowmeterExist) {
					 DateTime startDateTime = fromTime != null ? new DateTime(fromTime,DateTimeZone.UTC) : new DateTime(0,DateTimeZone.UTC);
					 DateTime endDateTime = toTime != null ? new DateTime(toTime,DateTimeZone.UTC) : new DateTime(DateTimeZone.UTC);
					 
					 Page<FlowMeterReadingDocument> flowMeterReadings = flowMeterReadingService.findFlowMeterReadingsByIDAndTimeRange(flowMeterId, startDateTime, endDateTime, page, size);

					 model.addAttribute("customerId", customerId);
					 model.addAttribute("flowMeterId", flowMeterId);
					 model.addAttribute("fromTime", fromTime);
					 model.addAttribute("toTime", toTime);
                     model.addAttribute("events", flowMeterReadings.iterator());
					 model.addAttribute("page", page);
					 model.addAttribute("pageSize", size);
					 model.addAttribute("pageTotalItems", flowMeterReadings.getNumberOfElements());
					 model.addAttribute("hasNextPage", flowMeterReadings.hasNext());
					 model.addAttribute("hasPreviousPage", flowMeterReadings.hasPrevious());
					 model.addAttribute("totalPages", flowMeterReadings.getTotalPages());
				 }
				 else {
					 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
					 model.addAttribute("cause","Customer and FlowMeter are not related, check the ids");
				}
			 }
			 else {
				 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				 model.addAttribute("cause", "authentication failed - API KEY is not valid");}
		 } else {
			 logger.info("API-KEY is invalid");
			 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			 return model.addAttribute("cause", "VINTEL user is not valid user");
		 }

		 Enumeration headerNames = request.getHeaderNames();
		    	while(headerNames.hasMoreElements()) {
		   		String key =(String) headerNames.nextElement();
		    		String value = request.getHeader(key);
		   		logger.info("key : " + key + " Value : " + value);
		   	}
        return model;
    }

	@RequestMapping(value = "/{customerId}/flowMeters/{flowMeterId}/events", method = RequestMethod.POST)
    @ApiOperation(value = "Ingest Flow Meter Events", notes = "Ingest batch of flow meter events.")
    public Model createFlowMeterReadingDocument(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "customerId", value = "Customer Id", required = true)
                    String customerId,
            @PathVariable
            @ApiParam(name = "flowMeterId", value = "Flow Meter Id", required = true)
                    String flowMeterId,
            @RequestBody
            @ApiParam(name = "flowMeterReadings", value = "Flow meter events to be ingested", required = true)
                    FlowMeterReadings flowMeterReadings,
           @RequestHeader(value="api-key") String apikey) throws Exception {
		
		  	Enumeration headerNames = request.getHeaderNames();
		    	while(headerNames.hasMoreElements()) {
		   		String key =(String) headerNames.nextElement();
		   		String value = request.getHeader(key);
		   		logger.info("key : " + key + " Value : " + value);
		   	}
 
    	if(customerId == null || apikey == null) {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    		return model.addAttribute("cause", "apikey and customer id are required");
    	}
    	
    	if(customerId != null && customerId.length() > 0 && apikey != null && apikey.length() > 0) {
    		CustomerDocument customerDocument = customerService.findCustomerByCustomerId(customerId);
    		if(customerDocument != null) {
    		String apikeyByVintel = customerDocument.getKey();
    		logger.info("apikeyByVintel : " + apikeyByVintel + " apikey : " + apikey);
    		if(apikeyByVintel != null && apikeyByVintel.length() > 0) {
    			
    			if(apikeyByVintel.equals(apikey)) {
    				boolean flowmeterExist = validateFlowMeterRelatedToCustomer(customerId,flowMeterId, response,model );
      				 if(flowmeterExist) {
    				for (FlowMeterData flowMeterData : flowMeterReadings.getEvents()) {
    		            flowMeterReadingSender.sendNotification(new FlowMeterReading(flowMeterData.getTimeStamp(), flowMeterData.getWaterAmount(), flowMeterData.getEnergyAvailable(), flowMeterData.getType(), flowMeterId), FlowMeterReading.NAME);
    		        }
    		        model.addAttribute("customerId", customerId);
    		        model.addAttribute("flowMeterId", flowMeterId);
    		        model.addAttribute("payload", flowMeterReadings);
    		        } else {
    		        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    					return  model.addAttribute(" cause ", "This customer(Id: " +customerId +") doesn't have  (Id:  "+flowMeterId+") ");
    				 }
    			} else {
    				logger.info("authentication failed");
					response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    				return model.addAttribute("cause", "authentication failed - ApiKey is not valid");
    			}
    		}
    		} else {
    			logger.info("customer id does not exit in mongo db" );
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    			model.addAttribute("cause", "Customer id  "+customerId+" not found");
    			}
    		}
    return model;    
 }

	private boolean validateFlowMeterRelatedToCustomer(String customerId,String flowmeterId,HttpServletResponse response,Model model) throws ResourceNotFoundException {
		CustomerDocument customerDocument = customerService.findCustomerByCustomerId(customerId);
		if(customerDocument != null) {
			Iterator<Device> iterator = customerDocument.getEntities().getDevices().iterator();
			while (iterator.hasNext()) {
				Device device = iterator.next();
				if (device.getSerialNumber().equalsIgnoreCase(flowmeterId)) {
					return true;
				}
            }
         } else {
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			model.addAttribute("msg","Customer with Id" + customerId + " is not valid");
          }
		return false;

	}
}
