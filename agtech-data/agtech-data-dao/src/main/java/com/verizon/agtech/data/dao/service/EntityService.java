package com.verizon.agtech.data.dao.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.verizon.agtech.data.common.data.entity.Entity;
import com.verizon.agtech.data.common.data.entity.EntityDetail;
import com.verizon.agtech.data.common.data.entity.EntityItem;
import com.verizon.agtech.data.dao.domain.CatalogDocument;
import com.verizon.agtech.data.dao.domain.CustomerDocument;
import com.verizon.agtech.data.dao.domain.EntityDocument;
import com.verizon.agtech.data.dao.repository.CustomerRepository;
import com.verizon.agtech.data.dao.repository.EntityRepository;

@Repository
@Transactional
public class EntityService {

    private final static Logger logger = LoggerFactory.getLogger(EntityService.class);

    @Autowired
    private EntityRepository entityRepository;
    
    @Autowired
    private CustomerRepository customerRepository;

    public EntityDocument findEntityById(String id) {
        return entityRepository.findById(id);
    }

    public EntityDocument findEntityByGuid(String guid) {
        return entityRepository.findByGuid(guid);
    }

    public EntityDocument findEntityByAnyId(String outageId) {
        EntityDocument entityDocument = entityRepository.findById(outageId);
        if (entityDocument == null) {
            entityDocument = entityRepository.findByGuid(outageId);
        }
        return entityDocument;
    }
    
    public EntityDocument findEntityByCustomerId(String guid) {
        return entityRepository.findByCustomerId(guid);
    }
    
    public EntityDocument findEntityByCatalogName(String catalogName) {
        return entityRepository.findByCatalog(catalogName);
    }
   
    public Page<EntityDocument> findEntityDocuments(int page, int size) {
        return entityRepository.findAll(new PageRequest(page, size));
    }

    public EntityDocument saveEntityDocument(EntityDocument entityDocument) {
        return entityRepository.save(entityDocument);
    }

    public Boolean removeEntityDocument(String entityId) {
        EntityDocument entityDocumentDocument = this.findEntityById(entityId);
        entityRepository.delete(entityDocumentDocument);
        return this.findEntityById(entityId) == null;
    }

    public Boolean removeEntityDocument(EntityDocument entityDocument) {
        String entityId = entityDocument.getId();
        entityRepository.delete(entityDocument);
        return this.findEntityById(entityId) == null;
    }

    public Page<EntityDocument> findEntitiesByCustomerId(String customerId, int page, int size) {
        return entityRepository.findByCustomerId(customerId, new PageRequest(page, size));
    }

    public EntityDocument updateEntityDocument(Entity entity, String customerId, CatalogDocument catalogDocument) {
    	EntityDocument entityDocument = entityRepository.findByCustomerId(customerId);
    	EntityItem item = new EntityItem();
    	if(entityDocument!=null)
    	{
    		 entityDocument.setCatalog(catalogDocument);
    		 List<EntityItem> catalogItems = entityDocument.getEntityItems();
    		 item.setSerialNumber(entity.getSerialNumber());
    		 item.setDeviceType(entity.getDeviceType());
    		 item.setBlockName(entity.getBlockName());
    		 catalogItems.add(item);
    	}
    	else
    	{
    		List<EntityItem> catalogItems = new ArrayList<EntityItem>();
    		  entityDocument = new EntityDocument(customerId);
    	        entityDocument.setCatalog(catalogDocument);
    	        item.setSerialNumber(entity.getSerialNumber());
    	        item.setDeviceType(entity.getDeviceType());
    	        item.setBlockName(entity.getBlockName());
    	        catalogItems.add(item);
    	        entityDocument.setEntityItems(catalogItems);
    	}
       
       return entityRepository.save(entityDocument);
    }
    
    public EntityDocument findEntityByAnyName(String catalogName) {
		// TODO Auto-generated method stub
		return entityRepository.findByCatalog(catalogName);
	}

	public Page<EntityDocument> findEntitiesByAccountName(String accountName, Integer page, Integer size) {
		// TODO Auto-generated method stub
		
		CustomerDocument customerDocument = customerRepository.findByAccountName(accountName);
		return entityRepository.findByCustomerId(customerDocument.getId(), new PageRequest(page, size));
	}
	
	public EntityDocument updateEntityDocument(EntityDocument entityDocument, Entity entity) throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		 entityDocument = entityRepository.findByCustomerId(entityDocument.getCustomerId());
		 List<EntityDetail> catalogItems = new ArrayList<EntityDetail>();
		 
		 List<EntityItem> entityItems = new ArrayList<EntityItem>();
		 List<EntityItem> catalogItems_v1 = new ArrayList<EntityItem>(entityItems);
		 EntityItem item = new EntityItem();
		if(entity != null)
		{
			if(entityDocument!=null)
	    	{
	    		entityItems = entityDocument.getEntityItems();
				boolean checkItem = false;
				for(EntityItem entityItem:entityItems)
				{
					if(entityItem.getSerialNumber().equalsIgnoreCase(entity.getSerialNumber()))
					{
						item.setDetail(entity.getEntityDetail());
						item.setBlockName(entityItem.getBlockName());
						item.setDeviceType(entityItem.getDeviceType());
						item.setSerialNumber(entityItem.getSerialNumber());
						checkItem=true;
						catalogItems_v1.remove(entityItem);
						//catalogItems_v1.g
						catalogItems_v1.add(item);
						
						}
					}
				if(!checkItem)
				{
					 item.setSerialNumber(entity.getSerialNumber());
		    		 item.setDeviceType(entity.getDeviceType());
		    		 item.setBlockName(entity.getBlockName());
		    		 item.setDetail(entity.getEntityDetail());
		    		 entityItems.add(item);
		    		 entityDocument.setEntityItems(entityItems);
				}
				else
				{
					 entityDocument.setEntityItems(catalogItems_v1);
				}
				//entityItems.add(item);
	    		 
	    	}
	    	else
	    	{
	    		    item.setSerialNumber(entity.getSerialNumber());
	    	        item.setDeviceType(entity.getDeviceType());
	    	        item.setBlockName(entity.getBlockName());
	    	        item.setDetail(entity.getEntityDetail());
	    	        entityItems.add(item);
	    	        entityDocument.setEntityItems(entityItems);
	    	}
			if(entityDocument.getEntityDetail()!=null && entityDocument.getEntityDetail().size()==0)
			{
			catalogItems.add(entity.getEntityDetail());
			entityDocument.setEntityDetail(catalogItems);
			}
			else
			{
				//List<EntityDetail> catalogItems = entityDocument.getEntityDetail();
				catalogItems.add(entity.getEntityDetail());
				entityDocument.setEntityDetail(catalogItems);
				}
			}
			  return entityRepository.save(entityDocument);
	}

    private byte[] convertLinesToBytes (String[] lines) {
       if (lines != null ) {
           return StringUtils.join(lines, System.getProperty("line.separator")).getBytes();
       } else {
           return null;
       }
    }
}
