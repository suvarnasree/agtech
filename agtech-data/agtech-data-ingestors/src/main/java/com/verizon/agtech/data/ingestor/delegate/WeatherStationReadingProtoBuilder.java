package com.verizon.agtech.data.ingestor.delegate;

import com.verizon.CDFProtos;
import com.verizon.agtech.AgTechCDFProtos;
import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.StopWorkException;
import com.verizon.agtech.data.common.data.AgTechDateTime;
import com.verizon.agtech.data.common.data.Measurement;
import com.verizon.agtech.data.common.data.weatherstation.WeatherStationData;
import com.verizon.agtech.data.ingestor.data.AgTechProtoData;
import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.UUID;

public class WeatherStationReadingProtoBuilder extends IngestorDelegate {
    public final static String NAME = "WEATHER_STATION_READING_PROTO_BUILDER";

    private final static Logger logger = LoggerFactory.getLogger(WeatherStationReadingProtoBuilder.class);

    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (logger.isDebugEnabled()) {
            logger.debug(messageSource.getMessage("delegate.entering", new Object[]{NAME}, null));
        }

        if (data != null && data instanceof WeatherStationData) {
            WeatherStationData weatherStationData = (WeatherStationData) data;

            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Weather Station Reading: %s", weatherStationData.toString()));
            }

            // Set up proto collection builder
            CDFProtos.Collection.Builder collectionBuilder = CDFProtos.Collection.newBuilder();

            // Set up collection basic info
            String collectionGuid = UUID.randomUUID().toString();
            collectionBuilder.setGuid(collectionGuid);
            collectionBuilder.setType("flat");
            collectionBuilder.setMode(CDFProtos.Collection.CollectionMode.STATIC);
            collectionBuilder.setStatus(CDFProtos.Collection.Status.GOOD);
            collectionBuilder.setFriendlyName("");
            collectionBuilder.setTriageMode(CDFProtos.Collection.TriageMode.NONE);
            collectionBuilder.setTriageValue("");
            collectionBuilder.setParentGuid("");

            // Set up data blob builder
            CDFProtos.DataBlob.Builder dataBlobBuilder = CDFProtos.DataBlob.newBuilder();
            dataBlobBuilder.setGuid(UUID.randomUUID().toString());
            dataBlobBuilder.setFriendlyName("agtech");
            dataBlobBuilder.setDataType(CDFProtos.DataBlob.Type.MEASUREMENT);

            AgTechCDFProtos.WeatherStationReading.Builder weatherStationBuilder = AgTechCDFProtos.WeatherStationReading.newBuilder();

            Measurement relativeHumidity = weatherStationData.getRelativeHumidity();
            if(relativeHumidity != null) {
                AgTechCDFProtos.WeatherStationMeasurement.Builder relativeHumidityReadingBuilder= AgTechCDFProtos.WeatherStationMeasurement.newBuilder();
                relativeHumidityReadingBuilder.setUnit(relativeHumidity.getUnit());
                relativeHumidityReadingBuilder.setValue(relativeHumidity.getValue().toString());
                weatherStationBuilder.setRelativeHumidity(relativeHumidityReadingBuilder.build());
            }

            Measurement meanTemperature = weatherStationData.getMeanTemperature();
            if(meanTemperature != null){
                AgTechCDFProtos.WeatherStationMeasurement.Builder meanTemperatureReadingBuilder= AgTechCDFProtos.WeatherStationMeasurement.newBuilder();
                meanTemperatureReadingBuilder.setUnit(meanTemperature.getUnit());
                meanTemperatureReadingBuilder.setValue(meanTemperature.getValue().toString());
                weatherStationBuilder.setMeanTemperature(meanTemperatureReadingBuilder.build());
            }

            Measurement windSpeed = weatherStationData.getWindSpeed();
            if(windSpeed != null){
                AgTechCDFProtos.WeatherStationMeasurement.Builder windSpeedReadingBuilder= AgTechCDFProtos.WeatherStationMeasurement.newBuilder();
                windSpeedReadingBuilder.setUnit(windSpeed.getUnit());
                windSpeedReadingBuilder.setValue(windSpeed.getValue().toString());
                weatherStationBuilder.setWindSpeed(windSpeedReadingBuilder.build());
            }

            Measurement solarRadiation = weatherStationData.getSolarRadiation();
            if(solarRadiation != null){
                AgTechCDFProtos.WeatherStationMeasurement.Builder solarRadiationReadingBuilder= AgTechCDFProtos.WeatherStationMeasurement.newBuilder();
                solarRadiationReadingBuilder.setUnit(solarRadiation.getUnit());
                solarRadiationReadingBuilder.setValue(solarRadiation.getValue().toString());
                weatherStationBuilder.setSolarRadiation(solarRadiationReadingBuilder.build());
            }

            Measurement totalPrecipitation = weatherStationData.getTotalPrecipitation();
            if(totalPrecipitation != null){
                AgTechCDFProtos.WeatherStationMeasurement.Builder totalPrecipitationReadingBuilder= AgTechCDFProtos.WeatherStationMeasurement.newBuilder();
                totalPrecipitationReadingBuilder.setUnit(totalPrecipitation.getUnit());
                totalPrecipitationReadingBuilder.setValue(totalPrecipitation.getValue().toString());
                weatherStationBuilder.setTotalPrecipitation(totalPrecipitationReadingBuilder.build());
            }

            CDFProtos.Measurement.Builder measurementBuilder = CDFProtos.Measurement.newBuilder();

            CDFProtos.Time.Builder timeBuilder = CDFProtos.Time.newBuilder();
            timeBuilder.setBegin(AgTechDateTime.getDefaultDateFormat().format(weatherStationData.getTimeStamp()));
            timeBuilder.setEnd("");

            measurementBuilder.setTimespan(timeBuilder.build());

            measurementBuilder.setExtension(AgTechCDFProtos.weatherStationReading, weatherStationBuilder.build());
            measurementBuilder.setSensorGuid(weatherStationData.getWeatherStationID());

            CDFProtos.Units.Builder unitsBuilder = CDFProtos.Units.newBuilder();
            unitsBuilder.setType(CDFProtos.Units.Type.OTHERS);
            measurementBuilder.setUnits(unitsBuilder.build());

            measurementBuilder.setEntityUid("");

            dataBlobBuilder.setMeasure(measurementBuilder.build());

            collectionBuilder.addData(dataBlobBuilder.build());

            CDFProtos.Collection collection = collectionBuilder.build();

            if (logger.isDebugEnabled()) {

            }

            ByteArrayOutputStream bOutput = new ByteArrayOutputStream();
            try {
                collection.writeTo(bOutput);
                if (logger.isDebugEnabled()) {
                    logger.debug("Encoded Binary Data: " + Base64.encodeBase64((bOutput.toByteArray())));
                    logger.debug(messageSource.getMessage("delegate.leaving", new Object[]{NAME}, null));
                }
                return new AgTechProtoData(bOutput.toByteArray());
            } catch (IOException e) {
                throw new StopWorkException("Failed to get binary data", e);
            } finally {
                try {
                    bOutput.close();
                } catch (IOException e) {
                    logger.error("Failed to close the output stream", e);
                }
            }
        } else {
            throw new StopWorkException(messageSource.getMessage("delegate.invalidData", new Object[]{NAME, WeatherStationData.class.getCanonicalName(), data.getClass().getCanonicalName()}, null));
        }
    }
}
