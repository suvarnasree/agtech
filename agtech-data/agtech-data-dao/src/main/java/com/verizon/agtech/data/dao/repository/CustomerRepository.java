package com.verizon.agtech.data.dao.repository;

import com.verizon.agtech.data.dao.domain.CustomerDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface CustomerRepository extends PagingAndSortingRepository<CustomerDocument, Long>, CustomerRepositoryCustom {
    /**
     * Returns the customer with the given identifier.
     *
     * @param id
     * @return
     */
    CustomerDocument findOne(Long id);

    /**
     * Saves the given {@link CustomerDocument}.
     *
     * @param customer
     * @return
     */
    CustomerDocument save(CustomerDocument customer);

    /**
     * Removes the given {@link CustomerDocument}.
     *
     * @param customer
     * @return
     */
    void delete(CustomerDocument customer);

    /**
     * @param id
     * @return
     */
    CustomerDocument findById(String id);

    /**
     * @param guid
     * @return
     */
    CustomerDocument findByGuid(String guid);


    /**
     * @param ecpId
     * @return CustomerDocument
     */
    @Query("{'ecpdId': ?0}")
    CustomerDocument findByEcpdId(String ecpdId);


    /**
     * @return
     */
    Page<CustomerDocument> findAll(Pageable pageable);

	//CustomerDocument findByCustomerName(String id);

	CustomerDocument findByAccountName(String id);
}
