package com.verizon.agtech.data.common.data.query;

/**
 * Created by drq on 9/13/14.
 */
public enum ItemType {

    ALL("all"),
    ALARM("alarm"),
    UTILITY("utility"),
    ELIGIBILITY("eligibility"),
    UNKNOWN("unknown");

    private String status;

    /**
     * @param status
     */
    ItemType(String status) {
        this.status = status;
    }

    /**
     * @return
     */
    public String toString() {
        return this.status;
    }

    /**
     * @param text
     * @return
     */
    public static ItemType fromString(String text) {
        if (text != null) {
            for (ItemType b : ItemType.values()) {
                if (text.equalsIgnoreCase(b.status)) {
                    return b;
                }
            }
        }
        return UNKNOWN;
    }
}
