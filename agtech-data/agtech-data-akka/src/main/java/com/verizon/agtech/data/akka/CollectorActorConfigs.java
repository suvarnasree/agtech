package com.verizon.agtech.data.akka;

import akka.routing.RouterConfig;

/**
 * Created by drq on 4/15/14.
 */
public class CollectorActorConfigs {
    private String name;
    private RouterConfig routerConfig;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RouterConfig getRouterConfig() {
        return routerConfig;
    }

    public void setRouterConfig(RouterConfig routerConfig) {
        this.routerConfig = routerConfig;
    }
}
