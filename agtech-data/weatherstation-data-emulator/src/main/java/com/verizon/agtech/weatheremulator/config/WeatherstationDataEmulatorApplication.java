package com.verizon.agtech.weatheremulator.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class WeatherstationDataEmulatorApplication {

	public static void main(String[] args) {
		System.out.println("Hello: " +args[0] );
		//System.out.println("Hello 1: " +args[1] );
		
		
		
		//var fancyFruit = {"name":"pear", "color": "greenish"};
		SpringApplication.run(WeatherstationDataEmulatorApplication.class, args);
	}
}
