package com.verizon.agtech.data.common.data.query;

/**
 * Created by drq on 9/13/14.
 */
public class QueryParameter {
    private ItemType itemType;
    private Circle circle;
    private PageParameter pageParameter;
    private String company;
    private String address;

    public QueryParameter() {
        this.itemType = ItemType.ALL;
        this.circle = new Circle();
        this.pageParameter = new PageParameter(0, 20);
    }

    public QueryParameter(ItemType itemType, Circle circle, PageParameter page, String company) {
        this.itemType = itemType;
        this.circle = circle;
        this.pageParameter = page;
        this.company = company;
    }

    public QueryParameter(ItemType itemType, Circle circle, PageParameter page, String company, String address) {
        this.itemType = itemType;
        this.circle = circle;
        this.pageParameter = page;
        this.company = company;
        this.address = address;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public PageParameter getPageParameter() {
        return pageParameter;
    }

    public void setPageParameter(PageParameter pageParameter) {
        this.pageParameter = pageParameter;
    }

    public String getCompany() {

        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
