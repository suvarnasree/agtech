package com.verizon.agtech.data.dao.service;

import com.verizon.agtech.data.dao.domain.CatalogDocument;
import com.verizon.agtech.data.dao.repository.CatalogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class CatalogService {

    private final static Logger logger = LoggerFactory.getLogger(CatalogService.class);

    @Autowired
    private CatalogRepository catalogRepository;

    public CatalogDocument findCatalogById(String id) {
        return catalogRepository.findById(id);
    }

    public CatalogDocument findCatalogByGuid(String guid) {
        return catalogRepository.findByGuid(guid);
    }

    public CatalogDocument findCatalogByAnyId(String id) {
        CatalogDocument catalogDocument = findCatalogById(id);
        if(catalogDocument == null){
            catalogDocument = findCatalogByGuid(id);
        }
        return catalogDocument;
    }
    public Page<CatalogDocument> findCatalogDocuments(int page, int size) {
        return catalogRepository.findAll(new PageRequest(page, size));
    }

    public CatalogDocument saveCatalogDocument(CatalogDocument catalogDocument) {
        return catalogRepository.save(catalogDocument);
    }

    public Boolean removeCatalogDocument(String catalogId) {
        CatalogDocument catalogDocumentDocument = this.findCatalogById(catalogId);
        catalogRepository.delete(catalogDocumentDocument);
        return this.findCatalogById(catalogId) == null;
    }

    public Boolean removeCatalogDocument(CatalogDocument catalogDocument) {
        String catalogId = catalogDocument.getId();
        catalogRepository.delete(catalogDocument);
        return this.findCatalogById(catalogId) == null;
    }

    public Page<CatalogDocument> findCatalogs(int page, int size) {
        return catalogRepository.findAll(new PageRequest(page, size));
    }

	public CatalogDocument findByCatalogName(String catalogName) {
		return catalogRepository.findByName(catalogName);
	}

	
}
