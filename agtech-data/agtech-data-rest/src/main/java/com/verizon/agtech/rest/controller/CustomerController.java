package com.verizon.agtech.rest.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.verizon.agtech.data.common.data.identity.Block;
import com.verizon.agtech.data.common.data.identity.Customer;
import com.verizon.agtech.data.common.data.identity.Entities;
import com.verizon.agtech.data.common.error.ResourceExistsException;
import com.verizon.agtech.data.common.error.ResourceNotFoundException;
import com.verizon.agtech.data.dao.domain.CustomerDocument;
import com.verizon.agtech.data.dao.service.CustomerService;
import com.verizon.agtech.data.pusher.CustomerAdditionEmailNotifier;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.bson.types.ObjectId;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import javax.mail.*;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by drq on 3/31/16.
 */
@Controller
@RequestMapping("v1/customers")
@Api(value = "/v1/customers", tags = "Customer", description = "Customer REST operations")
public class CustomerController {
    @Autowired
    private CustomerService customerService;

    @Autowired
    private CustomerAdditionEmailNotifier customerAdditionEmailNotifier;

    private final static Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @RequestMapping(method = RequestMethod.GET)
    @ApiOperation(value = "List customers", notes = "Returns the current list of customers")
    public Model findAllCustomerDocuments(
            @ApiIgnore
            final Model model,
            @PathVariable
            @RequestParam(value = "p", required = false, defaultValue = "0")
            @ApiParam(name = "p", value = "Start page number", required = false, defaultValue = "0")
                    Integer page,
            @ApiParam(name = "s", value = "Page size", required = false, defaultValue = "200")
            @RequestParam(value = "s", required = false, defaultValue = "200")
                    Integer size) {
    	logger.info("entering CustomerController.findAllCustomerDocuments()");
    	logger.info("entering  CustomerService.findAllCustomerDocuments()");

        Page<CustomerDocument> customers = customerService.findCustomers(page, size);

        model.addAttribute("customers", customers.iterator());
        model.addAttribute("page", page);
        model.addAttribute("pageSize", size);
        model.addAttribute("pageTotalItems", customers.getNumberOfElements());
        model.addAttribute("hasNextPage", customers.hasNext());
        model.addAttribute("hasPreviousPage", customers.hasPrevious());
        model.addAttribute("totalPages", customers.getTotalPages());
        logger.info("exiting CustomerController.findAllCustomerDocuments()");
        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    @ApiOperation(value = "Create customer", notes = "Create a new customer account.")
    public Model createCustomerDocument(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @RequestBody
            @ApiParam(name = "customer", value = "Customer", required = true)
                    Customer customer) throws NoSuchAlgorithmException {
    	logger.info("entering CustomerController.createCustomerDocument()");
    	logger.info("entering customerService.findByCustomerName()");
        CustomerDocument customerDocument = customerService.findAccountByAnyName(customer.getAccountName());
        logger.info("exiting customerService.findByCustomerName()");
        String key = generate(128);
        if (customerDocument == null) {
        	logger.info("customer document is null due to catalog name does not exist");
            this.updateBockWithBlockId(customer);
            customerDocument = new CustomerDocument(customer.getAccountName(),  customer.getFirstName(), customer.getLastName(),
                    customer.getEmail(), customer.getPhoneNumber(), customer.getEcpdId(), customer.getAddress(), customer.getEntities(), key);
            logger.info("entering customerService.saveCusstomerDocument(customerDocument)");
            customerDocument = customerService.saveCustomerDocument(customerDocument);
            logger.info("exiting customerService.saveCustomerDocument(customerDocument)");
            model.addAttribute("customer", customerDocument);
            logger.info("exiting CustomerController.saveCustomerDocument(customerDocument)");
        } else {
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            model.addAttribute("cause", "AccountName  "+customer.getAccountName()+"  already exists" );
        }
        return model;
    }
    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    @ApiOperation(value = "Update customer", notes = "Updates a customer account details.")
    public Model updateCustomerDocument(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "id", value = "Customer Id", required = true) String id,
            @RequestBody
            @ApiParam(name = "customer", value = "Customer", required = true)
                    Customer customer) {
    	logger.info("entering CustomerController.updateCustomerDocument(customerID)");
    	logger.info("entering customergService.findCustomerByAnyId(id)");
        CustomerDocument customerDocument = customerService.findCustomerByAnyId(id);
        if (customerDocument != null && customerDocument.getAccountName().equals(customer.getAccountName())) {
            updateCustomerDetails(customer,customerDocument,model);
        } else if(customerDocument != null && customerDocument.getAccountName() != customer.getAccountName()) {
            CustomerDocument customerDocument1 = customerService.findAccountByAnyName(customer.getAccountName());
            if (customerDocument1 != null) {
            	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
               model.addAttribute("cause", "Account  Name "+customer.getAccountName()+"  already exists");
            } else {
            	updateCustomerDetails(customer,customerDocument,model);
            }
        } else {
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        	model.addAttribute("cause", "customer  "+id+"  is not found" );
          }
        return model;
    }

    private void updateCustomerDetails(Customer customer,CustomerDocument customerDocument,final Model model) {
            logger.info("customer document is null due to customer id is not found in mongo db");
            this.updateBockWithBlockId(customer);
            customerDocument.setAccountName(customer.getAccountName());
            customerDocument.setFirstName(customer.getFirstName());
            customerDocument.setLastName(customer.getLastName());
            customerDocument.setEmail(customer.getEmail());
            customerDocument.setPhoneNumber(customer.getPhoneNumber());
            customerDocument.setEcpdId(customer.getEcpdId());
            customerDocument.setAddress(customer.getAddress());
            customerDocument.setEntities(customer.getEntities());
            logger.info("entering customerService.saveCustomerDocument(customerDocument)");
            customerDocument = customerService.saveCustomerDocument(customerDocument);
            logger.info("exiting customerService.saveCustomerDocument(customerDocument)");
            model.addAttribute("customer", customerDocument);
            logger.info("exiting CustomerController.updateCustomerDocument(customerID)");
            
    }
    

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ApiOperation(value = "Query customer details", notes = "Details of a specific customer for the given customer id.")
    public Model getCustomerDocument(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "id", value = "Customer Id", required = true) String id) {
    	logger.info("entering CustomerController.getCustomerDetailsById(customerID)");
    	logger.info("entering customerService.findCustomerByAnyId(customerID)");
        CustomerDocument customerDocument = customerService.findCustomerByAnyId(id);
        logger.info("exiting customerService.findCustomerByAnyId(customerID)");
        if (customerDocument != null) {
            model.addAttribute("customer", customerDocument);
        } else {
        	logger.info("customer document is null due to customer id is not found in mongo db");
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
           return  model.addAttribute("cause", "customerId "+id+"  not found" );
        }
        logger.info("exiting CustomerController.getCustomerDetailsById(customerDocument)");
        return model;
    }

    private String generate(final int keyLen) throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(keyLen);
        SecretKey secretKey = keyGen.generateKey();
        byte[] encoded = secretKey.getEncoded();
        return DatatypeConverter.printHexBinary(encoded).toLowerCase();
    }

    
    @RequestMapping(value = "/{CustomerId}/{APIKey}", method = RequestMethod.GET)
    @ApiOperation(value = "Validate apikey against customer Id", notes = "Validate apikey against customer id")
    public Map<String, Boolean> getKeyCustomerId(
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "CustomerId", value = "CustomerId", required = true)
                    String CustomerId,
            @PathVariable
            @ApiParam(name = "APIKey", value = "APIKey", required = true)
                    String APIKey) throws NoSuchAlgorithmException {
    	logger.info("entering CustomerController.getCustomerDetailsById(customerID)");
    	logger.info("entering customerService.findCustomerByAnyId(customerID)");

        CustomerDocument document = customerService.findCustomerByAnyId(CustomerId);
        logger.info("exiting customerService.findCustomerByAnyId(customerID)");
        if (document != null && document.getKey() != null && APIKey != null) {
            return Collections.singletonMap("success", document.getKey().trim().equals(APIKey.trim()));
        } else {
        	return Collections.singletonMap("success", false);
        }
    }
    
    
    @RequestMapping(value = "/{id}/getApiKey", method = RequestMethod.GET)
    @ApiOperation(value = "Validate apikey against customer Id", notes = "Validate apikey against customer id")
    public Model getApiKeyForCustomerId(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "id", value = "Id", required = true)
                    String id
            ) throws NoSuchAlgorithmException {
    	logger.info("entering CustomerController.getCustomerDetailsById(customerID)");
    	logger.info("entering customerService.findCustomerByAnyId(customerID)");
    	CustomerDocument customerDocument = customerService.findCustomerById(id);
    	if(customerDocument != null){
    		String apiKey= customerService.findCustomerByAnyId(id).getKey();
    		model.addAttribute("ApiKey", apiKey);
        } else {
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        	model.addAttribute("cause", "customer  "+id+"  is not found" );
        }
    		return model;
    }

    @RequestMapping(value = "emailNotification/{customerId}", method = RequestMethod.GET)
    @ApiOperation(value = "Email Notification to Itk" , notes = " Email Notification on new customer Addition through Mail")
    public Model deliverMailToItk(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "customerId" , value = "cusomerId",required = true)
                    String customerId) throws JSONException, ResourceNotFoundException, MessagingException, IOException {
        logger.info("Processing Customer-Added-Email Notification to CustomerId : " + customerId);
        CustomerDocument customerDocument = customerService.findCustomerByCustomerId(customerId);
        if (customerDocument!=null) {
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                File customerJson = new File("Customer.json");
                objectMapper.writerWithDefaultPrettyPrinter().writeValue(customerJson,customerDocument);
                customerAdditionEmailNotifier.sendEmail(customerJson);
                model.addAttribute("EmailNotification","Mail Delivered to the itk with CustomerId" + customerId);
            } catch (JsonProcessingException e) {
                logger.error("Conversion of CustomerDocument to jsonFile failed for CustomerId: "  +customerId+ "during Email Processing " + e.getMessage());
            }
        } else {
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            model.addAttribute("cause", "customer  "+customerId+"  is not found" );
        }
           return model;
    }

    private void updateBockWithBlockId(Customer customer) {
        if (customer == null) return;

        Entities entities = customer.getEntities();
        if (entities == null) return;

        List<Block> blocks = entities.getBlocks();
        if (blocks == null || blocks.isEmpty()) {
            return;
        }
        Iterator<Block> iterator = blocks.iterator();
        while (iterator.hasNext()) {
            Block block = iterator.next();
            if (block.getId() == null || block.getId().isEmpty()) {
                block.setId(new ObjectId().toString());
            }
        }
    }
}