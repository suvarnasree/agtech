package com.verizon.agtech.data.common.request;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class RequestContext implements Serializable {
    private String requestId;
    private Map<String, Integer> accessDecisions = new HashMap<String, Integer>();
    private String requestUser;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getRequestUser() {
        return requestUser;
    }

    public void setRequestUser(String requestUser) {
        this.requestUser = requestUser;
    }

    public void addAccessDecision(String voterName, int decision) {
        accessDecisions.put(voterName, decision);
    }
}
