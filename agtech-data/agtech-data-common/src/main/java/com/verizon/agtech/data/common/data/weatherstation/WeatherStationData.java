package com.verizon.agtech.data.common.data.weatherstation;

import com.verizon.agtech.data.common.data.Measurement;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class WeatherStationData {
    public static final String NAME = "WeatherStationData";
    @NotNull
    private String weatherStationID;
    @NotNull
    private Date timeStamp;
    private Measurement meanTemperature;
    private Measurement windSpeed;
    private Measurement relativeHumidity;
    private Measurement totalPrecipitation;
    private Measurement solarRadiation;

    public WeatherStationData() {
    }

    public WeatherStationData(String weatherStationID, Date timeStamp,
                              Measurement meanTemperature,
                              Measurement windSpeed, Measurement relativeHumidity,
                              Measurement totalPrecipitation, Measurement solarRadiation) {
        this.weatherStationID = weatherStationID;
        this.timeStamp = timeStamp;
        this.meanTemperature = meanTemperature;
        this.windSpeed = windSpeed;
        this.relativeHumidity = relativeHumidity;
        this.totalPrecipitation = totalPrecipitation;
        this.solarRadiation = solarRadiation;
    }

    public String getWeatherStationID() {
        return weatherStationID;
    }

    public void setWeatherStationID(String weatherStationID) {
        this.weatherStationID = weatherStationID;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Measurement getMeanTemperature() {
        return meanTemperature;
    }

    public void setMeanTemperature(Measurement meanTemperature) {
        this.meanTemperature = meanTemperature;
    }

    public Measurement getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Measurement windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Measurement getRelativeHumidity() {
        return relativeHumidity;
    }

    public void setRelativeHumidity(Measurement relativeHumidity) {
        this.relativeHumidity = relativeHumidity;
    }

    public Measurement getTotalPrecipitation() {
        return totalPrecipitation;
    }

    public void setTotalPrecipitation(Measurement totalPrecipitation) {
        this.totalPrecipitation = totalPrecipitation;
    }

    public Measurement getSolarRadiation() {
        return solarRadiation;
    }

    public void setSolarRadiation(Measurement solarRadiation) {
        this.solarRadiation = solarRadiation;
    }

    @Override
    public String toString() {
        return "WeatherStationData{" +
                "weatherStationID='" + weatherStationID + '\'' +
                ", timeStamp=" + timeStamp +
                ", meanTemperature=" + meanTemperature +
                ", windSpeed=" + windSpeed +
                ", relativeHumidity=" + relativeHumidity +
                ", totalPrecipitation=" + totalPrecipitation +
                ", solarRadiation=" + solarRadiation +
                '}';
    }
}
