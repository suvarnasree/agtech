package com.verizon.agtech.data.common.data.query;

import org.springframework.data.geo.Metrics;

/**
 * Created by drq on 9/13/14.
 */
public class Circle {
    private Double longitude;
    private Double latitude;
    private Double radius;
    private Metrics unit;

    public Circle() {
        this.longitude = 0.0;
        this.latitude = 0.0;
        this.radius = 0.0;
        this.unit = Metrics.NEUTRAL;
    }

    public Circle(Double longitude, Double latitude, Double radius, Metrics unit) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.radius = radius;
        this.unit = unit;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Metrics getUnit() {
        return unit;
    }

    public void setUnit(Metrics unit) {
        this.unit = unit;
    }
}
