package com.verizon.agtech.data.ingestor.data;

/**
 * Created by drq on 9/5/14.
 */
public class AgTechProtoData implements ProtoData {

    private byte[] bytes;

    public AgTechProtoData(byte[] bytes) {
        this.bytes = bytes;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
