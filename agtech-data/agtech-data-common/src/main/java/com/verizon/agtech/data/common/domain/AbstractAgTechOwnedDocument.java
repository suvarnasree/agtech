package com.verizon.agtech.data.common.domain;

/**
 * Created by drq on 4/21/16.
 */
public class AbstractAgTechOwnedDocument extends AbstractAgTechDocument {

    private String customerId;

    public AbstractAgTechOwnedDocument() {
        super();
    }

    public AbstractAgTechOwnedDocument(String customerId) {
        super();
        this.customerId = customerId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
