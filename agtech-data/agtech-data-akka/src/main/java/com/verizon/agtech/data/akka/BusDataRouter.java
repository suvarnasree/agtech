package com.verizon.agtech.data.akka;

import com.verizon.agtech.data.common.data.BusData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by drq on 5/19/14.
 */
public class BusDataRouter implements CollectorRouter {

    private final static Logger logger = LoggerFactory.getLogger(BusDataRouter.class);

    private String routerKey;

    private Map<String, String> routerMap;

    public String getRouterKey() {
        return routerKey;
    }

    public void setRouterKey(String routerKey) {
        this.routerKey = routerKey;
    }

    public Map<String, String> getRouterMap() {
        return routerMap;
    }

    public void setRouterMap(Map<String, String> routerMap) {
        this.routerMap = routerMap;
    }

    @Override
    public String router(Object data) {
        if (data instanceof BusData) {
            Map<String, Object> headers = ((BusData) data).getHeaders();
            if (routerKey != null && headers.containsKey(routerKey)) {
                String delegateBeanName = routerMap.get(headers.get(routerKey).toString());
                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("Route to delegate %s", delegateBeanName));
                }
                return delegateBeanName;
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Invalid router key");
                }
                return null;
            }
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Invalid data for router");
            }
            return null;
        }
    }
}
