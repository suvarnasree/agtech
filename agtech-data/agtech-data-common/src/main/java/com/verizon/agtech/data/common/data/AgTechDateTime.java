package com.verizon.agtech.data.common.data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 * Created by drq on 4/8/16.
 */
public class AgTechDateTime {
    public static DateFormat getDefaultDateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        TimeZone timeZone = TimeZone.getTimeZone("America/New_York");
        sdf.setTimeZone(timeZone);
        return sdf;
    }
}
