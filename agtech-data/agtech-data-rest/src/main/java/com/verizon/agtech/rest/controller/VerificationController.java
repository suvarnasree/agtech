package com.verizon.agtech.rest.controller;

import com.verizon.agtech.data.common.data.flowmeter.FlowMeterReadings;
import com.verizon.agtech.data.dao.domain.CustomerDocument;
import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;
import com.verizon.agtech.data.dao.service.VerificationService;
import com.verizon.agtech.data.dao.service.WeatherStationReadingService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

/**
 * Created by Thani on 6/15/16.
 */
@Controller
@RequestMapping("v1/file")
public class VerificationController {


    @Autowired
    private WeatherStationReadingService weatherStationReadingService;

    @Autowired
    private VerificationService verificationService;

    @RequestMapping(value = "Verification/customer/{customerId}", method = RequestMethod.GET)
    @ApiOperation(value = "Verification of Customer Devices", notes = "This Api returns the status of each devices for its working condition")
    public Model verificationCustomerDocument(
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "customerId", value = "Customer Id", required = true)
                    String customerId ) throws Exception {

        CustomerDocument verificationDocument = verificationService.populateVerificationStatusOfDevices(customerId);
        //Page<WeatherStationReadingDocument> weatherStationReadingDocuments = weatherStationReadingService.findLatestWeatherStationReadingDocument(weatherStationID);
        model.addAttribute("VerificationDocument", verificationDocument);

        return model;
    }


}

