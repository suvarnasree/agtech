package com.verizon.agtech.data.common.data.identity;

/**
 * Created by rangapo on 5/29/16.
 */
public class Block {
    private String id;
    private String blockName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBlockName() {
        return blockName;
    }

    public void setBlockName(String blockName) {
        this.blockName = blockName;
    }
}
