package com.verizon.weatherstationemulator.model;

import java.io.Serializable;

public class Measurement  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7520197901284822303L;
	private String unit;
    private Object value;
	public Measurement(String unit, Object value) {
		super();
		this.unit = unit;
		this.value = value;
	}
	public String getUnit() {
		return unit;
	}
	
	public Object getValue() {
		return value;
	}
	
	
	public boolean equals(Object o)
	  {
	    if (this == o) {
	      return true;
	    }
	    if (!(o instanceof Measurement)) {
	      return false;
	    }
	    Measurement measure = (Measurement)o;
	    if (this.unit != null ? !this.unit.equals(measure.unit) : measure.unit != null) {
	      return false;
	    }
	    return measure.value == null ? true : this.value != null ? this.value.equals(measure.value) : false;
	  }
	  
	  public int hashCode()
	  {
	    int result = this.unit != null ? this.unit.hashCode() : 0;
	    result = 31 * result + (this.value != null ? this.value.hashCode() : 0);
	    return result;
	  }
	}
    
    
	

