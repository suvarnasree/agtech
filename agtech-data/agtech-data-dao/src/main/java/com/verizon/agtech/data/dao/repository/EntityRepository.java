package com.verizon.agtech.data.dao.repository;

import com.verizon.agtech.data.dao.domain.EntityDocument;
import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface EntityRepository extends PagingAndSortingRepository<EntityDocument, Long>, EntityRepositoryCustom {
    /**
     * Returns the customer with the given identifier.
     *
     * @param id
     * @return
     */
    EntityDocument findOne(Long id);


    /**
     * Saves the given {@link EntityDocument}.
     *
     * @param entity
     * @return
     */
    EntityDocument save(EntityDocument entity);

    /**
     * Removes the given {@link EntityDocument}.
     *
     * @param entity
     * @return
     */
    void delete(EntityDocument entity);

    /**
     * @param id
     * @return
     */
    EntityDocument findById(String id);
    
    EntityDocument findByCustomerId(String id);
    EntityDocument findByCatalog(String catalogNname);

    /**
     * @param guid
     * @return
     */
    EntityDocument findByGuid(String guid);

    /**
     * @return
     */
    Page<EntityDocument> findAll(Pageable pageable);

    /**
     *
     * @param customerId
     * @param pageable
     * @return
     */
    Page<EntityDocument> findByCustomerId(String customerId, Pageable pageable);

	//Page<EntityDocument> findByAccountName(String accountName, Pageable pageable);
}
