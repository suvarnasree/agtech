package com.verizon.agtech.data.ingestor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jivesoftware.smack.util.Base64;
import org.springframework.util.Log4jConfigurer;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

/**
 * Created by drq on 8/6/15.
 */
@Test
@ContextConfiguration({"/xmpp-send-test-context.xml"})
public class MessageSenderTest extends AbstractTestNGSpringContextTests {

    private static final Log logger = LogFactory.getLog(MessageSenderTest.class);

    @Autowired
    @Qualifier("TestChannel1")
    private MessageChannel targetChannel1;

    public MessageChannel getTargetChannel1() {
        return targetChannel1;
    }

    public void setTargetChannel1(MessageChannel targetChannel1) {
        this.targetChannel1 = targetChannel1;
    }

    @Autowired
    @Qualifier("TestChannel2")
    private MessageChannel targetChannel2;

    public MessageChannel getTargetChannel2() {
        return targetChannel2;
    }

    public void setTargetChannel2(MessageChannel targetChannel2) {
        this.targetChannel2 = targetChannel2;
    }

    @Autowired
    @Qualifier("TestChannel3")
    private MessageChannel targetChannel3;

    public MessageChannel getTargetChannel3() {
        return targetChannel3;
    }

    public void setTargetChannel3(MessageChannel targetChannel3) {
        this.targetChannel3 = targetChannel3;
    }

    @Autowired
    @Qualifier("TestChannel4")
    private MessageChannel targetChannel4;

    public MessageChannel getTargetChannel4() {
        return targetChannel4;
    }

    public void setTargetChannel4(MessageChannel targetChannel4) {
        this.targetChannel4 = targetChannel4;
    }

    @Autowired
    @Qualifier("TestChannel5")
    private MessageChannel targetChannel5;

    public MessageChannel getTargetChannel5() {
        return targetChannel5;
    }

    public void setTargetChannel5(MessageChannel targetChannel5) {
        this.targetChannel5 = targetChannel5;
    }

    @Autowired
    @Qualifier("TestChannel6")
    private MessageChannel targetChannel6;

    public MessageChannel getTargetChannel6() {
        return targetChannel6;
    }

    public void setTargetChannel6(MessageChannel targetChannel6) {
        this.targetChannel6 = targetChannel6;
    }

    @Autowired
    @Qualifier("TestChannel7")
    private MessageChannel targetChannel7;

    public MessageChannel getTargetChannel7() {
        return targetChannel7;
    }

    public void setTargetChannel7(MessageChannel targetChannel7) {
        this.targetChannel7 = targetChannel7;
    }

    @Autowired
    @Qualifier("TestChannel8")
    private MessageChannel targetChannel8;

    public MessageChannel getTargetChannel8() {
        return targetChannel8;
    }

    public void setTargetChannel8(MessageChannel targetChannel8) {
        this.targetChannel8 = targetChannel8;
    }

    @Autowired
    @Qualifier("TestChannel9")
    private MessageChannel targetChannel9;

    public MessageChannel getTargetChannel9() {
        return targetChannel9;
    }

    public void setTargetChannel9(MessageChannel targetChannel9) {
        this.targetChannel9 = targetChannel9;
    }

    @Autowired
    @Qualifier("TestChannel10")
    private MessageChannel targetChannel10;

    public MessageChannel getTargetChannel10() {
        return targetChannel10;
    }

    public void setTargetChannel10(MessageChannel targetChannel10) {
        this.targetChannel10 = targetChannel10;
    }

    private String testMessage = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

    private int numberOfMessages;
    private int numberOfActiveChannels;
    private ArrayList<MessageChannel> messageChannels = new ArrayList<MessageChannel>();

    @BeforeClass
    public void setUp() throws Exception {
        try {
            Log4jConfigurer.initLogging("classpath:log4j.xml");
        } catch (FileNotFoundException ex) {
            System.err.println("Cannot Initialize log4j");
        }

        numberOfMessages = 100000;
        numberOfActiveChannels = 10;
        messageChannels.add(targetChannel1);
        messageChannels.add(targetChannel2);
        messageChannels.add(targetChannel3);
        messageChannels.add(targetChannel4);
        messageChannels.add(targetChannel5);
        messageChannels.add(targetChannel6);
        messageChannels.add(targetChannel7);
        messageChannels.add(targetChannel8);
        messageChannels.add(targetChannel9);
        messageChannels.add(targetChannel10);
    }

    @AfterClass
    public void tearDown() throws Exception {

    }

    @Test(threadPoolSize=10, invocationCount=10)
    public void messageSenderTest() {
        Date startTime = new Date();
        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Test started @ %s", startTime));
        }
        for (int i = 0; i < numberOfMessages; i++) {
            MessageChannel messageChannel = messageChannels.get(randInt(0, numberOfActiveChannels - 1));
            byte[] bytes = testMessage.getBytes();
            Message<String> message = MessageBuilder.withPayload(Base64.encodeBytes(bytes)).build();

            if (targetChannel1 != null) {
                if (logger.isDebugEnabled()) {
                    logger.debug(String.format("Sending message %d to Channel %s with size %d", i, messageChannel.toString(), bytes.length));
                }
                targetChannel1.send(message);
            }

        }
        Date endTime = new Date();
        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Test ended @ %s", endTime));
            logger.debug(String.format("Total time : %d ", endTime.getTime() - startTime.getTime()));
        }
    }

    private static int randInt(int min, int max) {
        // Usually this can be a field rather than a method variable
        Random rand = new Random();
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
}
