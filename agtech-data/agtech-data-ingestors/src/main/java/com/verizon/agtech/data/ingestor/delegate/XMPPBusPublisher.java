package com.verizon.agtech.data.ingestor.delegate;

import com.verizon.agtech.data.ingestor.data.ProtoData;
import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.CollectorDelegate;
import org.jivesoftware.smack.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import java.util.Map;

/**
 * Created by drq on 4/12/14.
 */
public class XMPPBusPublisher extends IngestorDelegate {
    public final static String NAME = "XMPP_BUS_PUBLISHER";

    private final static Logger logger = LoggerFactory.getLogger(XMPPBusPublisher.class);

    private MessageChannel targetChannel;

    private Map<String, ?> headers;

    public MessageChannel getTargetChannel() {
        return targetChannel;
    }

    public void setTargetChannel(MessageChannel targetChannel) {
        this.targetChannel = targetChannel;
    }

    public void setHeaders(Map<String, ?> headers) {
        this.headers = headers;
    }

    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (logger.isDebugEnabled()) {
            logger.debug(messageSource.getMessage("delegate.entering", new Object[]{NAME}, null));
        }

        byte[] bytes = ((ProtoData) data).getBytes();
        if (logger.isDebugEnabled()) {
            for (String headerKey : headers.keySet()) {
                logger.debug("Header:" + headerKey + " " + "Value:" + headers.get(headerKey));
            }
        }

        Message<String> message = MessageBuilder.withPayload(Base64.encodeBytes(bytes))
                .copyHeaders(headers).build();

        if (logger.isDebugEnabled()) {
            for (String key : message.getHeaders().keySet()) {
                logger.debug(String.format("Header: %s, Value: %s", key, message.getHeaders().get(key)));
            }
        }

        if (targetChannel != null) {
            if (logger.isDebugEnabled()) {
                logger.debug(String.format("Sending message to Channel %s", targetChannel.toString()));
            }
            targetChannel.send(message);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(messageSource.getMessage("delegate.leaving", new Object[]{NAME}, null));
        }
        return data;
    }
}
