package com.verizon.agtech.data.dao.service;

import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;
import com.verizon.agtech.data.dao.repository.WeatherStationReadingRepository;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.Date;
import java.util.List;
import org.springframework.data.domain.*;

/**
 * Created by drq on 2/3/14.
 */
@Repository
@Transactional
public class WeatherStationReadingService {

    private final static Logger logger = LoggerFactory.getLogger(WeatherStationReadingService.class);

    @Autowired
    private WeatherStationReadingRepository weatherStationReadingRepository;

    public WeatherStationReadingDocument findWeatherStationReadingById(String id) {
        return weatherStationReadingRepository.findById(id);
    }

    public WeatherStationReadingDocument findWeatherStationReadingByGuid(String guid) {
        return weatherStationReadingRepository.findByGuid(guid);
    }
    public WeatherStationReadingDocument findWeatherStationReadingByWeatherStationId(String weatherStationId) {
        return weatherStationReadingRepository.findByWeatherStationID(weatherStationId);
    }
    
    public WeatherStationReadingDocument findWeatherStationReadingByAnyId(String outageId) {
        WeatherStationReadingDocument weatherStationReadingDocument = weatherStationReadingRepository.findById(outageId);
        if (weatherStationReadingDocument == null) {
            weatherStationReadingDocument = weatherStationReadingRepository.findByGuid(outageId);
        }
        return weatherStationReadingDocument;
    }

    public Page<WeatherStationReadingDocument> findWeatherStationReadingDocuments(int page, int size) {
        return weatherStationReadingRepository.findAll(new PageRequest(page, size));
    }

    public WeatherStationReadingDocument saveWeatherStationReadingDocument(WeatherStationReadingDocument weatherStationReadingDocument) {
    	logger.info("entering WeatherStationReadingService.saveWeatherStationReadingDocument(WeatherID)");
    	logger.info("WeatherID : " + weatherStationReadingDocument.getWeatherStationID());
    	logger.info(" next call is for Mongo DB Spring API ");
        return weatherStationReadingRepository.save(weatherStationReadingDocument);
    }

    public Boolean removeWeatherStationReadingDocument(String weatherStationReadingId) {
        WeatherStationReadingDocument weatherStationReadingDocumentDocument = this.findWeatherStationReadingById(weatherStationReadingId);
        weatherStationReadingRepository.delete(weatherStationReadingDocumentDocument);
        return this.findWeatherStationReadingById(weatherStationReadingId) == null;
    }

    public Boolean removeWeatherStationReadingDocument(WeatherStationReadingDocument weatherStationReadingDocument) {
        String weatherStationReadingId = weatherStationReadingDocument.getId();
        weatherStationReadingRepository.delete(weatherStationReadingDocument);
        return this.findWeatherStationReadingById(weatherStationReadingId) == null;
    }

    public Page<WeatherStationReadingDocument> findWeatherStationReadingsByTimeStamp(DateTime startDateTime, DateTime endDateTime, int page, int size) {
    	logger.info("entering WeatherStationReadingService.findWeatherStationReadingsByIDAndTimeRange()");
    	logger.info(" next call is for Mongo DB Spring API ");
        return weatherStationReadingRepository.findWeatherStationReadingsByTimeStamp(startDateTime, endDateTime, new PageRequest(page, size));
    }

    public Page<WeatherStationReadingDocument> findWeatherStationReadingsByIDAndTimeRange(String id, DateTime startDateTime, DateTime endDateTime, int page, int size) {
    	logger.info("entering WeatherStationReadingService.findWeatherStationReadingsByIDAndTimeRange(WeatherId)");
    	logger.info("weatherID : " + id);
    	logger.info(" next call is for Mongo DB Spring API ");
        return weatherStationReadingRepository.findWeatherStationReadingsByIDAndTimeRange(id, startDateTime, endDateTime, new PageRequest(page, size));
    }

    public Page<WeatherStationReadingDocument> findLatestWeatherStationReadingDocument(String weatherStationD) {
        logger.info("Fetching the latest WeatherStationData for the Id:" + weatherStationD);
        return weatherStationReadingRepository.findLatestWeatherStationReadingPage(weatherStationD,new PageRequest(0,1, Sort.Direction.DESC,new String[]{"createdAt"}));

    }

}
