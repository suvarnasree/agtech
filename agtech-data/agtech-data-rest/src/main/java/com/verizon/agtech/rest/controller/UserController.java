package com.verizon.agtech.rest.controller;

import com.verizon.agtech.data.common.data.login.Login;
import com.verizon.agtech.data.dao.domain.RememberMeTockenDocument;
import com.verizon.agtech.data.dao.repository.RememberMeTokenRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import com.verizon.agtech.data.common.data.identity.User;
import com.verizon.agtech.data.dao.domain.UserDocument;
import com.verizon.agtech.data.dao.service.UserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by dontki7 on 6/22/16.
 */

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RememberMeTokenRepository rememberMeTokenRepository;

private final static Logger logger = LoggerFactory.getLogger(UserController.class);


    @RequestMapping(value = "registerUserAccount", method = RequestMethod.POST)
    @ApiOperation(value = "Register user account", notes = "Registering new User.")
    public Model createUserDocument(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @RequestBody
            @ApiParam(name = "user", value = "User", required = true)
                    User user) {
    	
     logger.info("entering UserRegistrationController.createUserDocument()");
     logger.info("entering userService.findUserByUserId()");
     UserDocument userDocument = new UserDocument( user.getUserName(),user.getRole() , user.getOneTimePassword());
     UserDocument userdocumentcheck = userService.findUserByName(user.getUserName());
     if(userdocumentcheck==null)
     {
    	   userDocument = userService.saveUserDocument(userDocument);
    	   model.addAttribute("user", userDocument); 
     } else
     {
    	 response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    	  model.addAttribute("cause", "user already exists" ); 
     }
      return model;
    }
    
    
    @RequestMapping(value = "v1/users", method= RequestMethod.GET)
    @ApiOperation(value = "List users", notes = "Returns the current list of users")
    public Model findAllUserDocuments(
            @ApiIgnore
            final Model model,
            @PathVariable
            @RequestParam(value = "p", required = false, defaultValue = "0")
            @ApiParam(name = "p", value = "Start page number", required = false, defaultValue = "0")
                    Integer page,
            @ApiParam(name = "s", value = "Page size", required = false, defaultValue = "200")
            @RequestParam(value = "s", required = false, defaultValue = "200")
                    Integer size) {
    	logger.info("entering UserRegistrationController.findAllUserDocuments()");
    	logger.info("entering  UserService.findAllUserDocuments()");

        Page<UserDocument> users = userService.findUsers(page, size);
        model.addAttribute("users", users.iterator());
        model.addAttribute("page", page);
        model.addAttribute("pageSize", size);
        model.addAttribute("pageTotalItems", users.getNumberOfElements());
        model.addAttribute("hasNextPage", users.hasNext());
        model.addAttribute("hasPreviousPage", users.hasPrevious());
        model.addAttribute("totalPages", users.getTotalPages());
        logger.info("exiting UserRegistrationController.findAllUserDocuments()");
        return model;
    }
    
    
    @RequestMapping(value = "v1/users/{userName}", method = RequestMethod.GET)
    @ApiOperation(value = "Query user details", notes = "Details of a specific user for the given userName .")
    public Model getUserDetails(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "userName", value = "User Name", required = true) String userName) {
    	logger.info("entering UserController.getUserByName(userName)");
    	logger.info("entering userService.findUserByName(userName)");
        UserDocument userDocument = userService.findUserByName(userName);
        logger.info("exiting userService.findUserByName(userName)");
        if(userDocument != null){
            model.addAttribute("userDocuments", userDocument);
        } else {
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        	logger.info("user document is null due to user id is not found in mongo db");
            model.addAttribute("cause", "user   "+userName+" not found ");
        }
        logger.info("exiting UserController.getUserByName(userName)");
        return model;
    }
    
    
    @RequestMapping(value = "v1/users/{userName}", method = RequestMethod.PUT)
    @ApiOperation(value = "Update user", notes = "Updates a user entry.")
    public Model updateUserDocument(HttpServletResponse response,
            @ApiIgnore
            final Model model,
            @PathVariable
            @ApiParam(name = "userName", value = "User Name", required = true) String userName,
            @RequestBody
            @ApiParam(name = "user", value = "User", required = true)
                    User user) {
    	logger.info("entering userController.updateUserDocument(userName)");
    	logger.info("entering userService.findUserByName(userName)");
       
        UserDocument userDocument = userService.findUserByName(userName);
        if (userDocument != null && userDocument.getUserName().equals(user.getUserName())){
        	updateUser(user, userDocument, model);
        } else if(userDocument != null && userDocument.getUserName() != user.getUserName()) {
        	UserDocument userDocument1 = userService.findUserById(user.getUserName());
        	if (userDocument1 != null) {
        		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        		model.addAttribute("cause", "user  Name "+user.getUserName()+"  already exists");
            }else {
            	updateUser(user, userDocument, model);
            }
        } else {
        	response.setStatus(HttpServletResponse.SC_FORBIDDEN);
             model.addAttribute("cause", "User not found");
        }
        	return model;
    }
    
    private void updateUser(User user,UserDocument userDocument, final Model model) {
    	
        userDocument.setUserName(user.getUserName());
        userDocument.setRole(user.getRole());
        userDocument.setOneTimePassword(user.getOneTimePassword());
        userDocument = userService.saveUserDocument(userDocument);
        model.addAttribute("user", userDocument);
     }
    


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation(value = "Login details to be shared", notes = "This operation shares the User with Role")
    public Model Login(
            @ApiIgnore
            final Model model,
            @RequestBody
            @ApiParam(name = "login", value = "login", required = true)
                    Login login ) throws Exception {

    	UserDocument userDocument = userService.findUserByName(login.getUsername());
        if(userDocument != null) {
            model.addAttribute("login",userDocument);
        } else {
            throw new Exception("userName not availble on System ");
        }
        return model;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    @ApiOperation(value = "Login details to be shared", notes = "This operation shares the User with Role")
    public Model Logout(
            @ApiIgnore
            final Model model) throws Exception {
        return model;
    }

}

