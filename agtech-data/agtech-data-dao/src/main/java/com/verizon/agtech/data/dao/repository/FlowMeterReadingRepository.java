package com.verizon.agtech.data.dao.repository;

import com.verizon.agtech.data.dao.domain.FlowMeterReadingDocument;
import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;

import org.joda.time.DateTime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by drq on 6/8/15.
 */
public interface FlowMeterReadingRepository extends PagingAndSortingRepository<FlowMeterReadingDocument, Long>, FlowMeterReadingRepositoryCustom {
    /**
     * Returns the customer with the given identifier.
     *
     * @param id
     * @return
     */
    FlowMeterReadingDocument findOne(Long id);

    /**
     * Saves the given {@link FlowMeterReadingDocument}.
     *
     * @param flowMeterReading
     * @return
     */
    FlowMeterReadingDocument save(FlowMeterReadingDocument flowMeterReading);

    /**
     * Removes the given {@link FlowMeterReadingDocument}.
     *
     * @param flowMeterReading
     * @return
     */
    void delete(FlowMeterReadingDocument flowMeterReading);

    /**
     * @param id
     * @return
     */
    FlowMeterReadingDocument findById(String id);

    /**
     * @param guid
     * @return
     */
    FlowMeterReadingDocument findByGuid(String guid);
    
    /**
     * @return
     */
    FlowMeterReadingDocument findByFlowMeterId(String flowMeterId);
    /**
     * @return
     */
    
    Page<FlowMeterReadingDocument> findAll(Pageable pageable);

    /**
     *
     * @param startDateTime
     * @param endDateTime
     * @param pageable
     * @return
     */
    @Query("{'timeStamp': {$gte: ?0, $lte: ?1}}")
    Page<FlowMeterReadingDocument> findFlowMeterReadingsByTimeStamp(DateTime startDateTime, DateTime endDateTime, Pageable pageable);

    /**
     *
     * @param id
     * @param startDateTime
     * @param endDateTime
     * @param pageable
     * @return
     */
    @Query("{'$and': [{'flowMeterId': ?0}, {'timeStamp': {$gte: ?1, $lte: ?2}}]}")
    Page<FlowMeterReadingDocument> findFlowMeterReadingsByIDAndTimeRange(String id, DateTime startDateTime, DateTime endDateTime, Pageable pageable);

    /**
     * This method returns the latest FlowMeter Reading Page fot the given Flow Meter Id
     * @param id
     * @param pageable
     * @return Page<FlowMeterReadingDocument>
     */

    @Query("{'$and': [{'flowMeterId': ?0},{'type': ?1}]}")
    Page<FlowMeterReadingDocument> findLatestFlowMeterReadingPage(String id, String heartBeatType,Pageable pageable);

}
