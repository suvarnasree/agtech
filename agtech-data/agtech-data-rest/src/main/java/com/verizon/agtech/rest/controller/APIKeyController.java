package com.verizon.agtech.rest.controller;

import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Map;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.verizon.agtech.data.dao.domain.APIKeyDocument;
import com.verizon.agtech.data.dao.service.APIKeyService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import springfox.documentation.annotations.ApiIgnore;

@Controller
@RequestMapping("v1/apiKey")
@Api(value = "/v1/apiKey", tags = "ApiKey", description = "API key Management")
public class APIKeyController {
	 @Autowired
	 private APIKeyService apiKeyService;
	 
	 @RequestMapping(value = "/{CustomerId}",method = RequestMethod.POST)
	 @ApiOperation(value = "Create Key for Customer", notes = "Creates a key for Customer")
	 public Model generateKeyForCustomer( @ApiIgnore
	     final Model model,
	     @PathVariable
	     @ApiParam(name = "CustomerId", value = "CustomerId", required = true)
	                    String CustomerId) throws NoSuchAlgorithmException {
		 
		 String key =generate(128);
		 APIKeyDocument document = apiKeyService.getApiCustomer(CustomerId);
		 if(document==null)
		 {
			  document = new APIKeyDocument(key,CustomerId);
			 apiKeyService.saveCatalogDocument(document);
		 }
		 model.addAttribute("apiKey", document);
		 return model;
		 
	 }
	 
	
	 @RequestMapping(value = "customer/{CustomerId}/APIKey/{APIKey}",method = RequestMethod.GET)
	 @ApiOperation(value = "Create Key for Customer", notes = "Creates a key for Customer")

	 public Map<String,Boolean> getKeyCustomerId( 
			  @ApiIgnore
	          final Model model,
	          @PathVariable
	          @ApiParam(name = "CustomerId", value = "CustomerId", required = true)
	                    String CustomerId,
	          @PathVariable
	          @ApiParam(name = "APIKey", value = "APIKey", required = true)
     					String APIKey) throws NoSuchAlgorithmException {
		 
		APIKeyDocument document = apiKeyService.findKeyCustomerId(CustomerId);
		 if(document != null && document.getKey() != null && APIKey != null) {
			 return Collections.singletonMap("success", document.getKey().trim().equals(APIKey.trim()));
		 	} 
		 else {
			 	return Collections.singletonMap("success", false);
		 }	 
	 }
	 
	 public static String generate(final int keyLen) throws NoSuchAlgorithmException {

	        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
	        keyGen.init(keyLen);
	        SecretKey secretKey = keyGen.generateKey();
	        byte[] encoded = secretKey.getEncoded();
	        return DatatypeConverter.printHexBinary(encoded).toLowerCase();
	    }
}
