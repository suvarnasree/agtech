package com.verizon.agtech.data.common.data.identity;

/**
 * Created by rangapo on 5/29/16.
 */
public class DeviceIdentity {
    private String serialNumber;
    private String name;
    private VerificationStatus verificationStatus;
    
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VerificationStatus getVerificationStatus() {
        return verificationStatus;
    }

    public void setVerificationStatus(VerificationStatus verificationStatus) {
        this.verificationStatus = verificationStatus;
    }

}

