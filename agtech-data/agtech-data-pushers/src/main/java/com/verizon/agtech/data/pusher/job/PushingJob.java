package com.verizon.agtech.data.pusher.job;

import com.verizon.agtech.data.akka.CollectorChain;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

/**
 * Created by drq on 10/2/15.
 */
public class PushingJob extends CollectorChain {
    public final static String NAME = "SFTP-PUSHER";

    private final static Logger logger = LoggerFactory.getLogger(PushingJob.class);

    public void pushingJobMethod () {
        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Pushing Job triggered at %s.", new Date()));
        }
        startingActor.tell("test", null);
    }
}
