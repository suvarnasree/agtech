package com.verizon.agtech.data.dao.domain;

import com.verizon.agtech.data.common.data.catalog.DeviceModel;
import com.verizon.agtech.data.common.domain.AbstractAgTechDocument;

import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

/**
 * Created by donthi
 */
@Document(collection = "catalogs")
@CompoundIndexes({
})
public class CatalogDocument extends AbstractAgTechDocument {
    @NotNull
    private String name;
    private List<DeviceModel> deviceModels = new ArrayList<>();

    public CatalogDocument(String name) {
        super();
        this.name = name;
    }

    public CatalogDocument() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public List<DeviceModel> getDeviceModels() {
        return deviceModels;
    }

    public void setDeviceModels(List<DeviceModel> deviceModels) {
        this.deviceModels = deviceModels;
    }

}
