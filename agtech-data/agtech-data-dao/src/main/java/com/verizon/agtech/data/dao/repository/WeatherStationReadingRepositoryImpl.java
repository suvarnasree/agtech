package com.verizon.agtech.data.dao.repository;

import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Date;
import java.util.List;


public class WeatherStationReadingRepositoryImpl implements WeatherStationReadingRepositoryCustom {
    private final static Logger logger = LoggerFactory.getLogger(WeatherStationReadingRepositoryImpl.class);

    @Autowired
    private MongoTemplate mongoTemplate;
}
