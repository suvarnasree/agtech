package com.verizon.agtech.data.consumer.delegate;

import com.google.protobuf.ExtensionRegistry;
import com.google.protobuf.InvalidProtocolBufferException;
import com.verizon.CDFProtos;
import com.verizon.agtech.AgTechCDFProtos;
import com.verizon.agtech.data.akka.CollectorActor;
import com.verizon.agtech.data.akka.CollectorDelegate;
import com.verizon.agtech.data.akka.StopWorkException;
import com.verizon.agtech.data.common.data.AgTechDateTime;
import com.verizon.agtech.data.common.data.BusData;
import com.verizon.agtech.data.common.data.weatherstation.WeatherStationData;
import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;
import com.verizon.agtech.data.dao.service.WeatherStationReadingService;
import org.apache.commons.net.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import java.text.ParseException;


public class WeatherStationReadingPersister extends CollectorDelegate {
    private final static Logger logger = LoggerFactory.getLogger(WeatherStationReadingPersister.class);
    public final static String NAME = "WEATHER_STATION_READING_PERSISTER";
    @Autowired
    protected MessageSource messageSource;

    @Autowired
    private WeatherStationReadingService weatherStationReadingService;



    @Override
    public Object processData(Object data, CollectorActor sender) {
        if (logger.isDebugEnabled()) {
            logger.debug(messageSource.getMessage("delegate.entering", new Object[]{NAME}, null));
        }
        // Persist bus data
        if (data instanceof BusData) {
            BusData busData = (BusData) data;

            String payload = busData.getPayload();
            byte[] bytes = payload.getBytes();
            if (logger.isDebugEnabled()) {
                logger.debug("Data binary:" + payload);
            }
            try {
                ExtensionRegistry extensionRegistry = ExtensionRegistry.newInstance();
                CDFProtos.registerAllExtensions(extensionRegistry);
                AgTechCDFProtos.registerAllExtensions(extensionRegistry);

                CDFProtos.Collection collection = CDFProtos.Collection.parseFrom(Base64.decodeBase64(bytes), extensionRegistry);

                for (CDFProtos.DataBlob dataBlob : collection.getDataList()) {
                    CDFProtos.Measurement measurement = dataBlob.getMeasure();
                    WeatherStationReadingDocument weatherStationReadingDocument = new WeatherStationReadingDocument();
                    weatherStationReadingDocument.setWeatherStationID(measurement.getSensorGuid());
                    if (logger.isDebugEnabled()) {
                        logger.debug(String.format("Weather Station ID: %s", measurement.getSensorGuid()));
                    }
                    if (measurement.getTimespan() != null) {
                        try {
                            weatherStationReadingDocument.setTimeStamp(AgTechDateTime.getDefaultDateFormat().parse(measurement.getTimespan().getBegin()));
                        } catch (ParseException e) {

                        }
                    }
                    if (measurement != null) {
                        AgTechCDFProtos.WeatherStationReading weatherStationReading = measurement.getExtension(AgTechCDFProtos.weatherStationReading);
                        AgTechCDFProtos.WeatherStationMeasurement meanTemperature = weatherStationReading.getMeanTemperature();
                        AgTechCDFProtos.WeatherStationMeasurement relativeHumidity = weatherStationReading.getRelativeHumidity();
                        AgTechCDFProtos.WeatherStationMeasurement windSpeed = weatherStationReading.getWindSpeed();
                        AgTechCDFProtos.WeatherStationMeasurement solarRadiation = weatherStationReading.getSolarRadiation();
                        AgTechCDFProtos.WeatherStationMeasurement totalPrecipitation = weatherStationReading.getTotalPrecipitation();

                        weatherStationReadingDocument.setMeanTemperature(new com.verizon.agtech.data.dao.domain.Measurement(meanTemperature.getUnit(),meanTemperature.getValue()));
                        weatherStationReadingDocument.setRelativeHumidity(new com.verizon.agtech.data.dao.domain.Measurement(relativeHumidity.getUnit(),relativeHumidity.getValue()));
                        weatherStationReadingDocument.setWindSpeed(new com.verizon.agtech.data.dao.domain.Measurement(windSpeed.getUnit(),windSpeed.getValue()));
                        weatherStationReadingDocument.setSolarRadiation(new com.verizon.agtech.data.dao.domain.Measurement(solarRadiation.getUnit(),solarRadiation.getValue()));
                        weatherStationReadingDocument.setTotalPrecipitation(new com.verizon.agtech.data.dao.domain.Measurement(totalPrecipitation.getUnit(),totalPrecipitation.getValue()));

                        if (logger.isDebugEnabled()) {
                            logger.debug(String.format("Time Stamp         : %s", weatherStationReadingDocument.getTimeStamp()));
                            logger.debug(String.format("Mean Temperature : %s", weatherStationReadingDocument.getMeanTemperature()));
                            logger.debug(String.format("Relative Humidity  : %s", weatherStationReadingDocument.getRelativeHumidity()));
                            logger.debug(String.format("Wind Speed         : %s", weatherStationReadingDocument.getWindSpeed()));
                            logger.debug(String.format("Solar Radiation    : %s", weatherStationReadingDocument.getSolarRadiation()));
                            logger.debug(String.format("Total Precipitation: %s", weatherStationReadingDocument.getTotalPrecipitation()));
                        }

                    }
                    weatherStationReadingDocument =  weatherStationReadingService.saveWeatherStationReadingDocument(weatherStationReadingDocument);

                    if (logger.isDebugEnabled()) {
                        logger.debug(String.format("Persist weather station reading %s", weatherStationReadingDocument.getId()));
                        logger.debug(messageSource.getMessage("delegate.leaving", new Object[]{NAME}, null));
                    }
                }
                return data;
            } catch (InvalidProtocolBufferException e) {
                throw new StopWorkException("Failed to decode protobuf binary", e);
            }
        } else {
            throw new StopWorkException(messageSource.getMessage("delegate.invalidData", new Object[]{NAME, WeatherStationData.class.getCanonicalName(), data.getClass().getCanonicalName()}, null));
        }
    }



}
