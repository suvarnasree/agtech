package com.verizon.agtech.AuthenticationHandler;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by Thani on 6/24/16.
 */
public class FailureAuthenticationHandler extends SimpleUrlAuthenticationFailureHandler {


    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {

            response.setContentType("application/json;charset=UTF-8");
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            PrintWriter out = response.getWriter();
            out.print("{\"message\":\"" + exception.getMessage()
                    + "\", \"access_denied\":true,\"cause\":\"SC_FORBIDDEN\"}");
            out.flush();
            out.close();
    }

}
