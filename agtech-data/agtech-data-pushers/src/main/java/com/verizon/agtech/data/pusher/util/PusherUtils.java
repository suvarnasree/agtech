package com.verizon.agtech.data.pusher.util;

import com.verizon.agtech.data.pusher.data.JobDetails;
import org.springframework.context.MessageSource;

import java.text.SimpleDateFormat;

/**
 * Created by drq on 11/12/15.
 */
public class PusherUtils {
    private static SimpleDateFormat dateFormat1;

    private static SimpleDateFormat dateFormat2;

    private static MessageSource messageSource;

    public static void setDateFormat1(SimpleDateFormat dateFormat1) {
        PusherUtils.dateFormat1 = dateFormat1;
    }

    public static void setDateFormat2(SimpleDateFormat dateFormat2) {
        PusherUtils.dateFormat2 = dateFormat2;
    }

    public static void setMessageSource(MessageSource messageSource) {
        PusherUtils.messageSource = messageSource;
    }

    public static String getNotificationFileName(JobDetails jobDetails) {
        String fileName = jobDetails.getCompany();
        if (jobDetails.getEnd() != null) {
            fileName += "_" + dateFormat1.format(jobDetails.getEnd());
        }
        return fileName;
    }
}
