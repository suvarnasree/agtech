package com.verizon.agtech.data.dao.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.verizon.agtech.data.common.domain.AbstractAgTechDocument;
import com.verizon.agtech.data.common.domain.AbstractAgTechOwnedDocument;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;

/**
 * Created by drq on 9/5/14.
 */
public class AbstractAgTechLocationDocument extends AbstractAgTechOwnedDocument {

    public static Double DEFAULT_LATITUDE = 90.0;
    public static Double DEFAULT_LONGITUDE = 180.0;

    @JsonIgnore
    @GeoSpatialIndexed
    private Point point;

    private Double altitude;

    private String geoCoder;

    public AbstractAgTechLocationDocument() {
    }

    public AbstractAgTechLocationDocument(String customerId) {
        super(customerId);
    }

    public AbstractAgTechLocationDocument(String customerId, Double latitude, Double longitude, Double altitude) {
        super(customerId);
        this.point = longitude != null && latitude != null ? new Point(longitude, latitude) : null;
        this.altitude = altitude;
    }

    public AbstractAgTechLocationDocument(String customerId, Double latitude, Double longitude, Double altitude, String geoCoder) {
        this(customerId, latitude, longitude, altitude);
        this.geoCoder = geoCoder;
    }

    public Double getLatitude() {
        return this.point != null ? this.point.getY() : null;
    }

    public Double getLongitude() {
        return this.point != null ? this.point.getX() : null;
    }

    public void setLongitude(Double longitude) {
        this.point = new Point(longitude, this.point == null ? DEFAULT_LATITUDE : this.point.getY());
    }

    public void setLatitude(Double latitude) {
        this.point = new Point(this.point == null ? DEFAULT_LONGITUDE : this.point.getX(), latitude);
    }

    public Double getAltitude() {
        return altitude;
    }

    public void setAltitude(Double altitude) {
        this.altitude = altitude;
    }

    public String getGeoCoder() {
        return geoCoder;
    }

    public void setGeoCoder(String geoCoder) {
        this.geoCoder = geoCoder;
    }
}
