//package com.verizon.agtech.rest.controller;
//
//import com.verizon.agtech.data.common.data.entity.Entity;
//import com.verizon.agtech.data.common.data.entity.EntityDetail;
//import com.verizon.agtech.data.common.data.identity.Customer;
//import com.verizon.agtech.data.common.error.ResourceNotFoundException;
//import com.verizon.agtech.data.dao.domain.CustomerDocument;
//import com.verizon.agtech.data.dao.domain.EntityDocument;
//import com.verizon.agtech.data.dao.service.CustomerService;
//import com.verizon.agtech.data.dao.service.EntityService;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import io.swagger.annotations.ApiParam;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.domain.Page;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.*;
//import springfox.documentation.annotations.ApiIgnore;
///**
// * Created by donthi
// */
//@RequestMapping("v1/customers")
//@Api(value = "/v1/customers", tags = "Deployment", description = "Deployment REST operations")
//public class DeploymentController {
//
//    @Autowired
//    private EntityService entityService;
//
//    @Autowired
//    private CustomerService customerService;
//
//    private final static Logger logger = LoggerFactory.getLogger(DeploymentController.class);
//
//    @RequestMapping(value = "/{accountName}/account", method = RequestMethod.PUT)
//    @ApiOperation(value = "Update customer", notes = "Updates customer account information.")
//    public Model updateCustomerDocument(
//            @ApiIgnore
//            final Model model,
//            @PathVariable
//            @ApiParam(name = "accountName", value = "Account Name", required = true)
//                    String accountName,
//            @RequestBody
//            @ApiParam(name = "customer", value = "Customer", required = true)
//                    Customer customer
//    ) {
//       CustomerDocument customerDocumentToBeUpdated = customerService.findAccountByAnyName(accountName);
//
//        if (customerDocumentToBeUpdated != null) {
//        	accountName = customerDocumentToBeUpdated.getAccountName();
//            customer.setAccountName(accountName);
//            customerDocumentToBeUpdated = customerService.updateCustomerDocument(customerDocumentToBeUpdated, customer);
//            model.addAttribute("accountName", accountName);
//            model.addAttribute("customer", customerDocumentToBeUpdated);
//        } else {
//            throw new ResourceNotFoundException(accountName, "customer");
//        }
//        return model;
//    }
//
//    @RequestMapping(value = "/{accountName}/entities", method = RequestMethod.GET)
//    @ApiOperation(value = "List customer entities", notes = "Returns the current list of entities for the given customer Id")
//    public Model findAllEntityDocuments(
//            @ApiIgnore
//            final Model model,
//            @PathVariable
//            @ApiParam(name = "accountName", value = "Account Name", required = true)
//                    String accountName,
//            @PathVariable
//            @RequestParam(value = "p", required = false, defaultValue = "0")
//            @ApiParam(name = "p", value = "Start page number", required = false, defaultValue = "0")
//                    Integer page,
//            @ApiParam(name = "s", value = "Page size", required = false, defaultValue = "20")
//            @RequestParam(value = "s", required = false, defaultValue = "20")
//                    Integer size) {
//
//    	Page<EntityDocument> entities = entityService.findEntitiesByAccountName(accountName, page, size);
//
//        model.addAttribute("accountName", accountName);
//        model.addAttribute("entities", entities.iterator());
//        model.addAttribute("page", page);
//        model.addAttribute("pageSize", size);
//        model.addAttribute("pageTotalItems", entities.getNumberOfElements());
//        model.addAttribute("hasNextPage", entities.hasNext());
//        model.addAttribute("hasPreviousPage", entities.hasPrevious());
//        model.addAttribute("totalPages", entities.getTotalPages());
//
//        return model;
//    }
//
//    @RequestMapping(value = "/{accountName}/entities/{catalogName}", method = RequestMethod.PUT)
//    @ApiOperation(value = "Update entity", notes = "Set the instance information for the given entity.")
//    public Model updateEntityDocument(
//            @ApiIgnore
//            final Model model,
//            @PathVariable
//            @ApiParam(name = "accountName", value = "Account Name", required = true)
//                    String accountName,
//            @PathVariable
//            @ApiParam(name = "catalogName", value = "Catalog Name", required = true)
//                    String catalogName,
//
//            @RequestBody
//           @ApiParam(name = "entity", value = "Entity", required = true)
//                   Entity entity
//                   ) throws CloneNotSupportedException {
//
//       CustomerDocument docu = customerService.findAccountByAnyName(accountName);
//        EntityDocument entityDocumentToBeUpdated = entityService.findEntityByCustomerId(docu.getId());
//        if (entityDocumentToBeUpdated != null) {
//            model.addAttribute("accountName", accountName);
//            model.addAttribute("catalogName", catalogName);
//            entityDocumentToBeUpdated = entityService.updateEntityDocument(entityDocumentToBeUpdated, entity);
//            model.addAttribute("entity", entityDocumentToBeUpdated);
//        } else {
//           throw new ResourceNotFoundException(catalogName, "catalogName");
//        }
//        return model;
//    }
//
//   @RequestMapping(value = "/{accountName}/entities/{catalogName}", method = RequestMethod.GET)
//    @ApiOperation(value = "Retrieve entity", notes = "Returns the current provisioning state of the given entity.")
//    public Model createEntityDocument(
//           @ApiIgnore
//            final Model model,
//            @PathVariable
//            @ApiParam(name = "accountName", value = "Account Name", required = true)
//                    String accountName,
//            @PathVariable
//            @ApiParam(name = "catalogName", value = "catalog Name", required = true)
//                    String catalogName) throws Exception {
//
//    	CustomerDocument docu = customerService.findAccountByAnyName(accountName);
//        EntityDocument entityDocument = entityService.findEntityByCustomerId(docu.getId());
//
//        if (entityDocument != null) {
//
//            model.addAttribute("accountName", accountName);
//            model.addAttribute("catalogName", catalogName);
//            model.addAttribute("entity", entityDocument);
//        } else {
//            throw new ResourceNotFoundException(catalogName, "entity");
//        }
//        return model;
//   }
//}
//
