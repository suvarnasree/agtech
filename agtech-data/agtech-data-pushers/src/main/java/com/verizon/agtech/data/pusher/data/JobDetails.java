package com.verizon.agtech.data.pusher.data;

import java.util.Date;

/**
 * Created by drq on 10/3/15.
 */
public class JobDetails {
    private String company;
    private Date start;
    private Date end;
    private Long maxResults;

    public JobDetails() {
    }

    public JobDetails(String company, Date start, Date end, Long maxResults) {
        this.company = company;
        this.start = start;
        this.end = end;
        this.maxResults = maxResults;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Long getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Long maxResults) {
        this.maxResults = maxResults;
    }
}
