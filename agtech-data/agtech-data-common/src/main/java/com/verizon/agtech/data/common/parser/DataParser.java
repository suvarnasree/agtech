package com.verizon.agtech.data.common.parser;

/**
 * Created by drq on 6/11/14.
 */
public interface DataParser {
    public Object parseData(Object data);
}
