package com.verizon.agtech.data.ingestor.delegate;

import com.verizon.agtech.data.akka.CollectorDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

/**
 * Created by drq on 4/11/16.
 */
public class IngestorDelegate extends CollectorDelegate{
    @Autowired
    protected MessageSource messageSource;
}
