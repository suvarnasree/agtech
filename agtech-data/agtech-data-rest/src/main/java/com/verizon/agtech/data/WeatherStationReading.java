package com.verizon.agtech.data;

import com.verizon.agtech.data.dao.domain.Measurement;
import com.verizon.agtech.data.dao.domain.WeatherStationReadingDocument;

import java.util.Date;

public class WeatherStationReading {

    private Date timeStamp;
    private Measurement windSpeed;
    private Measurement relativeHumidity;
    private Measurement totalPrecipitation;
    private Measurement solarRadiation;

    public WeatherStationReading(WeatherStationReadingDocument weatherStationReadingDocument){
        this.timeStamp = weatherStationReadingDocument.getTimeStamp();
        this.windSpeed = weatherStationReadingDocument.getWindSpeed();
        this.relativeHumidity = weatherStationReadingDocument.getRelativeHumidity();
        this.totalPrecipitation = weatherStationReadingDocument.getTotalPrecipitation();
        this.solarRadiation = weatherStationReadingDocument.getSolarRadiation();

    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Measurement getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Measurement windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Measurement getRelativeHumidity() {
        return relativeHumidity;
    }

    public void setRelativeHumidity(Measurement relativeHumidity) {
        this.relativeHumidity = relativeHumidity;
    }

    public Measurement getTotalPrecipitation() {
        return totalPrecipitation;
    }

    public void setTotalPrecipitation(Measurement totalPrecipitation) {
        this.totalPrecipitation = totalPrecipitation;
    }

    public Measurement getSolarRadiation() {
        return solarRadiation;
    }

    public void setSolarRadiation(Measurement solarRadiation) {
        this.solarRadiation = solarRadiation;
    }
}
