package com.verizon.agtech.data.pusher;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;


import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;

/**
 * Created by Thani on 6/7/16.
 */

public class CustomerAdditionEmailNotifier {

    private JavaMailSender javaMailSender;
    private String mailFrom;
    private String mailTo;
    private String mailSubject;
    private String msg;

    public CustomerAdditionEmailNotifier(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }


    public void sendEmail(File jsonCustomer) throws MessagingException, IOException {

        MimeMessage message = this.javaMailSender.createMimeMessage();


        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(mailTo,true));
        helper.setFrom(mailFrom);
        helper.setSubject(mailSubject);
        helper.setText(msg);
        helper.addAttachment("Customer.json", jsonCustomer);

        if (message != null) {
            this.javaMailSender.send(message);
        }

    }

    public String getMailFrom() {
        return mailFrom;
    }

    public void setMailFrom(String mailFrom) {
        this.mailFrom = mailFrom;
    }

    public String getMailTo() {
        return mailTo;
    }

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    public String getMailSubject() {
        return mailSubject;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }


}
