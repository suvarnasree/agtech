package com.verizon.agtech.data.dao.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.verizon.agtech.data.common.data.entity.EntityDetail;
import com.verizon.agtech.data.common.data.entity.EntityItem;
//import com.verizon.agtech.data.common.data.entity.EntityType;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;
/**
 * Created by donthi 
 */
@Document(collection = "entities")
@CompoundIndexes({
})
public class EntityDocument extends AbstractAgTechLocationDocument {
	
    @DBRef
    private CatalogDocument catalog;

    @DBRef
    private List<EntityDetail> entityDetail= new ArrayList<>();
    
    private List<EntityItem> entityItems = new ArrayList<>();
    
    //private List<EntityDetail> entity = new ArrayList<>();
    
    public EntityDocument() {
        super();
    }

    public EntityDocument(String customerId) {
        super(customerId);
    }

   public EntityDocument(String customerId, CatalogDocument catalog) {
        super(customerId);
        this.catalog = catalog;
    }

   public CatalogDocument getCatalog() {
        return catalog;
    }

    public void setCatalog(CatalogDocument catalog) {
        this.catalog = catalog;
    }

   public String getEntityId() {
        return getGuid();
    }

	
	public List<EntityDetail> getEntityDetail() {
	return entityDetail;
    }

  public void setEntityDetail(List<EntityDetail> entityDetail) {
	this.entityDetail = entityDetail;
    }

	public List<EntityItem> getEntityItems() {
		return entityItems;
	}

	public void setEntityItems(List<EntityItem> entityItems) {
		this.entityItems = entityItems;
	}
}
