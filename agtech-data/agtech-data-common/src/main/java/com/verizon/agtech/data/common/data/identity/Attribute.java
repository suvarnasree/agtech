package com.verizon.agtech.data.common.data.identity;

/**
 * Created by rangapo on 5/29/16.
 */
public class Attribute {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
