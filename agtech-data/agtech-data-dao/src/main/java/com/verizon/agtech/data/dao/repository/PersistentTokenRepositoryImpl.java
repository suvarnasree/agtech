package com.verizon.agtech.data.dao.repository;

import com.verizon.agtech.data.dao.domain.RememberMeTockenDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import java.util.Date;

/**
 * Created by pusasu5 on 6/20/16.
 */
public class PersistentTokenRepositoryImpl implements PersistentTokenRepository {

    @Autowired
    private RememberMeTokenRepository rememberMeTokenRepository;

    @Override
    public void createNewToken(PersistentRememberMeToken persistentRememberMeToken) {
        RememberMeTockenDocument rememberMeTockenDocument = new RememberMeTockenDocument
                (persistentRememberMeToken.getUsername(),persistentRememberMeToken.getSeries(),persistentRememberMeToken.getTokenValue(),persistentRememberMeToken.getDate());
        rememberMeTokenRepository.save(rememberMeTockenDocument);

    }

    @Override
    public void updateToken(String s, String s1, Date date) {
      RememberMeTockenDocument rememberMeTockenDocument = rememberMeTokenRepository.findBySeries(s);
        rememberMeTockenDocument.setDate(new Date());
        rememberMeTockenDocument.setSeries(s);
        rememberMeTockenDocument.setTokenValue(s1);
        rememberMeTokenRepository.save(rememberMeTockenDocument);

    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String s) {
        RememberMeTockenDocument rememberMeTockenDocument=rememberMeTokenRepository.findBySeries(s);
        PersistentRememberMeToken persistentRememberMeToken = null;
        if (rememberMeTockenDocument != null) {
            persistentRememberMeToken = new PersistentRememberMeToken(rememberMeTockenDocument.getUsername(),rememberMeTockenDocument.getSeries(),rememberMeTockenDocument.getTokenValue(),rememberMeTockenDocument.getDate());
            return persistentRememberMeToken;
        }

        return persistentRememberMeToken;
    }

    @Override
    public void removeUserTokens(String s) {
        rememberMeTokenRepository.delete(rememberMeTokenRepository.findBySeries(s));

    }
}
